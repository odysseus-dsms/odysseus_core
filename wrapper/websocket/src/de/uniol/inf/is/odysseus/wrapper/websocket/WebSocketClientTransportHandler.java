package de.uniol.inf.is.odysseus.wrapper.websocket;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.nio.ByteBuffer;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.uniol.inf.is.odysseus.core.collection.OptionMap;
import de.uniol.inf.is.odysseus.core.option.OptionParameter;
import de.uniol.inf.is.odysseus.core.physicaloperator.access.protocol.IProtocolHandler;
import de.uniol.inf.is.odysseus.core.physicaloperator.access.transport.AbstractTransportHandler;
import de.uniol.inf.is.odysseus.core.physicaloperator.access.transport.ITransportHandler;

public class WebSocketClientTransportHandler extends AbstractTransportHandler {

	Logger LOG = LoggerFactory.getLogger(WebSocketClientTransportHandler.class);

	private static final String URI = "uri";
	public static final String NAME = "WebsocketClient";

	private WebSocketClient client;
	@OptionParameter(name = URI, optional = false, type = URI.class, defaultValue = "", doc = "The web socket URL to connect to. Must start with ws://")
	private URI uri;
	private boolean inClosed = false;

	public WebSocketClientTransportHandler() {
		super();
	}

	public WebSocketClientTransportHandler(IProtocolHandler<?> protocolHandler, OptionMap options) {
		super(protocolHandler, options);
		init(options);
	}

	private void init(OptionMap options) {
		handleAnnotations(options, WebSocketClientTransportHandler.class);
		if (!uri.toString().startsWith("ws")) {
			throw new IllegalArgumentException("URI must start with 'ws'!");
		}
	}

	@Override
	public ITransportHandler createInstance(IProtocolHandler<?> protocolHandler, OptionMap options) {
		return new WebSocketClientTransportHandler(protocolHandler, options);
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public void processInOpen() throws IOException {

		inClosed = false;
		LOG.info("Connecting to " + uri);
		
		if (client == null) {

			client = new WebSocketClient(uri) {

				@Override
				public void onOpen(ServerHandshake handshakedata) {
					LOG.info("Connected to " + uri);
				}

				@Override
				public void onMessage(String message) {
					fireProcess(message);
				}

				@Override
				public void onMessage(ByteBuffer bytes) {
					ByteBuffer copy = ByteBuffer.allocate(bytes.capacity());
					final ByteBuffer readOnly = bytes.asReadOnlyBuffer();
					copy.put(readOnly);
					fireProcess(copy);
				}

				@Override
				public void onError(Exception ex) {
					LOG.error("ERROR in Websocket Connection", ex);
				}

				@Override
				public void onClose(int code, String reason, boolean remote) {
					LOG.info("Disconnected from "+uri+" id="+code+" reason="+reason+" "+remote);
					WebSocketClientTransportHandler.this.handleInReconnect();
				}
			};
			// Do not check for connection timeouts
			client.setConnectionLostTimeout(0);
			client.connect();
		}
	}

	protected void handleInReconnect() {
		try {
			if (isReconnect() && !inClosed) {
				if (client != null) {
					LOG.debug("Trying to reconnect");
					Runnable reconnectThread = new Runnable() {
						@Override
						public void run() {
							client.reconnect();
						}
					};
					new Thread(reconnectThread).start();
				}
			}
		}catch(Exception e) {
			LOG.error("Reconnection Error",e);
		}
	}

	@Override
	public void processOutOpen() throws IOException {

		LOG.info("Connecting to " + uri);

		client = new WebSocketClient(uri) {

			@Override
			public void onOpen(ServerHandshake handshakedata) {

			}

			@Override
			public void onMessage(String message) {

			}

			@Override
			public void onError(Exception ex) {

			}

			@Override
			public void onClose(int code, String reason, boolean remote) {
			}
		};
		client.connect();

	}

	@Override
	public void processInClose() throws IOException {
		inClosed = true;
		if (client != null) {
			client.close();
			client = null;
		}
	}

	@Override
	public void processOutClose() throws IOException {
		if (client != null) {
			client.close();
			client = null;
		}
	}

	@Override
	public void send(byte[] message) throws IOException {
		client.send(message);
	}

	@Override
	public InputStream getInputStream() {
		throw new UnsupportedOperationException();
	}

	@Override
	public OutputStream getOutputStream() {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean isSemanticallyEqualImpl(ITransportHandler other) {
		return false;
	}

}
