package de.uniol.inf.is.odysseus.datarate;

public enum DatarateType {

	BYTE,
	COUNT
}
