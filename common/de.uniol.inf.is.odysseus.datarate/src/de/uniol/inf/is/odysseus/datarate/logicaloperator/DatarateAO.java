package de.uniol.inf.is.odysseus.datarate.logicaloperator;

import de.uniol.inf.is.odysseus.core.logicaloperator.LogicalOperatorCategory;
import de.uniol.inf.is.odysseus.core.server.logicaloperator.UnaryLogicalOp;
import de.uniol.inf.is.odysseus.core.server.logicaloperator.annotations.LogicalOperator;
import de.uniol.inf.is.odysseus.core.server.logicaloperator.annotations.Parameter;
import de.uniol.inf.is.odysseus.core.server.logicaloperator.builder.BooleanParameter;
import de.uniol.inf.is.odysseus.core.server.logicaloperator.builder.EnumParameter;
import de.uniol.inf.is.odysseus.core.server.logicaloperator.builder.IntegerParameter;
import de.uniol.inf.is.odysseus.core.server.logicaloperator.builder.StringParameter;
import de.uniol.inf.is.odysseus.datarate.DatarateType;

@LogicalOperator( name = "Datarate", doc = "Calculates the datarate and inserts the results into metadata", minInputPorts = 1, maxInputPorts = 1, category = {LogicalOperatorCategory.BENCHMARK}, hidden = false)
public class DatarateAO extends UnaryLogicalOp {

	private static final long serialVersionUID = 1L;

	private int updateRateElements;
	private DatarateType type = DatarateType.COUNT;
	private String key;
	private Boolean includeMetaData;

	public DatarateAO() {

	}

	public DatarateAO( DatarateAO other ) {
		super(other);

		this.updateRateElements = other.updateRateElements;
		if(other.type != null)
			this.type=other.type;
		this.key = other.key;
		this.includeMetaData = other.includeMetaData;
	}

	@Override
	public DatarateAO clone() {
		return new DatarateAO(this);
	}

	@Parameter(name = "UpdateRate", aliasname= "UpdateRate",doc = "Element count after recalculating the datarate. Zero means no measurements.",type = IntegerParameter.class, optional = false)
	public void setUpdateRate( int updateRate ) {
		this.updateRateElements = updateRate;
		addParameterInfo("UPDATERATE", String.valueOf(updateRate));
	}
	
	@Parameter(name = "type",aliasname = "type",doc = "Set type = BYTE, UpdateRate=0 for Datarate in bytes instead of element count", type = EnumParameter.class, optional = true)
	public void setType(DatarateType type ) {
		this.type =type;
		addParameterInfo("TYPE", type);
	}

	@Parameter(name = "IncludeMetaData",aliasname = "IncludeMetaData",doc = "Include the size of meta data for data rate in bytes",type = BooleanParameter.class,optional = true)
	public void setIncludeMetaData(Boolean includeMetaData) {
		this.includeMetaData = includeMetaData;
	}

	public int getUpdateRate() {
		return updateRateElements;
	}
	public DatarateType getType() {
		return type;
	}
	public Boolean getIncludeMetaData() {
		return includeMetaData;
	}

	@Parameter(type = StringParameter.class, optional = false, doc = "Name of the measure point")
	public void setKey(String key) {
		this.key = key;
		addParameterInfo("KEY", key+"");
	}

	public String getKey() {
		return key;
	}

}
