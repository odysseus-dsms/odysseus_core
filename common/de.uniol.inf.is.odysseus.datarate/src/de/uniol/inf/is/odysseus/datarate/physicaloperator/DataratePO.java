package de.uniol.inf.is.odysseus.datarate.physicaloperator;

import de.uniol.inf.is.odysseus.core.datahandler.DataHandlerRegistry;
import de.uniol.inf.is.odysseus.core.datahandler.IStreamObjectDataHandler;
import de.uniol.inf.is.odysseus.core.metadata.IMetaAttribute;
import de.uniol.inf.is.odysseus.core.metadata.IStreamObject;
import de.uniol.inf.is.odysseus.core.physicaloperator.IPhysicalOperator;
import de.uniol.inf.is.odysseus.core.physicaloperator.IPunctuation;
import de.uniol.inf.is.odysseus.core.physicaloperator.OpenFailedException;
import de.uniol.inf.is.odysseus.core.sdf.schema.SDFSchema;
import de.uniol.inf.is.odysseus.core.server.metadata.MetadataRegistry;
import de.uniol.inf.is.odysseus.core.server.physicaloperator.AbstractPipe;
import de.uniol.inf.is.odysseus.datarate.DatarateType;
import de.uniol.inf.is.odysseus.datarate.IDatarate;
import de.uniol.inf.is.odysseus.datarate.logicaloperator.DatarateAO;

public class DataratePO<T extends IStreamObject<?>> extends AbstractPipe<T, T> {

	final private int updateRate;
	final private String key;
	final private DatarateType type;

	private long elementsRead = 0;
	private long lastTimestamp = -1;
	private double readSum = 0;
	private double dataRate = 0;

	private Boolean includeMetaData;
	private IStreamObjectDataHandler<?> objectHandler;

	public DataratePO(DatarateAO ao) {
		updateRate = ao.getUpdateRate();
		type = ao.getType();
		key = ao.getKey();
		if (type != DatarateType.BYTE && type != DatarateType.COUNT) {
			throw new IllegalArgumentException("DataratePO cannot handly DatarateType "+type.name());
		}
		SDFSchema schema = ao.getOutputSchema();
		objectHandler = DataHandlerRegistry.instance.getStreamObjectDataHandler(schema.getType().getSimpleName(),
				schema);
		includeMetaData = Boolean.TRUE.equals(ao.getIncludeMetaData());
		if (includeMetaData) {
			objectHandler
					.setMetaAttribute(MetadataRegistry.getMetadataType(ao.getOutputSchema().getMetaAttributeNames()));
		}
	}

	@Override
	public OutputMode getOutputMode() {
		return OutputMode.INPUT;
	}

	@Override
	protected void process_open() throws OpenFailedException {
		elementsRead = 0;
		readSum = 0;
		lastTimestamp = System.nanoTime();
		dataRate = 0;
	}

	@Override
	protected void process_next(T object, int port) {
		IMetaAttribute metadata = object.getMetadata();
		elementsRead++;
		if (type == DatarateType.BYTE) {
			readSum = readSum + objectHandler.memSize(object, includeMetaData);
		} else {
			readSum = elementsRead;
		}

		if (updateRate > 0) {
			if (elementsRead == updateRate) {
				long now = System.nanoTime();
				long lastPeriodNano = now - lastTimestamp;
				double lastDataRateNano = readSum / (double) lastPeriodNano;
				dataRate = lastDataRateNano * 1000000000.0;
				lastTimestamp = now;
				elementsRead = 0;
				readSum = 0;
			}
		} else {
			dataRate = readSum;
		}

		((IDatarate) metadata).setDatarate(key, dataRate);

		transfer(object);
	}

	@Override
	public void processPunctuation(IPunctuation punctuation, int port) {
		sendPunctuation(punctuation);
	}

	@Override
	public boolean process_isSemanticallyEqual(IPhysicalOperator ipo) {
		if (!(ipo instanceof DataratePO)) {
			return false;
		}
		
		@SuppressWarnings("unchecked")
		DataratePO<T> po = (DataratePO<T>) ipo;
		
		if 	(this.key != po.key) {
			return false;
		}
		
		if (this.type != po.type) {
			return false;
		}
		
		return ((DataratePO<?>) ipo).updateRate == updateRate;
	}

}
