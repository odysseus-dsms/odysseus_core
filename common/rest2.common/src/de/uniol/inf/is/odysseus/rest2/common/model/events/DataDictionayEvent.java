package de.uniol.inf.is.odysseus.rest2.common.model.events;

public class DataDictionayEvent {

	public String type;
	public String name;
	public boolean view;

	public DataDictionayEvent(String type, String name, boolean view) {
		this.type = type;
		this.name = name;
		this.view = view;
	}

}
