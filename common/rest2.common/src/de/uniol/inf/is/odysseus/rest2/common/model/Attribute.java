package de.uniol.inf.is.odysseus.rest2.common.model;

import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

/**
 * Attribute
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaMSF4JServerCodegen", date = "2019-03-27T10:38:43.789+01:00[Europe/Berlin]")
public class Attribute {
	@JsonProperty("sourcename")
	private String sourcename;

	@JsonProperty("attributename")
	private String attributename;

	@JsonProperty("datatype")
	private Datatype datatype = null;

	@JsonProperty("subschema")
	private Schema subschema = null;

	@JsonProperty("unit")
	private String unit = null;

	@JsonProperty("annotations")
	private List<String> annotations = null;

	@JsonProperty("constraints")
	private List<Constraint> constraints = null;

	public Attribute sourcename(String sourcename) {
		this.sourcename = sourcename;
		return this;
	}

	@ApiModelProperty(value = "")
	public String getSourcename() {
		return sourcename;
	}

	public void setSourcename(String sourcename) {
		this.sourcename = sourcename;
	}

	public Attribute attributename(String attributename) {
		this.attributename = attributename;
		return this;
	}

	@ApiModelProperty(value = "")
	public String getAttributename() {
		return attributename;
	}

	public void setAttributename(String attributename) {
		this.attributename = attributename;
	}

	public Attribute datatype(Datatype datatype) {
		this.datatype = datatype;
		return this;
	}

	@ApiModelProperty(value = "")
	public Datatype getDatatype() {
		return datatype;
	}

	public void setDatatype(Datatype datatype) {
		this.datatype = datatype;
	}

	public Attribute subschema(Schema subschema) {
		this.subschema = subschema;
		return this;
	}

	@ApiModelProperty(value = "")
	public Schema getSubschema() {
		return subschema;
	}

	public void setSubschema(Schema subschema) {
		this.subschema = subschema;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public List<String> getAnnotations() {
		return annotations;
	}

	public void setAnnotations(List<String> annotations) {
		this.annotations = annotations;
	}

	public List<Constraint> getConstraints() {
		return constraints;
	}

	public void setConstraints(List<Constraint> constraints) {
		this.constraints = constraints;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class Attribute {\n");

		sb.append("    sourcename: ").append(toIndentedString(sourcename)).append("\n");
		sb.append("    attributename: ").append(toIndentedString(attributename)).append("\n");
		sb.append("    datatype: ").append(toIndentedString(datatype)).append("\n");
		sb.append("    subschema: ").append(toIndentedString(subschema)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	@Override
	public int hashCode() {
		return Objects.hash(annotations, attributename, constraints, datatype, sourcename, subschema, unit);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Attribute other = (Attribute) obj;
		return Objects.equals(annotations, other.annotations) && Objects.equals(attributename, other.attributename)
				&& Objects.equals(constraints, other.constraints) && Objects.equals(datatype, other.datatype)
				&& Objects.equals(sourcename, other.sourcename) && Objects.equals(subschema, other.subschema)
				&& Objects.equals(unit, other.unit);
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}
