package de.uniol.inf.is.odysseus.rest2.common.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Constraint {

	@JsonProperty("key")
	String key;

	@JsonProperty("value")
	String value;

	public Constraint() {
	}

	public Constraint(String key, String value) {
		this.key = key;
		this.value = value;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
