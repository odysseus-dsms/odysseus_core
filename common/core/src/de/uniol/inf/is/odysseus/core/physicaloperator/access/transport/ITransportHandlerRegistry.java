package de.uniol.inf.is.odysseus.core.physicaloperator.access.transport;

import java.util.Map;
import java.util.Set;

import com.google.common.collect.ImmutableList;

import de.uniol.inf.is.odysseus.core.collection.OptionMap;
import de.uniol.inf.is.odysseus.core.option.OptionParameter;
import de.uniol.inf.is.odysseus.core.physicaloperator.access.protocol.IProtocolHandler;

public interface ITransportHandlerRegistry {

	ITransportHandler getInstance(String name, IProtocolHandler<?> protocolHandler, OptionMap options);

	ImmutableList<String> getHandlerNames();

	ITransportHandler getITransportHandlerClass(String transportHandler);

	Set<OptionParameter> getParameters(String handlerName);
	
	Map<String, Set<OptionParameter>> getParameters();
	
}
