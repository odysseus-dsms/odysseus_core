package de.uniol.inf.is.odysseus.core.collection;

import java.io.Serializable;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Objects;

/**
 * This class provides options that can be set once and has a marker for
 * elements that have been read
 *
 * @author Marco Grawunder
 *
 */

public class OptionMap implements Serializable {

	private static final long serialVersionUID = 7256451697615376076L;

	private Map<String, Serializable> optionMap = new HashMap<>();
	private Map<String, Boolean> keyRead = new HashMap<String, Boolean>();

	public OptionMap() {
	}

	public void clear() {
		optionMap.clear();
		keyRead.clear();
	}

	public OptionMap(Map<String, Serializable> optionMap) {
		putAll(optionMap);
	}

	public void putAll(Map<String, Serializable> options) {
		if (options != null) {
			for (Entry<String, Serializable> e : options.entrySet()) {
				overwriteOption(e.getKey().toLowerCase(), e.getValue());
			}
		}
	}

	public OptionMap(List<Option> optionMap) {
		addAll(optionMap);
	}

	public void addAll(List<Option> optionMap) {
		if (optionMap != null) {
			for (Option e : optionMap) {
				overwriteOption(e.getName().toLowerCase(), e.getValue());
			}
		}
	}

	public OptionMap(OptionMap optionMap) {
		addAll(optionMap);
	}

	public Map<String, Object> getOptions() {
		return Collections.unmodifiableMap(optionMap);
	}

	public void addAll(OptionMap optionMap) {
		if (optionMap != null) {
			for (Entry<String, Serializable> e : optionMap.optionMap.entrySet()) {
				overwriteOption(e.getKey().toLowerCase(), e.getValue());
			}
		}
	}

	public void setOption(String key, Serializable value) {
		if (optionMap.containsKey(key.toLowerCase())) {
			throw new IllegalStateException(
					"Option " + key.toLowerCase() + " already set with value " + optionMap.get(key));
		}
		overwriteOption(key.toLowerCase(), value);
	}

	public void overwriteOption(String key, Serializable value) {
		optionMap.put(key.toLowerCase(), value);
		keyRead.put(key.toLowerCase(), Boolean.FALSE);
	}

	@SuppressWarnings("unchecked")
	public <K> K removeOption(String key) {
		keyRead.remove(key.toLowerCase());
		return (K) optionMap.remove(key.toLowerCase());
	}

	public boolean containsKey(String key) {
		return optionMap.containsKey(key.toLowerCase());
	}

	@SuppressWarnings("unchecked")
	public <K> K getValue(String key) {
		if (optionMap.containsKey(key.toLowerCase())) {
			keyRead.put(key.toLowerCase(), Boolean.TRUE);
		}
		return (K) optionMap.get(key.toLowerCase());
	}

	public String get(String key) {
		Object ret = getValue(key);
		return ret != null ? ret + "" : null;
	}

	public String getString(String key) {
		return get(key);
	}

	public String get(String key, String defaultValue) {
		String v = get(key.toLowerCase());
		return v != null ? v : defaultValue;
	}

	public String getString(String key, String defaultValue) {
		return get(key, defaultValue);
	}

	public String getString(String key, String alias, String defaultValue) {
		String v = get(key.toLowerCase());
		return v != null ? v : get(alias, defaultValue);
	}

	public boolean getBoolean(String key, boolean defaultValue) {
		String v = get(key);
		return v != null ? Boolean.parseBoolean(v) : defaultValue;
	}

	public boolean getBoolean(String key, String alias, boolean defaultValue) {
		String v = get(key);
		return v != null ? Boolean.parseBoolean(v) : getBoolean(alias, defaultValue);
	}

	public boolean getBoolean(String key, String defaultValue) {
		return getBoolean(key, Boolean.parseBoolean(defaultValue));
	}

	public boolean getBoolean(String key, String alias, String defaultValue) {
		return getBoolean(key, alias, Boolean.parseBoolean(defaultValue));
	}

	public byte getByte(String key, String defaultValue) {
		return getByte(key, Byte.parseByte(defaultValue));
	}

	public byte getByte(String key, String alias, String defaultValue) {
		String v = get(key);
		return v != null ? Byte.parseByte(v) : getByte(alias, defaultValue);
	}

	public byte getByte(String key, byte defaultValue) {
		String v = get(key);
		return v != null ? Byte.parseByte(v) : defaultValue;
	}

	public char getChar(String key, String defaultValue) {
		return getChar(key, defaultValue.toCharArray()[0]);
	}

	public char getChar(String key, String alias, String defaultValue) {
		String v = get(key);
		return v != null ? v.toCharArray()[0] : getChar(alias, defaultValue);
	}

	public char getChar(String key, char defaultValue) {
		String v = get(key);
		return v != null ? v.toCharArray()[0] : defaultValue;
	}

	public Character getCharacter(String key, Character defaultValue) {
		String v = get(key);
		return v != null ? Character.valueOf(v.toCharArray()[0]) : defaultValue;
	}

	public Character getCharacter(String key, String alias, String defaultValue) {
		String v = get(key);
		return v != null ? Character.valueOf(v.toCharArray()[0]) : getCharacter(alias, defaultValue);
	}

	public Character getCharacter(String key, String defaultValue) {
		return getCharacter(key, Character.valueOf(defaultValue.toCharArray()[0]));
	}

	public short getShort(String key, short defaultValue) {
		String v = get(key);
		return v != null ? Short.parseShort(v) : defaultValue;
	}

	public short getShort(String key, String defaultValue) {
		return getShort(key, Short.parseShort(defaultValue));
	}

	public short getShort(String key, String alias, String defaultValue) {
		String v = get(key);
		return v != null ? Short.parseShort(v) : getShort(alias, defaultValue);
	}

	public int getInt(String key, int defaultValue) {
		String v = get(key);
		return v != null ? Integer.parseInt(v) : defaultValue;
	}

	public int getInt(String key, String defaultValue) {
		return getInt(key, Integer.parseInt(defaultValue));
	}

	public int getInt(String key, String alias, String defaultValue) {
		String v = get(key);
		return v != null ? Integer.parseInt(v) : getInt(alias, defaultValue);
	}

	public long getLong(String key, long defaultValue) {
		String v = get(key);
		return v != null ? Long.parseLong(v) : defaultValue;
	}

	public long getLong(String key, String defaultValue) {
		return getLong(key, Long.parseLong(defaultValue));
	}

	public long getLong(String key, String alias, String defaultValue) {
		String v = get(key);
		return v != null ? Long.parseLong(v) : getLong(alias, defaultValue);
	}

	public float getFloat(String key, float defaultValue) {
		String v = get(key);
		return v != null ? Float.parseFloat(v) : defaultValue;
	}

	public float getFloat(String key, String alias, float defaultValue) {
		String v = get(key);
		return v != null ? Float.parseFloat(v) : getFloat(alias, defaultValue);
	}

	public float getFloat(String key, String defaultValue) {
		return getFloat(key, Float.parseFloat(defaultValue));
	}

	public float getFloat(String key, String alias, String defaultValue) {
		return getFloat(key, alias, Float.parseFloat(defaultValue));
	}

	public double getDouble(String key, double defaultValue) {
		String v = get(key);
		return v != null ? Double.parseDouble(v) : defaultValue;
	}

	public double getDouble(String key, String alias, double defaultValue) {
		String v = get(key);
		return v != null ? Double.parseDouble(v) : getDouble(alias, defaultValue);
	}

	public double getDouble(String key, String defaultValue) {
		return getDouble(key, Double.parseDouble(defaultValue));
	}

	public double getDouble(String key, String alias, String defaultValue) {
		return getDouble(key, alias, Double.parseDouble(defaultValue));
	}

	public Object getObject(String key, Object defaultValue) {
		Object ret = getValue(key);
		return ret != null ? ret : defaultValue;
	}

	public Object getObject(String key, String alias, Object defaultValue) {
		Object ret = getValue(key);
		return ret != null ? ret : getObject(alias, defaultValue);
	}

	public URI getURI(String key, String defaultValue) throws URISyntaxException {
		String v = get(key);
		return v != null ? new URI(v) : new URI(defaultValue);
	}

	public URI getURI(String key, String alias, String defaultValue) throws URISyntaxException {
		String v = get(key);
		return v != null ? new URI(v) : getURI(alias, defaultValue);
	}

	public List<String> getUnreadOptions() {
		List<String> unread = new LinkedList<String>();
		for (Entry<String, Boolean> e : keyRead.entrySet()) {
			if (!e.getValue()) {
				unread.add(e.getKey());
			}
		}
		return unread;
	}

	public List<String> checkRequired(String... keys) {
		List<String> missing = new LinkedList<String>();
		for (String k : keys) {
			if (!containsKey(k.toLowerCase())) {
				missing.add(k.toLowerCase());
			}
		}
		return missing;
	}

	public void checkRequiredException(String... keys) {
		List<String> missing = checkRequired(keys);
		if (!missing.isEmpty()) {
			throw new IllegalArgumentException("The following required options are not set: " + missing);
		}
	}

	public List<String> checkRequired(List<String> keys) {
		List<String> missing = new LinkedList<String>();
		for (String k : keys) {
			if (!containsKey(k.toLowerCase())) {
				missing.add(k.toLowerCase());
			}
		}
		return missing;
	}

	public void checkRequiredException(List<String> keys) {
		List<String> missing = checkRequired(keys);
		if (!missing.isEmpty()) {
			throw new IllegalArgumentException("The following required options are not set: " + missing);
		}
	}

	public boolean existsOneOf(String... keys) {
		for (String k : keys) {
			if (containsKey(k.toLowerCase())) {
				return true;
			}
		}

		return false;
	}

	public void existsOneOfException(String... keys) {
		if (!existsOneOf(keys)) {
			throw new IllegalArgumentException("One of the following options must be set: " + Arrays.asList(keys));
		}
	}

	public List<Pair<String, String>> getPairList() {
		List<Pair<String, String>> ret = new ArrayList<>();
		for (Entry<String, Serializable> option : optionMap.entrySet()) {
			ret.add(new Pair<String, String>(option.getKey(), String.valueOf(option.getValue())));
		}
		return ret;
	}

	@Override
	public String toString() {
		return optionMap.toString();
	}

	public Set<String> getKeySet() {
		return this.optionMap.keySet();
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof OptionMap)) {
			return false;
		}

		OptionMap other = (OptionMap) obj;

		for (Entry<String, Serializable> e : optionMap.entrySet()) {
			Object otherValue = other.get(e.getKey());
			if (otherValue == null) {
				return false;
			}
			if (!Objects.equal(String.valueOf(e.getValue()), otherValue)) {
				return false;
			}
		}
		return true;
	}

	// -----------------------------------------------------------------------
	// creator Methods

	public static OptionMap fromStringMap(Map<String, String> options) {
		OptionMap ret = new OptionMap();
		for (Entry<String, String> e : options.entrySet()) {
			ret.setOption(e.getKey(), e.getValue());
		}
		return ret;
	}

	public static OptionMap fromString(String input) {
		// (type=INTER_OPERATOR) (degree=4) (buffersize=AUTO) (optimization=true)
		OptionMap options = new OptionMap();

		String[] subparams = input.trim().split("\\ ");
		for (String subparam : subparams) {
			int openingBracket = subparam.indexOf("(");
			int closingBracket = subparam.indexOf(")");
			int equalsPos = subparam.indexOf("=");
			if (openingBracket == -1 || closingBracket == -1 || equalsPos == -1) {
				throw new IllegalArgumentException("Missformed Parameter '" + subparam + "'. Must be (NAME=VALUE)");
			}
			String name = subparam.substring(openingBracket + 1, equalsPos).trim();
			String value = subparam.substring(equalsPos + 1, closingBracket).trim();
			options.setOption(name, value);
		}

		return options;
	}

	public static OptionMap newInstance(List<Pair<String, String>> options) {
		OptionMap map = new OptionMap();
		if (options != null) {
			for (Pair<String, String> option : options) {
				map.overwriteOption(option.getE1(), option.getE2());
			}
		}
		return map;
	}

	public String getJSON() {
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			return objectMapper.writeValueAsString(optionMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}

}
