package de.uniol.inf.is.odysseus.core.physicaloperator.marker;

import java.util.HashMap;
import java.util.Map;

import de.uniol.inf.is.odysseus.core.physicaloperator.IPhysicalOperator;

public class MarkerManager implements IMarkerManager {

	private Map<Integer, IMarkerPunctuation> portMarkerMap = new HashMap<>();
	private IPhysicalOperator po;
	private String id;

	@Override
	public void init(IPhysicalOperator po, String id, int size) {
		this.po = po;
		this.id = id;
		for (int i = 0; i <= size; i++) {
			portMarkerMap.put(i, null);
		}
	}

	@Override
	public IMarkerPunctuation createMarker(IMarkerPunctuation marker, int port) {
		if (!portMarkerMap.containsKey(port)) {
			throw new NullPointerException("Port was not set as input port on which marker was received");
		}
		// The outputMarker is used to determine if the marker was merged and should be send forward or not
		IMarkerPunctuation outputMarker = null;
		portMarkerMap.put(port, marker);

		if (isSynced()) {
			marker.merge(portMarkerMap.values());
			marker.processOperator(po, id);

			portMarkerMap.clear();
			outputMarker = marker;
		}
		return outputMarker;
	}

	private boolean isSynced() {
		boolean synced = true;
		for (IMarkerPunctuation marker : portMarkerMap.values()) {
			if (marker == null) {
				synced = false;
				break;
			}
		}
		return synced;
	}
}