package de.uniol.inf.is.odysseus.core.objecthandler;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.uniol.inf.is.odysseus.core.util.OsgiObjectInputStream;

public final class ObjectByteConverter {

	private static final Logger LOG = LoggerFactory.getLogger(ObjectByteConverter.class);

	private ObjectByteConverter() {
	}

	public static Object bytesToObject(byte[] data) {
		if (data == null || data.length == 0) {
			return null;
		}

		final ByteArrayInputStream bis = new ByteArrayInputStream(data);
		ObjectInput in = null;
		try {
			in = new OsgiObjectInputStream(bis);
			return in.readObject();
		} catch (IOException | ClassNotFoundException e) {
			LOG.error("Could not read object", e);
		} finally {
			try {
				bis.close();
				if (in != null) {
					in.close();
				}
			} catch (final IOException ex) {
			}
		}

		return null;
	}

	public static byte[] objectToBytes(Object obj) {
		if (obj == null) {
			return new byte[0];
		}

		final ByteArrayOutputStream bos = new ByteArrayOutputStream();
		ObjectOutput out = null;
		try {
			out = new ObjectOutputStream(bos);
			out.writeObject(obj);
			return bos.toByteArray();
		} catch (final IOException e) {
			LOG.error("Could not convert object {} to byte array", obj, e);
			return new byte[0];
		} finally {
			tryClose(out);
			tryClose(bos);
		}
	}

	public static float[] toFloatArray(byte[] data) {
		if (data == null || data.length == 0) {
			return null;
		}

		final ByteArrayInputStream bis = new ByteArrayInputStream(data);
		DataInputStream in = null;
		try {
			in = new DataInputStream(bis);
			int size = in.readInt();
			if (size > 0) {
				float[] retArray = new float[size];
				for (int i=0;i<size;i++) {
					retArray[i] = in.readFloat();
				}
				return retArray;
			}
			return null;
		} catch (IOException e) {
			LOG.error("Could not read array", e);
		} finally {
			try {
				bis.close();
				if (in != null) {
					in.close();
				}
			} catch (final IOException ex) {
			}
		}

		return null;
	}
	
	public static int[] toIntArray(byte[] data) {
		if (data == null || data.length == 0) {
			return null;
		}

		final ByteArrayInputStream bis = new ByteArrayInputStream(data);
		DataInputStream in = null;
		try {
			in = new DataInputStream(bis);
			int size = in.readInt();
			if (size > 0) {
				int[] retArray = new int[size];
				for (int i=0;i<size;i++) {
					retArray[i] = in.readInt();
				}
				return retArray;
			}
			return null;
		} catch (IOException e) {
			LOG.error("Could not read array", e);
		} finally {
			try {
				bis.close();
				if (in != null) {
					in.close();
				}
			} catch (final IOException ex) {
			}
		}

		return null;
	}

	public static double[] toDoubleArray(byte[] data) {
		if (data == null || data.length == 0) {
			return null;
		}

		final ByteArrayInputStream bis = new ByteArrayInputStream(data);
		DataInputStream in = null;
		try {
			in = new DataInputStream(bis);
			int size = in.readInt();
			if (size > 0) {
				double[] retArray = new double[size];
				for (int i=0;i<size;i++) {
					retArray[i] = in.readDouble();
				}
				return retArray;
			}
			return null;
		} catch (IOException e) {
			LOG.error("Could not read array", e);
		} finally {
			try {
				bis.close();
				if (in != null) {
					in.close();
				}
			} catch (final IOException ex) {
			}
		}

		return null;
	}

	
	// TODO: The following methods do all contain the same code ...
	public static byte[] toByteArray(int[] data) {
		if (data == null) {
			return new byte[0];
		}

		final ByteArrayOutputStream bos = new ByteArrayOutputStream();
		DataOutputStream out = null;
		try {
			out = new DataOutputStream(bos);
			out.write(data.length);
			for (int i=0;i<data.length;i++) {
				out.writeInt(data[i]);
			}
			return bos.toByteArray();
		} catch (final IOException e) {
			LOG.error("Could not convert array {} to byte array", data , e);
			return new byte[0];
		} finally {
			tryClose(out);
			tryClose(bos);
		}
	}

	public static byte[] toByteArray(float[] data) {
		if (data == null) {
			return new byte[0];
		}

		final ByteArrayOutputStream bos = new ByteArrayOutputStream();
		DataOutputStream out = null;
		try {
			out = new DataOutputStream(bos);
			out.write(data.length);
			for (int i=0;i<data.length;i++) {
				out.writeFloat(data[i]);
			}
			return bos.toByteArray();
		} catch (final IOException e) {
			LOG.error("Could not convert array {} to byte array", data , e);
			return new byte[0];
		} finally {
			tryClose(out);
			tryClose(bos);
		}
	}

	public static byte[] toByteArray(double[] data) {
		if (data == null) {
			return new byte[0];
		}

		final ByteArrayOutputStream bos = new ByteArrayOutputStream();
		DataOutputStream out = null;
		try {
			out = new DataOutputStream(bos);
			out.write(data.length);
			for (int i=0;i<data.length;i++) {
				out.writeDouble(data[i]);
			}
			return bos.toByteArray();
		} catch (final IOException e) {
			LOG.error("Could not convert array {} to byte array", data , e);
			return new byte[0];
		} finally {
			tryClose(out);
			tryClose(bos);
		}
	}

	
	
	private static void tryClose(ObjectOutput out) {
		try {
			out.close();
		} catch (final IOException ex) {
		}
	}

	private static void tryClose(OutputStream out) {
		try {
			out.close();
		} catch (final IOException ex) {
		}
	}



}
