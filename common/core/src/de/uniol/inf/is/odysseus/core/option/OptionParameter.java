package de.uniol.inf.is.odysseus.core.option;

import static java.lang.annotation.ElementType.FIELD;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(FIELD)
public @interface OptionParameter{

	String name();
	String alias() default "";
	Class<?> type(); // TODO. Find a more elegant solution to work with option map
	boolean optional() default false;
	String defaultValue() default "";
	boolean defaultValueIsNull() default false;
	boolean deprecated() default false;
	String doc();
}
