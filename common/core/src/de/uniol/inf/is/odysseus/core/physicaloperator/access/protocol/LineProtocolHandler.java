/*******************************************************************************
 * Copyright 2012 The Odysseus Team
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.uniol.inf.is.odysseus.core.physicaloperator.access.protocol;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;

import de.uniol.inf.is.odysseus.core.collection.OptionMap;
import de.uniol.inf.is.odysseus.core.datahandler.IStreamObjectDataHandler;
import de.uniol.inf.is.odysseus.core.infoservice.InfoService;
import de.uniol.inf.is.odysseus.core.infoservice.InfoServiceFactory;
import de.uniol.inf.is.odysseus.core.metadata.IMetaAttribute;
import de.uniol.inf.is.odysseus.core.metadata.IStreamObject;
import de.uniol.inf.is.odysseus.core.option.OptionParameter;
import de.uniol.inf.is.odysseus.core.physicaloperator.access.transport.IAccessPattern;
import de.uniol.inf.is.odysseus.core.physicaloperator.access.transport.ITransportDirection;
import de.uniol.inf.is.odysseus.core.physicaloperator.access.transport.ITransportExchangePattern;

public class LineProtocolHandler<T extends IStreamObject<IMetaAttribute>> extends AbstractProtocolHandler<T> {

	public static final String NAME = "Line";
	static final Runtime RUNTIME = Runtime.getRuntime();

	Logger LOG = LoggerFactory.getLogger(LineProtocolHandler.class);
	InfoService INFOSERVICE = InfoServiceFactory.getInfoService(LineProtocolHandler.class);

	protected BufferedReader reader;
	protected BufferedWriter writer;

	public static final String DELAY = "delay";
	@OptionParameter(name=DELAY, optional = true, defaultValue = "0", type = Long.class, deprecated = true, doc = "Delay of reading in milliseconds. DEPRECATED: use scheduler.delay instead if not used together with delayeach")
	protected long delay;

	public static final String NANODELAY = "nanodelay";
	@OptionParameter(name=NANODELAY, optional = true, defaultValue = "0", type = Integer.class, doc = " Delay of reading in nanoseconds.")
	protected int nanodelay; // why integer?
	
	public static final String DELAYEACH = "delayeach";
	@OptionParameter(name=DELAYEACH, optional = true, defaultValue = "0", type = Integer.class, doc = "The number of lines between a delay is used.")
	protected int delayeach = 0;
	
	public static final String READFIRSTLINE = "readfirstline";
	@OptionParameter(name=READFIRSTLINE, optional = true, defaultValue = "true", type = Boolean.class, doc = "Should the first line of the file be ignored (e.g. because of header information).")
	protected boolean readFirstLine = true;
	
	public static final String SKIPFIRSTLINES = "skipFirstLines";
	@OptionParameter(name=SKIPFIRSTLINES, optional = true, defaultValue = "0", type = Integer.class, doc = "The number of lines that should skipped. Use this, if one or more lines should be skipped (e.g. in case of a longer header).")
	protected int firstLinesToSkip = 0;

	public static final String DUMP_EACH_LINE = "dumpeachline";
	@OptionParameter(name = DUMP_EACH_LINE, optional = true, defaultValue = "-1", type = Long.class, doc = "Dumps lines to the console. if set to 1 each line will be dumped")
	protected long dumpEachLine = -1;
	
	public static final String MEASURE_EACH_LINE = "measureeachline";
	@OptionParameter(name = MEASURE_EACH_LINE, optional = true, defaultValue = "-1", type = Long.class, doc = "Measures the processing time between n elements that are dumped")
	protected long measureEachLine = -1;

	public static final String LAST_LINE = "lastline";
	public static final String MAX_LINES = "maxlines";
	//@OptionParameter(name = MAX_LINES, alias= LAST_LINE, optional = true, defaultValue = "-1", type = Long.class, doc = "Stop processing after n elements are read")
	@OptionParameter(name = MAX_LINES, optional = true, defaultValue = "-1", type = Long.class, doc = "Stop processing after n elements are read")
	protected long lastLine = -1;
	
	public static final String DEBUG = "debug";
	@OptionParameter(name=DEBUG, optional = true, defaultValue = "false", type = Boolean.class, doc ="If set to true, some additional thinks are available (like "+DUMP_EACH_LINE+" or "+MEASURE_EACH_LINE)
	protected boolean debug = false;

	public static final String DUMPFILE = "dumpfile";
	@OptionParameter(name="DUMPFILE", defaultValue = "", type = String.class, optional = true, doc = "In case of debug, write debug output to this file")
	protected String dumpFile = null;

	public static final String NODONE = "nodone";
	@OptionParameter(name = NODONE, optional = true, defaultValue = "false", type = Boolean.class, doc = "If set to true, the handler will never be done.")
	protected boolean noDone = false;

	
	public static final String CHECKDELAY = "checkdelay";
	@OptionParameter(name = CHECKDELAY, optional = true, defaultValue = "0", type = Long.class, doc = "Delay for some time in milliseconds when there is no input.")
	protected long checkDelay;

	
	public static final String DUMPMEMORY = "dumpmemory";
	@OptionParameter(name = DUMPMEMORY, optional = true, defaultValue =  "false", type = Boolean.class, doc ="In debug mode print free memory for each dumped line, too.")
	protected boolean dumpMemory;
	
	
	private long delayCounter = 0L;
	
	protected long lineCounter = 0L;
	private boolean isDone = false;
	private long lastDumpTime = 0;
	private long lastMeasureTime = 0;
	private long basetime;
	private PrintWriter dumpOut;

	private Map<Long, StringBuilder> currentInputStringMap = new HashMap<>();

	public LineProtocolHandler() {
		super();
	}

	public LineProtocolHandler(ITransportDirection direction, IAccessPattern access,
			IStreamObjectDataHandler<T> dataHandler, OptionMap optionsMap) {
		super(direction, access, dataHandler, optionsMap);
		init_internal();
	}

	@Override
	void optionsMapChanged(String key, String value) {
		// simply update
		init_internal();
	}


	private void init_internal() {
	    handleAnnotations(optionsMap, LineProtocolHandler.class);	    
		lastDumpTime = System.currentTimeMillis();
		if (!this.readFirstLine) {
			firstLinesToSkip = 1;
		}
	}

	@Override
	public void open() throws UnknownHostException, IOException {
		getTransportHandler().open();
		if (getDirection().equals(ITransportDirection.IN)) {
			if ((this.getAccessPattern().equals(IAccessPattern.PULL))
					|| (this.getAccessPattern().equals(IAccessPattern.ROBUST_PULL))) {
				reader = new BufferedReader(new InputStreamReader(getTransportHandler().getInputStream()));
			}
		} else {
			if ((this.getAccessPattern().equals(IAccessPattern.PULL))
					|| (this.getAccessPattern().equals(IAccessPattern.ROBUST_PULL))) {
				writer = new BufferedWriter(new OutputStreamWriter(getTransportHandler().getOutputStream()));
			}
		}
		delayCounter = 0;
		lineCounter = 0;
		isDone = false;
		if (debug) {
			ProtocolMonitor.getInstance().addToMonitor(this);
			if (!Strings.isNullOrEmpty(dumpFile)) {
				dumpOut = new PrintWriter(dumpFile);
			}
		}
	}

	@Override
	public void close() throws IOException {
		if (getDirection().equals(ITransportDirection.IN)) {
			if (reader != null) {
				reader.close();
			}
		} else {
			if (writer != null) {
				writer.close();
			}
		}
		getTransportHandler().close();
		if (debug) {
			ProtocolMonitor.getInstance().informMonitor(this, lineCounter);
			ProtocolMonitor.getInstance().removeFromMonitor(this);
			if (dumpOut != null) {
				dumpOut.close();
			}
		}
	}

	@Override
	public boolean hasNext() throws IOException {
		if (hasNext(reader)) {
			return true;
		} else {
			if (checkDelay > 0) {
				try {
					Thread.sleep(checkDelay);
				} catch (InterruptedException e) {
					// interrupting the delay might be correct
					// e.printStackTrace();
				}
			}

			return false;
		}
	}

	private boolean hasNext(BufferedReader reader) {
		try {
			if (reader.ready() == false) {
				isDone = true;
				return false;
			}
		} catch (Exception e) {
			if (!e.getMessage().equalsIgnoreCase("Stream closed")) {
				LOG.error("Could not determine hasNext()", e);

			}
			// DO NOT SET DONE, this is no normal processing!
			return false;
		}
		return true;
	}

	protected String getNextLine(BufferedReader toReadFrom) throws IOException {
		// ignore first lines ( if first line has to be skipped, firstLinesToSkip is 1 and lineCounter is 0)
		while (lineCounter < firstLinesToSkip) {
			toReadFrom.readLine();
			lineCounter++;
		}
		delay();
		String line = null;
		if (hasNext(toReadFrom)) {
			line = toReadFrom.readLine();
		} else {
			long time = System.currentTimeMillis();
			LOG.debug("Read last line. " + lineCounter + " " + time + " " + (time - lastDumpTime) + " ("
					+ Integer.toHexString(hashCode()) + ") line.");
			return null;
		}

		if (debug) {
			if (dumpEachLine > 0) {
				if (lineCounter % dumpEachLine == 0) {
					long time = System.currentTimeMillis();
					LOG.debug(lineCounter + " " + time + " " + (time - lastDumpTime)
							+ (dumpMemory ? " M = " + RUNTIME.freeMemory() + " " : " ") + line + " ("
							+ Integer.toHexString(hashCode()) + ") line: ");
					lastDumpTime = time;
				}
			}
			if (measureEachLine > 0) {
				if (lineCounter % measureEachLine == 0) {
					long time = System.currentTimeMillis();
					// measurements.append(lineCounter).append(";").append(time
					// - basetime).append("\n");
					if (dumpOut != null) {
						dumpOut(time);
					}
					lastMeasureTime = time;
				}
			}

			if (lastLine == lineCounter || lineCounter == 0) {
				long time = System.currentTimeMillis();
				if (lineCounter == 0) {
					basetime = time;
				}
				LOG.debug(lineCounter + " " + time);
				if (dumpOut != null) {
					dumpOut(time);
				}
				// measurements.append(lineCounter).append(";").append(time -
				// basetime).append("\n");
				if (lastLine == lineCounter) {
					// System.out.println(measurements);
					ProtocolMonitor.getInstance().informMonitor(this, lineCounter);
					noDone = false;
					isDone = true;
				}
			}
		}
		lineCounter++;
		return line;
	}

	public void dumpOut(long time) {
		dumpOut.print(lineCounter + ";" + (time - basetime) + ";" + (time - lastMeasureTime));
		if (dumpMemory) {
			dumpOut.println(RUNTIME.freeMemory());
		} else {
			dumpOut.println();
		}
		dumpOut.flush();
	}

	@Override
	public T getNext() throws IOException {
		String line = getNextLine(reader);
		if (line != null) {
			return getDataHandler().readData(line);
		} else {
			return null;
		}
	}

	@Override
	public void write(T object) throws IOException {
		if (writer == null)
			getTransportHandler().send(object.toString().getBytes(getCharset().name()));
		else
			writer.write(object.toString());
	}

	private String bb_to_str(ByteBuffer buffer) {
		String data = "";
		try {
			data = getCharset().decode(buffer).toString();
			// reset buffer's position to its original so it is not altered:
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
		return data;
	}

	@Override
	public synchronized void process(long callerId, ByteBuffer message) {

		String strMsg = bb_to_str(message);
		StringBuilder currentInputString = currentInputStringMap.get(callerId);
		String data = currentInputString != null ? currentInputString + strMsg : strMsg;

		currentInputString = new StringBuilder();
		currentInputStringMap.put(callerId, currentInputString);

		for (char s : data.toCharArray()) {
			if (s == '\n' || s == '\r') {
				if (currentInputString.length() > 0) {
					// if first line has to be skipped, firstLinesToSkip is 1 and lineCounter is 0
					if (lineCounter < firstLinesToSkip) {
						lineCounter++;
						continue;
					} else {
						String token = currentInputString.toString();
						process(token);
					}
				}
				currentInputString = new StringBuilder();
				lineCounter++;
			} else {
				currentInputString.append(s);
			}
		}
		if (currentInputString.length() > 0) {
			currentInputStringMap.put(callerId, currentInputString);
		} else {
			currentInputStringMap.remove(callerId);
		}

	}

	public void process(String token) {
		try {
			T retValue = getDataHandler().readData(token);
			getTransfer().transfer(retValue);
		} catch (Exception e) {
			INFOSERVICE.warning("Cannot read line " + token, e);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.uniol.inf.is.odysseus.core.physicaloperator.access.protocol.
	 * AbstractProtocolHandler#process(java.lang.String[])
	 */
	@Override
	public void process(String[] message) {
		int i = 0;
		String line;
		try {
			while (i < message.length) {
				line = message[i++];
				if (lineCounter >= firstLinesToSkip) {
					process(line);
				}
				lineCounter++;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		;
	}

	@Override
	public void process(InputStream message) {
		BufferedReader r = new BufferedReader(new InputStreamReader(message));
		String line;
		try {
			while ((line = getNextLine(r)) != null) {
				if (lineCounter >= firstLinesToSkip) {
					process(line);
				}
				lineCounter++;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	protected void delay() {
		if (delayeach > 0) {
			delayCounter++;
			if (delayCounter < delayeach) {
				return;
			}
			delayCounter = 0;
		}
		if (delay > 0) {
			try {
				Thread.sleep(delay);
			} catch (InterruptedException e) {
				// interrupting the delay might be correct
				// e.printStackTrace();
			}
		} else {
			if (nanodelay > 0) {
				try {
					Thread.sleep(0L, nanodelay);
				} catch (InterruptedException e) {
					// interrupting the delay might be correct
					// e.printStackTrace();
				}
			}
		}
	}

	@Override
	public IProtocolHandler<T> createInstance(ITransportDirection direction, IAccessPattern access, OptionMap options,
			IStreamObjectDataHandler<T> dataHandler) {
		LineProtocolHandler<T> instance = new LineProtocolHandler<T>(direction, access, dataHandler, options);
		return instance;
	}

	@Override
	public String getName() {
		return NAME;
	}

	public long getDelay() {
		return delay;
	}

	public void setDelay(long delay) {
		this.delay = delay;
	}

	public void setNanodelay(int nanodelay) {
		this.nanodelay = nanodelay;
	}

	public int getNanodelay() {
		return nanodelay;
	}

	@Override
	public ITransportExchangePattern getExchangePattern() {
		if (this.getDirection() != null) {
			if (this.getDirection().equals(ITransportDirection.IN)) {
				return ITransportExchangePattern.InOnly;
			} else {
				return ITransportExchangePattern.OutOnly;
			}
		}
		return ITransportExchangePattern.Undefined;
	}

	public int getDelayeach() {
		return delayeach;
	}

	public void setDelayeach(int delayeach) {
		this.delayeach = delayeach;
	}

	@Override
	public boolean isDone() {
		if (noDone) {
			return false;
		} else {
			return isDone;
		}
	}

	@Override
	public boolean isSemanticallyEqualImpl(IProtocolHandler<?> o) {
		if (!(o instanceof LineProtocolHandler)) {
			return false;
		}
		LineProtocolHandler<?> other = (LineProtocolHandler<?>) o;
		if (this.nanodelay != other.getNanodelay() || this.delay != other.getDelay()
				|| this.delayeach != other.getDelayeach() || this.dumpEachLine != other.getDumpEachLine()
				|| this.measureEachLine != other.getMeasureEachLine() || this.lastLine != other.getLastLine()
				|| this.debug != other.isDebug() || this.readFirstLine != other.isReadFirstLine()) {
			return false;
		}
		return true;
	}

	public boolean isReadFirstLine() {
		return readFirstLine;
	}

	public long getDumpEachLine() {
		return dumpEachLine;
	}

	public long getLastLine() {
		return lastLine;
	}

	public boolean isDebug() {
		return debug;
	}

	public long getMeasureEachLine() {
		return measureEachLine;
	}
}
