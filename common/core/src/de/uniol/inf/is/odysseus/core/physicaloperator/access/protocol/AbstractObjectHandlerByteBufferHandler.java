package de.uniol.inf.is.odysseus.core.physicaloperator.access.protocol;

import java.io.IOException;
import java.nio.ByteBuffer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.uniol.inf.is.odysseus.core.collection.OptionMap;
import de.uniol.inf.is.odysseus.core.datahandler.IDataHandler;
import de.uniol.inf.is.odysseus.core.datahandler.IStreamObjectDataHandler;
import de.uniol.inf.is.odysseus.core.metadata.IMetaAttribute;
import de.uniol.inf.is.odysseus.core.metadata.IStreamObject;
import de.uniol.inf.is.odysseus.core.objecthandler.ByteBufferHandler;
import de.uniol.inf.is.odysseus.core.objecthandler.ObjectByteConverter;
import de.uniol.inf.is.odysseus.core.option.OptionParameter;
import de.uniol.inf.is.odysseus.core.physicaloperator.IPunctuation;
import de.uniol.inf.is.odysseus.core.physicaloperator.PunctuationFactory;
import de.uniol.inf.is.odysseus.core.physicaloperator.access.transport.IAccessPattern;
import de.uniol.inf.is.odysseus.core.physicaloperator.access.transport.ITransportDirection;

public abstract class AbstractObjectHandlerByteBufferHandler<T extends IStreamObject<? extends IMetaAttribute>> extends AbstractByteBufferHandler<T> {

	private static final Logger LOG = LoggerFactory
			.getLogger(AbstractObjectHandlerByteBufferHandler.class);

	public static final String OBJECT_SIZE="objectsize";
	@OptionParameter(name=OBJECT_SIZE, optional = true, defaultValue = ""+ByteBufferHandler.DEFAULT_BUFFER_SIZE, type = Integer.class, doc = "The initial size of the objects, that are treated")
	private int objectSize;

	final protected ByteBufferHandler<T> objectHandler;
	
	public AbstractObjectHandlerByteBufferHandler() {
		super();
		objectHandler = null;
	}

	public AbstractObjectHandlerByteBufferHandler(ITransportDirection direction, IAccessPattern access,
			IStreamObjectDataHandler<T> dataHandler, OptionMap optionsMap) {
		super(direction, access, dataHandler, optionsMap);
		handleAnnotations(optionsMap, AbstractObjectHandlerByteBufferHandler.class);
		objectHandler = new ByteBufferHandler<T>(dataHandler, objectSize);		
	}

	public AbstractObjectHandlerByteBufferHandler(ITransportDirection direction, IAccessPattern access,
			IStreamObjectDataHandler<T> dataHandler, OptionMap optionsMap, ByteBufferHandler<T> objectHandler) {
		super(direction, access, dataHandler, optionsMap);
		handleAnnotations(optionsMap, AbstractObjectHandlerByteBufferHandler.class);
		this.objectHandler = objectHandler;		
	}

	
	@Override
	public boolean hasNext() throws IOException {
		if (getTransportHandler().getInputStream() != null) {
			return getTransportHandler().getInputStream().available() > 0;
		} else {
			return false;
		}
	}

	protected ByteBuffer prepareObject(IPunctuation punctuation) {
		return convertPunctuation(punctuation);
	}

	public static ByteBuffer convertPunctuation(IPunctuation punctuation) {
		ByteBuffer buffer;
		byte puncNumber = punctuation.getNumber();
		IStreamObjectDataHandler<?> dataHandler = PunctuationFactory.getDataHandler(puncNumber);
		if (dataHandler != null) {
			buffer = ByteBuffer.allocate(1024);
			buffer.put(punctuation.getNumber());
			dataHandler.writeData(buffer, punctuation.getValue());
		}else {
			byte[] data = ObjectByteConverter.objectToBytes(punctuation);
			buffer = ByteBuffer.allocate(data.length+4);
			buffer.putInt(data.length);
			buffer.put(data);	
		}
		buffer.flip();
		return buffer;
	}

	protected ByteBuffer prepareObject(T object) {
		return convertObject(object, getDataHandler(), objectSize);
	}

	public static ByteBuffer convertObject(Object object, IDataHandler<?> dataHandler, int objectSize) {
		ByteBuffer buffer = ByteBuffer.allocate(objectSize);
		dataHandler.writeData(buffer, object);
		buffer.flip();
		return buffer;
	}


	protected void processObject() throws IOException, ClassNotFoundException {
		T object = objectHandler.create();
		if (object != null) {
			getTransfer().transfer(object);
		} else {
			LOG.error("Empty object");
		}
	}

}
