package de.uniol.inf.is.odysseus.core.physicaloperator.marker;

import java.util.ArrayList;
import java.util.List;

import de.uniol.inf.is.odysseus.core.collection.Tuple;
import de.uniol.inf.is.odysseus.core.metadata.PointInTime;
import de.uniol.inf.is.odysseus.core.physicaloperator.AbstractPunctuation;
import de.uniol.inf.is.odysseus.core.sdf.schema.SDFAttribute;
import de.uniol.inf.is.odysseus.core.sdf.schema.SDFDatatype;
import de.uniol.inf.is.odysseus.core.sdf.schema.SDFSchema;
import de.uniol.inf.is.odysseus.core.sdf.schema.SDFSchemaFactory;

public abstract class AbstractMarkerPunctuation extends AbstractPunctuation implements IMarkerPunctuation {

	private static final long serialVersionUID = 9167278768023212453L;

	final static byte NUMBER = 1;

	public static final SDFSchema schema;


	static {
		List<SDFAttribute> attributes = new ArrayList<SDFAttribute>();
		attributes.add(new SDFAttribute("Marker", "type", SDFDatatype.INTEGER));
		attributes.add(new SDFAttribute("Marker", "point", SDFDatatype.TIMESTAMP));
		schema = SDFSchemaFactory.createNewSchema("Marker", Tuple.class, attributes);
	}

	public AbstractMarkerPunctuation(long point) {
		super(point);
	}

	public AbstractMarkerPunctuation(PointInTime p) {
		super(p);
	}
	
	public AbstractMarkerPunctuation(AbstractPunctuation punct){
		super(punct);
	}
	
	@Override
	public byte getNumber() {
		return NUMBER;
	}

	@Override
	public String toString() {
		return "Marker " + getTime();
	}
	
	@Override
	public boolean isNodeLocal() {
		return true;
	}
	
	@Override
	public boolean isMarker() {
		return true;
	}

	@Override
	public SDFSchema getSchema() {
		return schema;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Tuple<?> getValue() {
		Tuple<?> ret = new Tuple(2, false);
		ret.setAttribute(0, NUMBER);
		ret.setAttribute(1, getTime().getMainPoint());
		return ret;
	}
}