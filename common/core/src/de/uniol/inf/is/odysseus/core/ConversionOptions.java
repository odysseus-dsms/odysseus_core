package de.uniol.inf.is.odysseus.core;

import java.lang.reflect.Field;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.uniol.inf.is.odysseus.core.collection.OptionMap;
import de.uniol.inf.is.odysseus.core.option.OptionParameter;

public class ConversionOptions {

	private static final Logger LOG = LoggerFactory.getLogger(ConversionOptions.class);

	private static final String BASE_64 = "base64";

	// Do not use default values for deprecated entries!
	public static final String DELIMITER = "delimiter";
	@OptionParameter(name = DELIMITER, defaultValue = "", defaultValueIsNull = true, optional = true, type = String.class, deprecated = true, doc = "The delimiter as char to use to separate entries. Default is ','")
	private String delimiterStr;

	public static final String CSV_DELIMITER = "csv.delimiter";
	@OptionParameter(name = CSV_DELIMITER, defaultValue = ",", optional = true, type = String.class, doc = "The delimiter as char to use to separate entries. Default is ','")
	private String csvDelimiterStr;

	private final char delimiter;

	public static final String TEXT_DELIMITER = "textdelimiter";
	public static final String CSV_TEXT_DELIMITER = "csv.textdelimiter";
	@OptionParameter(name = TEXT_DELIMITER, alias = CSV_TEXT_DELIMITER, defaultValue = "\"", defaultValueIsNull = false, optional = true, type = Character.class, deprecated = true, doc = "A char that starts and ends a text part, i.e. where the delimiter to separate entries is ignored")
	private Character textSeperator;

	public static final String CSV_FLOATING_FORMATTER = "csv.floatingformatter";
	@OptionParameter(name = CSV_FLOATING_FORMATTER, defaultValue = "", optional = true, type = String.class, doc = "If used for writing, each double/float value will be formatted using this formatter or read based on this format. See https://docs.oracle.com/javase/7/docs/api/java/text/DecimalFormat.html ")
	private String floatingFormatterString;
	final private NumberFormat floatingFormatter;

	public static final String DECIMAL_SEPARATOR = "decimalseparator";
	@OptionParameter(name = DECIMAL_SEPARATOR, defaultValue = "", optional = true, defaultValueIsNull = true, type = String.class, doc = "decimalseparator: How is the fraction of the number separated.")
	private String decimalSeparator;

	public static final String EXPONENT_SEPARATOR = "exponentseparator";
	@OptionParameter(name = EXPONENT_SEPARATOR, defaultValue = "", optional = true, defaultValueIsNull = true, type = String.class, doc = "How to separate the exponent, typically E.")
	private String exponentSeparator;

	public static final String GROUPING_SEPARATOR = "groupingseparator";
	@OptionParameter(name = GROUPING_SEPARATOR, defaultValue = "", optional = true, defaultValueIsNull = true, type = String.class, doc = "To separate thousends etc.")
	private String groupingSpearator;

	public static final String CSV_NUMBER_FORMATTER = "csv.numberformatter";
	@OptionParameter(name = CSV_NUMBER_FORMATTER, defaultValue = "", optional = true, defaultValueIsNull = true, type = String.class, doc = "If used for writing, each number other than double/float value will be formatted using this formatter (default null). See https://docs.oracle.com/javase/7/docs/api/java/text/DecimalFormat.html")
	private String numberFormatterString;
	final private NumberFormat numberFormatter;

	public static final String NULL_VALUE_TEXT = "nullvaluetext";
	@OptionParameter(name = NULL_VALUE_TEXT, defaultValue = "", optional = true, type = String.class, doc = "The representation of null values, e.g. '<NULL>'")
	String nullValueString;

	private static final String DEFAULT_CHARSET = "UTF-8";
	public static final String CHARSET = "charset";
	@OptionParameter(name = CHARSET, defaultValue = DEFAULT_CHARSET, optional = true, type = String.class, doc = "The charset to use.")
	private String charsetString;

	@OptionParameter(name = BASE_64, optional = true, defaultValue = "false", type = Boolean.class, doc = "Use base64 encoding.")
	private boolean base64 = false;

	private Charset charset;
	final CharsetDecoder decoder;
	final CharsetEncoder encoder;

	protected Set<OptionParameter> possibleParameter = new HashSet<>();

	public static final ConversionOptions defaultOptions = new ConversionOptions(',', '"',
			(NumberFormat) null, (NumberFormat) null);

	public ConversionOptions(char delimiter, Character textSeperator, NumberFormat floatingFormatter,
			NumberFormat numberFormatter) {
		super();

		this.charset = Charset.forName(DEFAULT_CHARSET);
		this.decoder = charset.newDecoder();
		this.encoder = charset.newEncoder();

		this.delimiter = delimiter;
		this.textSeperator = textSeperator;
		this.floatingFormatter = floatingFormatter;
		this.numberFormatter = numberFormatter;
		this.nullValueString = "null";

	}

	public ConversionOptions(OptionMap optionMap) {
		super();
		handleAnnotations(optionMap, ConversionOptions.class);

		this.charset = Charset.forName(charsetString);
		this.decoder = charset.newDecoder();
		this.encoder = charset.newEncoder();

		if (delimiterStr != null && delimiterStr.length() > 0) {
			delimiter = determineDelimiter(delimiterStr);
		} else {
			delimiter = determineDelimiter(csvDelimiterStr);
		}

		DecimalFormatSymbols dfs = createDFS();

		if (floatingFormatterString != null) {
			floatingFormatter = new DecimalFormat(floatingFormatterString, dfs);
		} else {
			floatingFormatter = null;
		}

		if (numberFormatterString != null) {
			numberFormatter = new DecimalFormat(numberFormatterString, dfs);
		} else {
			numberFormatter = null;
		}

	}

	protected void handleAnnotations(OptionMap options, Class<?> forClass) throws IllegalArgumentException {
		List<String> check = new ArrayList<>();
		for (Field field : forClass.getDeclaredFields()) {
			if (field.isAnnotationPresent(OptionParameter.class)) {
				OptionParameter ann = field.getAnnotation(OptionParameter.class);
				LOG.trace("Found annotation " + ann);
				if (!ann.optional()) {
					check.add(ann.name());
				}
				try {
					setField(field, ann, options);
				} catch (IllegalAccessException | URISyntaxException e) {
					throw new IllegalArgumentException("Error accessing options for " + this + ".", e);
				}

			}
		}
		if (!check.isEmpty()) {
			options.checkRequiredException(check);
		}
	}

	// must be done in this class, else field is not accessible
	protected void setField(Field field, OptionParameter ann, OptionMap options)
			throws IllegalArgumentException, IllegalAccessException, URISyntaxException {
		// a besser version possible?
		String name = ann.type().getSimpleName();
		field.setAccessible(true);
		if (ann.defaultValueIsNull() && !options.containsKey(ann.name())) {
			field.set(this, null);
		} else {
			switch (name) {
			case "String":
				field.set(this, options.getString(ann.name(), ann.alias(), ann.defaultValue()));
				break;
			case "Boolean":
				field.setBoolean(this, options.getBoolean(ann.name(), ann.alias(), ann.defaultValue()));
				break;
			case "Byte":
				field.setByte(this, options.getByte(ann.name(), ann.alias(), ann.defaultValue()));
				break;
			case "Character":
				field.set(this, options.getCharacter(ann.name(),ann.alias(), ann.defaultValue()));
				break;
			case "Integer":
				field.setInt(this, options.getInt(ann.name(), ann.alias(),ann.defaultValue()));
				break;
			case "Double":
				field.setDouble(this, options.getDouble(ann.name(),ann.alias(), ann.defaultValue()));
				break;
			case "Long":
				field.setLong(this, options.getLong(ann.name(), ann.alias(),ann.defaultValue()));
				break;
			case "Float":
				field.setFloat(this, options.getFloat(ann.name(),ann.alias(), ann.defaultValue()));
				break;
			case "URI":
				field.set(this, options.getURI(ann.name(), ann.alias(),ann.defaultValue()));
				break;
			default:
				throw new IllegalArgumentException("Cannot handle type " + name);
			}
		}
	}

	protected void determinePossibleParameters(Class<?> forClass) throws IllegalArgumentException {
		// TODO check, if a non required option has a default value --> Programming
		// error!
		for (Field field : forClass.getDeclaredFields()) {
			if (field.isAnnotationPresent(OptionParameter.class)) {
				OptionParameter ann = field.getAnnotation(OptionParameter.class);
				possibleParameter.add(ann);
			}
			if (ConversionOptions.class.isAssignableFrom(field.getType())) {
				// possibleParameter.addAll( );
			}
		}
	}

	public Set<OptionParameter> getPossibleParameters() {
		if (possibleParameter.isEmpty()) {
			Class<?> forClass = getClass();
			determinePossibleParameters(forClass);

			// and now for all upper classes (reflection does not provide members of upper
			// classes)
			while ((forClass = forClass.getSuperclass()) != Object.class) {
				determinePossibleParameters(forClass);
			}

		}
		return possibleParameter;
	}

	private DecimalFormatSymbols createDFS() {
		DecimalFormatSymbols dfs = DecimalFormatSymbols.getInstance();

		if (decimalSeparator != null) {
			dfs.setDecimalSeparator(decimalSeparator.toCharArray()[0]);
		}
		if (exponentSeparator != null) {
			dfs.setExponentSeparator(exponentSeparator);
		}
		if (groupingSpearator != null) {
			dfs.setGroupingSeparator(groupingSpearator.toCharArray()[0]);
		}
		return dfs;
	}

	public ConversionOptions(ConversionOptions convOpts, OptionMap optionMap) {

		// THIS IS UGLY (even before) ... this should be handled similar to
		// the OptionParameter handling!

		// this is a little bit tricky because of the new OptionParameter handling
		// create a new Options-Object with values from the optionMap

		ConversionOptions newOptions = new ConversionOptions(optionMap);

		// and replace in this class, only if the value was part of the optionMap
		if (optionMap.containsKey(CHARSET)) {
			this.charset = newOptions.getCharset();
		} else {
			this.charset = convOpts.charset;
		}
		this.decoder = charset.newDecoder();
		this.encoder = charset.newEncoder();

		if (optionMap.containsKey(ConversionOptions.DELIMITER)
				|| optionMap.containsKey(ConversionOptions.CSV_DELIMITER)) {
			delimiter = newOptions.getDelimiter();
		} else {
			delimiter = convOpts.delimiter;
		}

		if (optionMap.containsKey(ConversionOptions.CSV_FLOATING_FORMATTER)) {
			floatingFormatter = newOptions.floatingFormatter;
		} else {
			floatingFormatter = convOpts.floatingFormatter;
		}
		if (optionMap.containsKey(ConversionOptions.CSV_NUMBER_FORMATTER)) {
			numberFormatter = newOptions.numberFormatter;
		} else {
			numberFormatter = convOpts.numberFormatter;
		}

		if (optionMap.containsKey(TEXT_DELIMITER)) {
			this.textSeperator = newOptions.textSeperator;
		} else if (optionMap.containsKey(CSV_TEXT_DELIMITER)) {
			this.textSeperator = newOptions.textSeperator;
		} else {
			this.textSeperator = convOpts.textSeperator;
		}

		if (optionMap.containsKey(NULL_VALUE_TEXT)) {
			this.nullValueString = newOptions.nullValueString;
		} else {
			this.nullValueString = convOpts.nullValueString;
		}

		if (optionMap.containsKey(BASE_64)) {
			this.base64 = newOptions.base64;
		} else {
			this.base64 = convOpts.base64;
		}

		// Could there be any options that are new in optionMap that are not reflected
		// now?

	}

	public String getNullValueString() {
		return nullValueString;
	}

	public char getDelimiter() {
		return delimiter;
	}

	public Character getTextSeperator() {
		return textSeperator;
	}

	public boolean hasTextSeperator() {
		return textSeperator != null;
	}

	public NumberFormat getFloatingFormatter() {
		return floatingFormatter;
	}

	public boolean hasFloatingFormatter() {
		return floatingFormatter != null;
	}

	public NumberFormat getNumberFormatter() {
		return numberFormatter;
	}

	public boolean hasNumberFormatter() {
		return numberFormatter != null;
	}

	public Charset getCharset() {
		return charset;
	}

	public CharsetDecoder getDecoder() {
		return decoder;
	}

	public CharsetEncoder getEncoder() {
		return encoder;
	}

	public boolean isBase64() {
		return base64;
	}

	static public char determineDelimiter(String v) {
		char ret;
		if (v.equals("\\t")) {
			ret = '\t';
		} else {
			ret = v.toCharArray()[0];
		}
		return ret;
	}

}
