package de.uniol.inf.is.odysseus.core.physicaloperator.access.transport;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;

import de.uniol.inf.is.odysseus.core.collection.OptionMap;
import de.uniol.inf.is.odysseus.core.option.OptionParameter;
import de.uniol.inf.is.odysseus.core.physicaloperator.access.protocol.IProtocolHandler;

abstract public class AbstractFileHandler extends AbstractTransportHandler {


	protected InputStream in;
	protected OutputStream out;
	final private ByteBuffer writeBuffer;
	
	public static final String FILENAME = "filename";
	@OptionParameter(name="filename", type = String.class, doc="The filename that should be used")
	protected String filename;
	
	public static final String APPEND = "append";
	@OptionParameter(name=APPEND, type=Boolean.class, defaultValue = "false", optional = true, doc = "Append to an existing file or create a new one.")
	protected boolean append;
	
	public static final String WRITEDELAYSIZE = "writedelaysize";
	@OptionParameter(name = WRITEDELAYSIZE, type=Integer.class, defaultValue = "0", optional=true, doc="Write to file after this number of elements.")
	protected int writeDelaySize = 0;
	
	public AbstractFileHandler() {
		super();
		writeDelaySize = 0;
		writeBuffer = null;
	}
	
	public AbstractFileHandler(IProtocolHandler<?> protocolHandler,
			OptionMap options) {
		super(protocolHandler, options);
		
		handleAnnotations(options, AbstractFileHandler.class);
		
		filename = convertForOS(filename);
		
		if (writeDelaySize > 0) {
			this.writeBuffer = ByteBuffer.allocate(writeDelaySize);
		} else {
			this.writeBuffer = null;
		}

	}

	protected String convertForOS(String filename) {
		char thisos = File.separatorChar;
		if (thisos == '/') {
			filename = filename.replace('\\', thisos);
		} else {
			filename = filename.replace('/', thisos);
		}
		return filename;
	}

	@Override
	public synchronized void send(byte[] message) throws IOException {
		if (writeDelaySize > 0) {
			int msgL = message.length;
			// Would message plus current buffer content exceed buffers
			// capacity?
			if (msgL + writeBuffer.position() > writeDelaySize) {

				dumpBuffer();
				// To avoid double writes, write content only if message
				// does not
				// fit into buffer
				if (msgL > writeDelaySize) {
					out.write(message);
				} else {
					writeBuffer.put(message);
				}
			} else {
				writeBuffer.put(message);
			}
		} else {
			out.write(message);
		}
	}

	private void dumpBuffer() throws IOException {
		byte[] copy = new byte[writeBuffer.position()];
		writeBuffer.flip();
		writeBuffer.get(copy, 0, writeBuffer.limit());
		out.write(copy);
		writeBuffer.clear();
	}

	@Override
	public InputStream getInputStream() {
		return in;
	}

	@Override
	public boolean isDone() {
		try {
			return in.available() == 0;
		} catch (IOException e) {
			return true;
		}
	}
	
	@Override
	public OutputStream getOutputStream() {
		return out;
	}

	@Override
	public void processInStart() {

	}

	@Override
	public void processInClose() throws IOException {
		fireOnDisconnect();
		in.close();
	}

	@Override
	public synchronized void processOutClose() throws IOException {
		fireOnDisconnect();

		if (writeDelaySize > 0)
			dumpBuffer();

		out.flush();
		out.close();
		out = null;
	}

}
