package de.uniol.inf.is.odysseus.core.physicaloperator;

import java.io.Serializable;

import de.uniol.inf.is.odysseus.core.collection.Tuple;
import de.uniol.inf.is.odysseus.core.metadata.IStreamable;
import de.uniol.inf.is.odysseus.core.metadata.ITimeComparable;
import de.uniol.inf.is.odysseus.core.metadata.PointInTime;
import de.uniol.inf.is.odysseus.core.sdf.schema.SDFSchema;

public interface IPunctuation extends ITimeComparable, IStreamable, Serializable, Comparable<IPunctuation> {

	/**
	 * Every punctuation needs a time to allow the ordering regarding the stream
	 * @return
	 */
	PointInTime getTime();
	
	/**
	 * States that this punctuation is a heartbeat (i.e. states time progress). This is used to avoid
	 * instanceof calls at runtime. If the element returns true, sequent punctuations may compress (i.e.
	 * only the youngest punctuation is send)
	 * @return
	 */
	boolean isHeartbeat();
	
	/**
	 * States that this punctuation is a marker. This is used to avoid
	 * instanceof calls at runtime.
	 * @return
	 */
	boolean isMarker();
	
	byte getNumber();
	
	/**
	 * 
	 * @return
	 */
	SDFSchema getSchema();
	
	Tuple<?> getValue();
		
	IPunctuation clone();

	IPunctuation clone(PointInTime p_start);
	
	default boolean before(IPunctuation time) {
		return this.before(time.getTime());
	}
	
	default boolean beforeOrEquals(IPunctuation time) {
		return this.beforeOrEquals(time.getTime());
	}
	
	default boolean after(IPunctuation time) {
		return this.after(time.getTime());
	}
	
	default boolean afterOrEquals(IPunctuation time) {
		return this.afterOrEquals(time.getTime());
	}
	
	default int compareTo(IPunctuation p) {
		return this.getTime().compareTo(p.getTime());
	}
	
	boolean isNodeLocal();
	
}
