package de.uniol.inf.is.odysseus.core.physicaloperator.access.protocol;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.UnknownHostException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.uniol.inf.is.odysseus.core.collection.OptionMap;
import de.uniol.inf.is.odysseus.core.datahandler.IStreamObjectDataHandler;
import de.uniol.inf.is.odysseus.core.metadata.IMetaAttribute;
import de.uniol.inf.is.odysseus.core.metadata.IStreamObject;
import de.uniol.inf.is.odysseus.core.option.OptionParameter;
import de.uniol.inf.is.odysseus.core.physicaloperator.access.transport.IAccessPattern;
import de.uniol.inf.is.odysseus.core.physicaloperator.access.transport.ITransportDirection;
import de.uniol.inf.is.odysseus.core.physicaloperator.access.transport.ITransportExchangePattern;
import de.uniol.inf.is.odysseus.core.physicaloperator.access.transport.ITransportHandler;

/**
 * Protocol handler to read the complete content of an input stream
 *
 * @author Christian Kuka <christian@kuka.cc>
 *
 * @param <T>
 */
public class DocumentProtocolHandler<T extends IStreamObject<? extends IMetaAttribute>> extends AbstractProtocolHandler<T> {
	
	static final String REMOVE_NEWLINES = "removeNewlines";
	
	Logger LOG = LoggerFactory.getLogger(DocumentProtocolHandler.class);

	@OptionParameter(name="delay", optional = true, type = Long.class, defaultValue = "0", doc = "Delay of reading in milliseconds")
	private long delay;
	@OptionParameter(name="nanodelay", optional = true, type = Integer.class, defaultValue = "0", doc = "Delay of reading in nanoseconds")	
	private int nanodelay;
	@OptionParameter(name="delayeach", optional = true, type = Integer.class, defaultValue = "0", doc = "Delay after reading n documents")		
	private int delayeach = 0;
	@OptionParameter(name="dumpeachdocument", optional = true, type = Long.class, defaultValue = "-1", doc = "Log each nth document")
	private long dumpEachDocument = -1;
	@OptionParameter(name="measureeachdocument", optional = true, type = Long.class, defaultValue = "-1", doc = "Log time measurement after each nth document")	
	private long measureEachDocument = -1;
	@OptionParameter(name="maxdocument", optional = true, type = Long.class, defaultValue = "-1", doc = "Stop reading after this number to documents")		
	private long lastDocument = -1;
	@OptionParameter(name="debug", optional = true, type=Boolean.class, defaultValue = "false", doc ="dump, measure and maxDocument are only treated if debug is true")
	private boolean debug = false;
	@OptionParameter(name="onedocpercall", optional = true, type=Boolean.class, defaultValue = "true", doc = "If set to true (default) there will be an complete open/process/close call on the underlying transporthandler for each new element. If set to false, the source (transporthandler) will only be called once.")
	private boolean oneDocPerCall = true;
	@OptionParameter(name="removenewlines", optional = true, type=Boolean.class, defaultValue = "false", doc = "Should newlines be removed in the output?")
	private boolean removeNewlines = false;


	protected BufferedReader reader;
	protected BufferedWriter writer;
	private long delayCounter = 0L;
	protected long documentCounter = 0L;
	private boolean isDone = false;
	private StringBuilder measurements = new StringBuilder("");
	private long lastDumpTime = 0;
	private long basetime;


	public DocumentProtocolHandler() {
		super();
	}

	public DocumentProtocolHandler(ITransportDirection direction,
			IAccessPattern access, IStreamObjectDataHandler<T> dataHandler, OptionMap optionsMap) {
		super(direction, access, dataHandler, optionsMap);
		init_internal();
	}

	private void init_internal() {
		handleAnnotations(optionsMap, DocumentProtocolHandler.class);		
		lastDumpTime = System.currentTimeMillis();
	}


	@Override
	public void open() throws UnknownHostException, IOException {
		if (!oneDocPerCall) {
			intern_open();
		}
	}

	private void intern_open() throws UnknownHostException, IOException {
		getTransportHandler().open();
		if (getDirection().equals(ITransportDirection.IN)) {
			if ((this.getAccessPattern().equals(IAccessPattern.PULL))
					|| (this.getAccessPattern().equals(IAccessPattern.ROBUST_PULL))) {
				reader = new BufferedReader(new InputStreamReader(
						getTransportHandler().getInputStream()));
			}
		} else {
			writer = new BufferedWriter(new OutputStreamWriter(
					getTransportHandler().getOutputStream()));
		}
		delayCounter = 0;
		documentCounter = 0;
		isDone = false;
	}

	@Override
	public void close() throws IOException {
		if (!oneDocPerCall) {
			intern_close();
		}
	}

	private void intern_close() throws IOException {
		try {
			if (getDirection().equals(ITransportDirection.IN)) {
				if (reader != null) {
					reader.close();
				}
			} else {
				if (writer != null) {
					writer.close();
				}
			}
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			throw new IOException(e);
		} finally {
			getTransportHandler().close();
		}
	}

	@Override
	public boolean hasNext() throws IOException {
		// if (reader.ready() == false) {
		// isDone = true;
		// return false;
		// }
		return true;
	}

	@Override
	public T getNext() throws IOException {
		String result = null;
		delay();
		if (oneDocPerCall) {
			intern_open();
		}
		if (reader.ready()) {
			StringBuilder builder = new StringBuilder();
			String line;
			while ((line = reader.readLine()) != null) {
				builder.append(line).append("\n");
			}
			// remove last "\n"
			result = builder.subSequence(0, builder.length() - 1).toString();

			if (debug) {
				if (dumpEachDocument > 0) {
					if (documentCounter % dumpEachDocument == 0) {
						long time = System.currentTimeMillis();
						LOG.debug(documentCounter + " " + time + " "
								+ (time - lastDumpTime) + " " + line);
						lastDumpTime = time;
					}
				}
				if (measureEachDocument > 0) {
					if (documentCounter % measureEachDocument == 0) {
						long time = System.currentTimeMillis();
						measurements.append(documentCounter).append(";")
								.append(time - basetime).append("\n");
					}
				}

				if (lastDocument == documentCounter || documentCounter == 0) {
					long time = System.currentTimeMillis();
					if (documentCounter == 0) {
						basetime = time;
					}
					LOG.debug(documentCounter + " " + time);
					measurements.append(documentCounter).append(";")
							.append(time - basetime).append("\n");
					if (lastDocument == documentCounter) {
						System.out.println(measurements);
						isDone = true;
					}
				}
				documentCounter++;
			}
		}
		if (oneDocPerCall) {
			intern_close();
		}
		if (result != null) {
			if (removeNewlines) {
				result = result.replace("\n", "").replace("\r", "");
			}
			return getDataHandler().readData(result);
		} else {
			return null;
		}
	}

	@Override
	public void write(T object) throws IOException {
		writer.write(object.toString());
	}

	@Override
	public boolean isDone() {
		return isDone;
	}

	@Override
	public void onDisonnect(ITransportHandler caller) {
		if (!oneDocPerCall){
			super.onDisonnect(caller);
		}
	}

	@Override
	public IProtocolHandler<T> createInstance(ITransportDirection direction,
			IAccessPattern access, OptionMap options,
			IStreamObjectDataHandler<T> dataHandler) {
		DocumentProtocolHandler<T> instance = new DocumentProtocolHandler<T>(
				direction, access, dataHandler, options);
		return instance;
	}

	@Override
	public String getName() {
		return "Document";
	}

//	public long getDelay() {
//		return delay;
//	}
//
//	public void setDelay(long delay) {
//		this.delay = delay;
//	}
//
//	public void setNanodelay(int nanodelay) {
//		this.nanodelay = nanodelay;
//	}
//
//	public int getNanodelay() {
//		return nanodelay;
//	}
//
//	public int getDelayeach() {
//		return delayeach;
//	}
//
//	public void setDelayeach(int delayeach) {
//		this.delayeach = delayeach;
//	}

	@Override
	public ITransportExchangePattern getExchangePattern() {
		if (this.getDirection() != null && this.getDirection().equals(ITransportDirection.IN)) {
			return ITransportExchangePattern.InOnly;
		} else {
			return ITransportExchangePattern.OutOnly;
		}
	}


	protected void delay() {
		if (delayeach > 0) {
			delayCounter++;
			if (delayCounter < delayeach) {
				return;
			}
			delayCounter = 0;
		}
		if (delay > 0) {
			try {
				Thread.sleep(delay);
			} catch (InterruptedException e) {
				LOG.debug(e.getMessage(), e);
			}
		} else {
			if (nanodelay > 0) {
				try {
					Thread.sleep(0L, nanodelay);
				} catch (InterruptedException e) {
					LOG.debug(e.getMessage(), e);
				}
			}
		}
	}

	@Override
	public boolean isSemanticallyEqualImpl(IProtocolHandler<?> o) {
		if(!(o instanceof DocumentProtocolHandler)) {
			return false;
		}
		DocumentProtocolHandler<?> other = (DocumentProtocolHandler<?>)o;
		if(this.nanodelay != other.nanodelay ||
				this.delay != other.delay ||
				this.delayeach != other.delayeach ||
				this.dumpEachDocument != other.getDumpEachDocument() ||
				this.measureEachDocument != other.getMeasureEachDocument() ||
				this.lastDocument != other.getLastDocument() ||
				this.debug != other.isDebug() ||
				this.oneDocPerCall != other.isOneDocPerCall()) {
			return false;
		}
		return true;
	}

	public long getDumpEachDocument() {
		return dumpEachDocument;
	}

	public long getLastDocument() {
		return lastDocument;
	}

	public boolean isDebug() {
		return debug;
	}

	public long getMeasureEachDocument() {
		return measureEachDocument;
	}

	public boolean isOneDocPerCall() {
		return oneDocPerCall;
	}
}
