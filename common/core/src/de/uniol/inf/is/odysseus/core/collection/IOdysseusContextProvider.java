package de.uniol.inf.is.odysseus.core.collection;

import java.util.Map;

public interface IOdysseusContextProvider {
	Map<String, String> getValues();
}
