package de.uniol.inf.is.odysseus.core.physicaloperator;

import java.util.ArrayList;
import java.util.List;

import de.uniol.inf.is.odysseus.core.collection.Tuple;
import de.uniol.inf.is.odysseus.core.metadata.PointInTime;
import de.uniol.inf.is.odysseus.core.sdf.schema.SDFAttribute;
import de.uniol.inf.is.odysseus.core.sdf.schema.SDFDatatype;
import de.uniol.inf.is.odysseus.core.sdf.schema.SDFSchema;
import de.uniol.inf.is.odysseus.core.sdf.schema.SDFSchemaFactory;

final public class ClearStatePunctuation extends AbstractPunctuation {

	private static final long serialVersionUID = 7733672970811260652L;
	public final static byte NUMBER = 3;

	public static final SDFSchema schema;

	static {
		List<SDFAttribute> attributes = new ArrayList<SDFAttribute>();
		attributes.add(new SDFAttribute("ClearState", "type", SDFDatatype.INTEGER));
		attributes.add(new SDFAttribute("ClearState", "point", SDFDatatype.TIMESTAMP));
		schema = SDFSchemaFactory.createNewSchema("ClearState", Tuple.class, attributes);
	}

	private ClearStatePunctuation(long point) {
		super(point);
	}

	private ClearStatePunctuation(PointInTime point) {
		super(point);
	}

	@Override
	public boolean isHeartbeat() {
		return false;
	}

	@Override
	public AbstractPunctuation clone() {
		return this;
	}

	@Override
	public AbstractPunctuation clone(PointInTime point) {
		return new ClearStatePunctuation(point);
	}

	@Override
	public byte getNumber() {
		return NUMBER;
	}

	@Override
	public String toString() {
		return "Clearstate " + getTime();
	}

	static public ClearStatePunctuation createNewInstance(long point) {
		return new ClearStatePunctuation(point);
	}

	static public ClearStatePunctuation createNewInstance(Tuple<?> input) {
		return new ClearStatePunctuation(new PointInTime((long) input.getAttribute(1)));
	}

	static public ClearStatePunctuation createNewInstance(PointInTime point) {
		return new ClearStatePunctuation(point);
	}

	@Override
	public SDFSchema getSchema() {
		return schema;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Tuple<?> getValue() {
		Tuple<?> ret = new Tuple(2, false);
		ret.setAttribute(0, NUMBER);
		ret.setAttribute(1, getTime().getMainPoint());
		return ret;
	}

}
