package de.uniol.inf.is.odysseus.core.util;

import java.io.IOException;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.uniol.inf.is.odysseus.core.Activator;

public class ClassHelper {
	
	private static final Logger LOG = LoggerFactory.getLogger(ClassHelper.class);

	public static Class<?> resolveClass(String name) throws IOException, ClassNotFoundException {
		Class<?> ret = null;
		Objects.requireNonNull(Activator.getBundleContext(), "BundleContext not set!");

		try {
			ret = Class.forName(name);
		}catch(ClassNotFoundException ex) {
			// ignore here
		}

		if (ret == null) {
			try {
				ret = BundleClassLoading.findClass(name, Activator.getBundleContext().getBundle());
				return ret;
			} catch (ClassNotFoundException e) {
				LOG.error("Unable to find class " + name + " " + e);
			} catch (NullPointerException e) {
				LOG.error("Nullpointer finding class " + name, e);
			}
		}
		if (ret == null) {
			throw new ClassNotFoundException(name);
		}
		return ret;
	}
	
}
