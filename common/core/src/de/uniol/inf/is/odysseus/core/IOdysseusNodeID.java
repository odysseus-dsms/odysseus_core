package de.uniol.inf.is.odysseus.core;

import java.io.Serializable;
import java.util.UUID;

public interface IOdysseusNodeID extends Comparable<IOdysseusNodeID>, Serializable{
	UUID getUUID();

}
