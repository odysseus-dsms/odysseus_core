package de.uniol.inf.is.odysseus.core.physicaloperator;

import java.util.HashMap;
import java.util.Map;

import de.uniol.inf.is.odysseus.core.collection.Tuple;
import de.uniol.inf.is.odysseus.core.datahandler.DataHandlerRegistry;
import de.uniol.inf.is.odysseus.core.datahandler.IStreamObjectDataHandler;
import de.uniol.inf.is.odysseus.core.metadata.PointInTime;
import de.uniol.inf.is.odysseus.core.physicaloperator.access.transport.NewFilenamePunctuation;

/**
 * A helper class to handle different punctuation types at a common place
 * 
 * @author Marco Grawunder
 *
 */

public class PunctuationFactory {

	static public final Map<Byte, IStreamObjectDataHandler<?>> dataHandlerList = new HashMap<>();
	static public final Map<String, Byte> typeForName = new HashMap<>();
	static
	{
		dataHandlerList.put(Heartbeat.NUMBER,
				DataHandlerRegistry.instance.getStreamObjectDataHandler("tuple", Heartbeat.schema));
		typeForName.put("heartbeat", Heartbeat.NUMBER);
		dataHandlerList.put(NewFilenamePunctuation.NUMBER,
				DataHandlerRegistry.instance.getStreamObjectDataHandler("tuple", NewFilenamePunctuation.schema));
		typeForName.put("newfilename", NewFilenamePunctuation.NUMBER);
		dataHandlerList.put(ClearStatePunctuation.NUMBER,
				DataHandlerRegistry.instance.getStreamObjectDataHandler("tuple", ClearStatePunctuation.schema));
		typeForName.put("clearstate", ClearStatePunctuation.NUMBER);
	}

	
	public static IPunctuation createNewInstance(byte number, long time) {
		switch (number) {
		case Heartbeat.NUMBER:
			return Heartbeat.createNewInstance(time);
		case ClearStatePunctuation.NUMBER:
			return ClearStatePunctuation.createNewInstance(time);
		default:
			return null;
		}
	}

	public static IPunctuation createNewInstance(byte number, PointInTime time) {
		switch (number) {
		case Heartbeat.NUMBER:
			return Heartbeat.createNewInstance(time);
		case ClearStatePunctuation.NUMBER:
			return ClearStatePunctuation.createNewInstance(time);
		default:
			return null;
		}
	}

	public static IPunctuation createNewInstance(byte number, Tuple<?> input) {
		switch (number) {
		case Heartbeat.NUMBER:
			return Heartbeat.createNewInstance(input);
		case ClearStatePunctuation.NUMBER:
			return ClearStatePunctuation.createNewInstance(input);
		case NewFilenamePunctuation.NUMBER:
			return NewFilenamePunctuation.createNewInstance(input);
		default:
			return null;
		}
	}

	public static IStreamObjectDataHandler<?> getDataHandler(byte type) {
		return dataHandlerList.get(type);
	}

	public static byte getType(String punctuationType) {
		Byte type = typeForName.get(punctuationType.toLowerCase());
		if (type == null) {
			return -1;
		}
		return type;		
	}

}
