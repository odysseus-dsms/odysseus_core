package de.uniol.inf.is.odysseus.core.collection;

import java.util.List;

/**
 * A class than is used for expression processing
 * atm hold values for the last read and for the last written objects
 * @author Marco
 *
 */
public class History <T> {
	
	List<T> readObjects;
	List<T> writtenObjects;
	
	public History(List<T> readObjects) {
		this.readObjects = readObjects;
	}
	
	public List<T> getRead() {
		return readObjects;
	}
	
	public void setRead(List<T> readObjects) {
		this.readObjects = readObjects;
	}
	
	public List<T> getWrite(){
		return writtenObjects;
	}
	
	public void setWrite(List<T> writtenObjects) {
		this.writtenObjects = writtenObjects;
	}
	

}
