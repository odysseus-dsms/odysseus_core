package de.uniol.inf.is.odysseus.core.physicaloperator.marker;

import de.uniol.inf.is.odysseus.core.physicaloperator.IPhysicalOperator;

public interface IMarkerManager {

	public void init(IPhysicalOperator po, String id, int size);

	public IMarkerPunctuation createMarker(IMarkerPunctuation marker, int port);
}
