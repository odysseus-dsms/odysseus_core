package de.uniol.inf.is.odysseus.core.physicaloperator.marker;

import java.util.Collection;

import de.uniol.inf.is.odysseus.core.physicaloperator.IPhysicalOperator;
import de.uniol.inf.is.odysseus.core.physicaloperator.IPunctuation;

public interface IMarkerPunctuation extends IPunctuation {
	
	public void merge(Collection<IMarkerPunctuation> markerPunctuations);
	
	public void processOperator(IPhysicalOperator po, String id);

}
