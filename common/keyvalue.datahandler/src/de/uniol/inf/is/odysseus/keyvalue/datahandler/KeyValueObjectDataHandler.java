package de.uniol.inf.is.odysseus.keyvalue.datahandler;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.uniol.inf.is.odysseus.core.WriteOptions;
import de.uniol.inf.is.odysseus.core.datahandler.AbstractStreamObjectDataHandler;
import de.uniol.inf.is.odysseus.core.datahandler.IDataHandler;
import de.uniol.inf.is.odysseus.core.datahandler.StringHandler;
import de.uniol.inf.is.odysseus.core.sdf.schema.SDFSchema;
import de.uniol.inf.is.odysseus.keyvalue.datatype.KeyValueObject;
import de.uniol.inf.is.odysseus.keyvalue.datatype.SDFKeyValueDatatype;

/**
 *
 * @author Marco Grawunder
 *
 */

public class KeyValueObjectDataHandler extends AbstractStreamObjectDataHandler<KeyValueObject<?>> {

	protected static List<String> types = new ArrayList<String>();
	protected static final Logger LOG = LoggerFactory.getLogger(KeyValueObjectDataHandler.class);

	static {
		types.add(SDFKeyValueDatatype.KEYVALUEOBJECT.getURI());
	}

	private StringHandler dataHandler = new StringHandler();
	
	@Override
	public List<String> getSupportedDataTypes() {
		return types;
	}

	@Override
	public IDataHandler<KeyValueObject<?>> getInstance(SDFSchema schema) {
		return new KeyValueObjectDataHandler();
	}

	@Override
	public Class<?> createsType() {
		return KeyValueObject.class;
	}

	@Override
	public KeyValueObject<?> readData(ByteBuffer buffer, boolean handleMetaData) {
		String v = dataHandler.readData(buffer);
		return KeyValueObject.createInstance(v);
	}

	@Override
	public KeyValueObject<?> readData(InputStream inputStream, boolean handleMetaData) throws IOException {
		String v = dataHandler.readData(inputStream);
		return KeyValueObject.createInstance(v);
	}

	@Override
	public KeyValueObject<?> readData(Iterator<String> input, boolean handleMetaData) {
		String v = dataHandler.readData(input.next());
		return KeyValueObject.createInstance(v);
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void writeData(ByteBuffer buffer, Object data, boolean handleMetaData) {
		dataHandler.writeData(buffer, ((KeyValueObject)data).toString(handleMetaData));		
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void writeData(ByteBuffer buffer, KeyValueObject<?> object, boolean handleMetaData) {
		dataHandler.writeData(buffer, ((KeyValueObject)object).toString(handleMetaData));
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void writeData(StringBuilder string, Object data, boolean handleMetaData) {
		dataHandler.writeData(string, ((KeyValueObject)data).toString(handleMetaData));
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void writeData(List<String> output, Object data, boolean handleMetaData, WriteOptions options) {
		dataHandler.writeData(output, ((KeyValueObject)data).toString(handleMetaData), options);
	}

	@Override
	public int memSize(Object attribute, boolean handleMetaData) {
		return dataHandler.memSize(attribute);
	}

}
