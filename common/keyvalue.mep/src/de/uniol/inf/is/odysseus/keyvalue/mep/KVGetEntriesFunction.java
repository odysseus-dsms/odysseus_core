package de.uniol.inf.is.odysseus.keyvalue.mep;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import de.uniol.inf.is.odysseus.core.collection.Tuple;
import de.uniol.inf.is.odysseus.core.metadata.IMetaAttribute;
import de.uniol.inf.is.odysseus.core.sdf.schema.SDFDatatype;
import de.uniol.inf.is.odysseus.keyvalue.datatype.KeyValueObject;
import de.uniol.inf.is.odysseus.keyvalue.datatype.SDFKeyValueDatatype;
import de.uniol.inf.is.odysseus.mep.AbstractFunction;

/**
 * MEP function to retrieve all entries as tuples with key (String) and value
 * (Object) from a key value object.
 * 
 * @author Michael Brand (michael.brand@uol.de)
 *
 */
public class KVGetEntriesFunction extends AbstractFunction<List<Tuple<IMetaAttribute>>> {

	/**
	 * The version of this class for serialization.
	 */
	private static final long serialVersionUID = 5167527548933691923L;

	/**
	 * The name of the MEP function to be used in query languages.
	 */
	private static final String name = "getEntries";

	/**
	 * The amount of input values.
	 */
	private static final int numInputs = 1;

	/**
	 * The expected data types of the inputs. One row for each input. Different data
	 * types in a row mark different possible data types for the input.
	 */
	private static final SDFDatatype[][] inputTypes = new SDFDatatype[][] { { SDFKeyValueDatatype.KEYVALUEOBJECT } };

	/**
	 * The data type of the outputs.
	 */
	private static final SDFDatatype outputType = SDFDatatype.LIST_TUPLE;

	/**
	 * Creates a new MEP function.
	 */
	public KVGetEntriesFunction() {
		super(name, numInputs, inputTypes, outputType);
	}

	@Override
	public List<Tuple<IMetaAttribute>> getValue() {
		KeyValueObject<IMetaAttribute> kv = getInputValue(0);
		return new ArrayList<>(kv.getAsKeyValueMap().entrySet().stream().map(e -> {
			Tuple<IMetaAttribute> t = new Tuple<IMetaAttribute>(2, false);
			t.setAttribute(0, e.getKey());
			t.setAttribute(1, e.getValue());
			return t;
		}).collect(Collectors.toList()));
	}	

}