package de.uniol.inf.is.odysseus.keyvalue.mep;

import java.util.List;

import de.uniol.inf.is.odysseus.core.collection.Tuple;
import de.uniol.inf.is.odysseus.core.metadata.IMetaAttribute;
import de.uniol.inf.is.odysseus.core.sdf.schema.SDFDatatype;
import de.uniol.inf.is.odysseus.keyvalue.datatype.KeyValueObject;
import de.uniol.inf.is.odysseus.keyvalue.datatype.SDFKeyValueDatatype;
import de.uniol.inf.is.odysseus.mep.AbstractFunction;

public class KVGetElements2Function extends AbstractFunction<Tuple<IMetaAttribute>> {

	private static final long serialVersionUID = -499074489296265903L;
	private static final SDFDatatype[][] acceptedTypes = new SDFDatatype[][] {
		new SDFDatatype[] { SDFKeyValueDatatype.KEYVALUEOBJECT}, {SDFDatatype.LIST_STRING}};

	public KVGetElements2Function(){
		super("getElements", 2, acceptedTypes, SDFDatatype.OBJECT, false);
	}

	@Override
	public Tuple<IMetaAttribute> getValue() {
		KeyValueObject<?> kv = getInputValue(0);
		List<String> keys = getInputValue(1);
		Tuple<IMetaAttribute> tuple = null;
		if (keys != null){
			tuple = new Tuple<>(keys.size(), false);
			for(int i=0;i<keys.size();i++){
				 tuple.setAttribute(i, kv.getAttribute(keys.get(i)));
			}
		}
		return tuple;

	}



}
