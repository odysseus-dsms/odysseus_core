package de.uniol.inf.is.odysseus.keyvalue.mep;

import java.util.ArrayList;
import java.util.List;

import de.uniol.inf.is.odysseus.core.metadata.IMetaAttribute;
import de.uniol.inf.is.odysseus.core.sdf.schema.SDFDatatype;
import de.uniol.inf.is.odysseus.keyvalue.datatype.KeyValueObject;
import de.uniol.inf.is.odysseus.keyvalue.datatype.SDFKeyValueDatatype;
import de.uniol.inf.is.odysseus.mep.AbstractFunction;

/**
 * MEP function to retrieve all values from a key value object.
 * 
 * @author Michael Brand (michael.brand@uol.de)
 *
 */
public class KVGetValuesFunction extends AbstractFunction<List<Object>> {

	/**
	 * The version of this class for serialization.
	 */
	private static final long serialVersionUID = -3086496498246050992L;

	/**
	 * The name of the MEP function to be used in query languages.
	 */
	private static final String name = "getValues";

	/**
	 * The amount of input values.
	 */
	private static final int numInputs = 1;

	/**
	 * The expected data types of the inputs. One row for each input. Different data
	 * types in a row mark different possible data types for the input.
	 */
	private static final SDFDatatype[][] inputTypes = new SDFDatatype[][] { { SDFKeyValueDatatype.KEYVALUEOBJECT } };

	/**
	 * The data type of the outputs.
	 */
	private static final SDFDatatype outputType = SDFDatatype.LIST;

	/**
	 * Creates a new MEP function.
	 */
	public KVGetValuesFunction() {
		super(name, numInputs, inputTypes, outputType);
	}

	@Override
	public List<Object> getValue() {
		KeyValueObject<IMetaAttribute> kv = getInputValue(0);
		return new ArrayList<Object>(kv.getAsKeyValueMap().values());
	}

}