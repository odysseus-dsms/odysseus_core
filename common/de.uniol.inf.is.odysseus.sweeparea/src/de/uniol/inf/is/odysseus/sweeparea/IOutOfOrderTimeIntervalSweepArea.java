package de.uniol.inf.is.odysseus.sweeparea;

import java.util.Iterator;
import java.util.List;

import de.uniol.inf.is.odysseus.core.metadata.PointInTime;


/**
 * Interface for SweepAreas that support operations needed for OutOfOrder processing.
 * @author Joost van Mark
 *
 * @param <T>
 */
public interface IOutOfOrderTimeIntervalSweepArea<T> extends ITimeIntervalSweepArea<T> {
	
	
	/**
	 * Extracts all Elements before the cutOf.
	 * Elements whose TimeInterval lies on both sides of the cutOf will be split in two.
	 * The Part that lies after the cutOf will remain in the SweepArea, with an Interval of [cutOf, end).
	 * The part that lies before the cutOf will be extracted with an Interval of [start, cutOf).  
	 * @param cutOf
	 * @return Iterator over extracted Elements.
	 */
	Iterator<T>  extractAndCutStartingBefore(PointInTime cutOf);
	
	/**
	 * Extracts all Elements before the cutOf.
	 * Elements whose TimeInterval lies on both sides of the cutOf will be split in two.
	 * The Part that lies after the cutOf will remain in the SweepArea, with an Interval of [cutOf, end).
	 * The part that lies before the cutOf will be extracted with an Interval of [start, cutOf).  
	 * @param cutOf
	 * @return List over extracted Elements.
	 */
	List<T>  extractAndCutStartingBeforeAsList(PointInTime cutOf);
	
	/**
	 * Extracts all Elements that overlap with the Element and have the same value
	 * @param element
	 * @return
	 */
	Iterator<T> extractOverlappingAndEquals(T element);

}
