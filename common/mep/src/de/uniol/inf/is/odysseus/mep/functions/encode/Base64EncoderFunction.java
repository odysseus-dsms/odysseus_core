package de.uniol.inf.is.odysseus.mep.functions.encode;

import java.util.Base64;

import de.uniol.inf.is.odysseus.core.objecthandler.ObjectByteConverter;
import de.uniol.inf.is.odysseus.core.sdf.schema.SDFDatatype;
import de.uniol.inf.is.odysseus.mep.AbstractFunction;

public class Base64EncoderFunction extends AbstractFunction<String> {

	private static final long serialVersionUID = -5240837661341097085L;
	private static final SDFDatatype[][] ACC_TYPES = new SDFDatatype[][] { SDFDatatype.SIMPLE_TYPES };

	public Base64EncoderFunction() {
		super("base64Encode", 1, ACC_TYPES, SDFDatatype.STRING);
	}
	
	@Override
	public String getValue() {
		Object object = getInputValue(0);
		byte[] input = ObjectByteConverter.objectToBytes(object);
		return Base64.getEncoder().encodeToString(input);
	}

}
