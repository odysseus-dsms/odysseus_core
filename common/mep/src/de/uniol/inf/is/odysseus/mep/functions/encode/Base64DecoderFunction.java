package de.uniol.inf.is.odysseus.mep.functions.encode;

import java.util.Base64;

import de.uniol.inf.is.odysseus.core.objecthandler.ObjectByteConverter;
import de.uniol.inf.is.odysseus.core.sdf.schema.SDFDatatype;
import de.uniol.inf.is.odysseus.mep.AbstractFunction;

public class Base64DecoderFunction extends AbstractFunction<Object> {

	private static final long serialVersionUID = -5240837661341097085L;
	private static final SDFDatatype[][] ACC_TYPES = new SDFDatatype[][] { { SDFDatatype.STRING } };

	public Base64DecoderFunction() {
		super("base64Decode", 1, ACC_TYPES, SDFDatatype.OBJECT);
	}
	
	@Override
	public Object getValue() {
		String str = getInputValue(0);
		byte[] object = Base64.getDecoder().decode(str);
		return ObjectByteConverter.bytesToObject(object);
	}

}
