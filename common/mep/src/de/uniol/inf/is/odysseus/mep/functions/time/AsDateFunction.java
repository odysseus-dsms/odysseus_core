package de.uniol.inf.is.odysseus.mep.functions.time;

import java.util.Date;

import de.uniol.inf.is.odysseus.core.sdf.schema.SDFDatatype;
import de.uniol.inf.is.odysseus.mep.AbstractFunction;

/**
 * 
 * @author Marco Grawunder
 * 
 */
public class AsDateFunction extends AbstractFunction<Date> {

	private static final long serialVersionUID = 6255887477026357429L;
	private static final SDFDatatype[][] accTypes = new SDFDatatype[][] { { SDFDatatype.OBJECT} };

	public AsDateFunction() {
		super("asDate", 1, accTypes, SDFDatatype.DATE);
	}

	@Override
	public Date getValue() {
		Object date = getInputValue(0);
		return (Date) date;
	}

}
