package de.uniol.inf.is.odysseus.mep.functions.string;

import de.uniol.inf.is.odysseus.core.sdf.schema.SDFDatatype;
/**
 * 
 * @author Christian Kuka <christian.kuka@offis.de>
 * @author Marco Grawunder 
 */
public class StringPlusOperator2 extends AbstractStringPlusOperator{


	private static final long serialVersionUID = -6758609091849696249L;
	private static final SDFDatatype[][] accTypes = new SDFDatatype[][] {
		{ SDFDatatype.STRING }, SDFDatatype.getTypesAsArray() };

	public StringPlusOperator2() {
		super("+",accTypes, SDFDatatype.STRING);
	}
}
