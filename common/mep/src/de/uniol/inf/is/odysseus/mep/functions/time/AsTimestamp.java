package de.uniol.inf.is.odysseus.mep.functions.time;

import de.uniol.inf.is.odysseus.core.sdf.schema.SDFDatatype;
import de.uniol.inf.is.odysseus.mep.AbstractFunction;

/**
 * 
 * @author Marco Grawunder
 * 
 */
public class AsTimestamp extends AbstractFunction<Long> {

	private static final long serialVersionUID = 6255887477026357429L;
	private static final SDFDatatype[][] accTypes = new SDFDatatype[][] { { SDFDatatype.OBJECT} };

	public AsTimestamp() {
		super("asTimestamp", 1, accTypes, SDFDatatype.TIMESTAMP);
	}

	@Override
	public Long getValue() {
		Object input = getInputValue(0);
		return (Long) input;
	}

}
