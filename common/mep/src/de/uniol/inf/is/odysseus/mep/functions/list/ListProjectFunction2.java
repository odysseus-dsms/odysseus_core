package de.uniol.inf.is.odysseus.mep.functions.list;

import java.util.ArrayList;
import java.util.List;

import de.uniol.inf.is.odysseus.core.sdf.schema.SDFDatatype;
import de.uniol.inf.is.odysseus.mep.AbstractFunction;

public class ListProjectFunction2 extends AbstractFunction<List<Object>> {

	private static final long serialVersionUID = 482155249187485954L;
		
	private static final SDFDatatype[][] accTypes = new SDFDatatype[][] { SDFDatatype.getLists() , SDFDatatype.getLists()};
	
	public ListProjectFunction2() {
		super("ListProject", accTypes,SDFDatatype.LIST, false);
	}
	
	@Override
	public List<Object> getValue() {
		List<Object> out = new ArrayList<Object>();
		List<Object> inList = getInputValue(0);
		List<Integer> posList = getInputValue(1);
		
		posList.forEach(pos -> {
			out.add(inList.get(pos));
		});
		return out;
	}

}
