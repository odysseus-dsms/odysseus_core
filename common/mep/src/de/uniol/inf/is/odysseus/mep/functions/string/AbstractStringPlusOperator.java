package de.uniol.inf.is.odysseus.mep.functions.string;

import de.uniol.inf.is.odysseus.core.sdf.schema.SDFDatatype;
import de.uniol.inf.is.odysseus.mep.AbstractBinaryOperator;
import de.uniol.inf.is.odysseus.mep.IOperator;
/**
 * 
 * @author Christian Kuka <christian.kuka@offis.de>
 * @author Marco Grawunder
 *
 */
public abstract class AbstractStringPlusOperator extends AbstractBinaryOperator<String> {


	private static final long serialVersionUID = -6758609091849696249L;

	public AbstractStringPlusOperator(String name, SDFDatatype[][] acctypes2, SDFDatatype returnType) {
		super(name, acctypes2, returnType);
	}

	@Override
	public int getPrecedence() {
		return 6;
	}


	@Override
	public String getValue() {
		Object a = getInputValue(0);
		Object b = getInputValue(1);
		if ((a == null) || (b == null)) {
			return null;
		}
		StringBuilder sb = new StringBuilder();
		sb.append(a);
		sb.append(b);
		return sb.toString();
	}

	@Override
	public boolean isCommutative() {
		return false;
	}

	@Override
	public boolean isAssociative() {
		return false;
	}

	@Override
	public boolean isLeftDistributiveWith(IOperator<String> operator) {
		return false;
	}

	@Override
	public boolean isRightDistributiveWith(IOperator<String> operator) {
		return false;
	}

	@Override
	public de.uniol.inf.is.odysseus.mep.IOperator.ASSOCIATIVITY getAssociativity() {
		return ASSOCIATIVITY.LEFT_TO_RIGHT;
	}

}
