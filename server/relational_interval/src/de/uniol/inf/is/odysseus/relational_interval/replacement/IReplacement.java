package de.uniol.inf.is.odysseus.relational_interval.replacement;

import java.util.List;

import de.uniol.inf.is.odysseus.core.metadata.IMetaAttribute;
import de.uniol.inf.is.odysseus.core.metadata.IStreamObject;

public interface IReplacement<T extends IStreamObject<? extends IMetaAttribute>> {

	/**
	 * To allow replacements based on more that the last object, the
	 * replacement needs to be informed about each new object
	 * @param object
	 */
	void newObject(T object);

	
	/**
	 * This method tries to find a replacement for one attribute
	 * @param newObject the new object that is read, can be used e.g. when using interpolation
	 * @param missingValuesCount how many value are missing
	 * @param valueAttributePos where in the input tuple, can the missing value be found
	 * @return a list of object, where the values are replaced
	 */
	List<Object> determineReplacements(
			T newObject, int missingValuesCount,
			int valueAttributePos);
		
	String getName();

}
