package de.uniol.inf.is.odysseus.relational_interval.physicaloperator;

import de.uniol.inf.is.odysseus.core.collection.History;
import de.uniol.inf.is.odysseus.core.collection.Tuple;
import de.uniol.inf.is.odysseus.core.expression.RelationalExpression;
import de.uniol.inf.is.odysseus.core.metadata.ITimeInterval;
import de.uniol.inf.is.odysseus.core.metadata.PointInTime;
import de.uniol.inf.is.odysseus.core.physicaloperator.Heartbeat;
import de.uniol.inf.is.odysseus.core.physicaloperator.IPunctuation;
import de.uniol.inf.is.odysseus.core.physicaloperator.PunctuationFactory;
import de.uniol.inf.is.odysseus.core.server.physicaloperator.AbstractPipe;

public class RelationalPunctuationPO<T extends ITimeInterval> extends AbstractPipe<Tuple<T>, Tuple<T>> {

	private final RelationalExpression<T> timeExpression;
	private final boolean createOnHeartbeat;
	private final RelationalExpression<T> sendHeartbeatExpression;
	private final boolean suppressFireOnElement;
	private final byte punctuationType;

	private PointInTime lastTS = null;
	private boolean punctuationAfterElement;

	public RelationalPunctuationPO(RelationalExpression<T> timeExpression,
			RelationalExpression<T> sendHeartbeatExpression, boolean createOnHeartbeat, boolean suppressFireOnElement, byte punctuationType, boolean punctuationAfterElement) {
		this.timeExpression = timeExpression;
		this.createOnHeartbeat = createOnHeartbeat;
		this.sendHeartbeatExpression = sendHeartbeatExpression;
		this.suppressFireOnElement = suppressFireOnElement;
		this.punctuationType = punctuationType;
		this.punctuationAfterElement = punctuationAfterElement;
	}

	@Override
	protected void process_next(Tuple<T> object, int port) {
		evaluateTimeExpression(object);
		boolean punctuationExpression = evaluatePunctuationExpression(object);

		if (!punctuationExpression) {
			transfer(object);
		} else {
			if (!suppressFireOnElement) {
				if (!punctuationAfterElement) {
					sendPunctuation();
					transfer(object);
				}else{
					transfer(object);
					sendPunctuation();
				}
			}else {
				sendPunctuation();
			}
		}

	}

	@Override
	public void processPunctuation(IPunctuation punctuation, int port) {
		if (punctuation.isHeartbeat() && createOnHeartbeat && lastTS != null) {
			sendPunctuation(Heartbeat.createNewHeartbeat(lastTS));
		} else {
			sendPunctuation(punctuation);
		}
	}

	private boolean evaluatePunctuationExpression(Tuple<T> object) {
		if (sendHeartbeatExpression != null && sendHeartbeatExpression.evaluate(object)) {
			return true;
		}
		return false;
	}

	private void evaluateTimeExpression(Tuple<T> object) {
		Object value = timeExpression.evaluate(object, null, (History<Tuple<T>>)null);
		if (value instanceof PointInTime) {
			lastTS = (PointInTime) value;
		} else if (value != null) {
			lastTS = new PointInTime(((Number) value).longValue());
		}
	}
	
	private void sendPunctuation() {
		sendPunctuation(PunctuationFactory.createNewInstance(punctuationType, lastTS));
	}

	@Override
	public OutputMode getOutputMode() {
		return OutputMode.INPUT;
	}

}
