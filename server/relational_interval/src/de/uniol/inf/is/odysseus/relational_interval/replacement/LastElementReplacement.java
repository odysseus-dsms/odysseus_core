package de.uniol.inf.is.odysseus.relational_interval.replacement;

import java.util.ArrayList;
import java.util.List;

import de.uniol.inf.is.odysseus.core.collection.Tuple;
import de.uniol.inf.is.odysseus.core.metadata.IMetaAttribute;

public class LastElementReplacement<T extends Tuple<IMetaAttribute>> extends AbstractReplacement<T> {

	@Override
	public List<Object> determineReplacements(T newObject,
			int missingValuesCount, int valueAttributePos) {
		List<Object> vals = new ArrayList<>(missingValuesCount);
		for (int i=1;i<missingValuesCount;i++){
			vals.add(lastObject.getAttribute(valueAttributePos));
		}
		return vals;
	}

	@Override
	public String getName() {
		return "Last";
	}

}
