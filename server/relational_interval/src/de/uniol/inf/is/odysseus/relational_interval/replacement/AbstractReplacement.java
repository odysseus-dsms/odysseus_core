package de.uniol.inf.is.odysseus.relational_interval.replacement;

import de.uniol.inf.is.odysseus.core.metadata.IMetaAttribute;
import de.uniol.inf.is.odysseus.core.metadata.IStreamObject;

abstract public class AbstractReplacement<T extends IStreamObject<IMetaAttribute>> implements IReplacement<T>{
	
	protected T lastObject;
	
	@SuppressWarnings("unchecked")
	@Override
	public void newObject(T object) {
		this.lastObject = (T) object.clone();
	}
	
}
