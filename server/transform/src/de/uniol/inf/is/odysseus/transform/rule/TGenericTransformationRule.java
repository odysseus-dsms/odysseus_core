package de.uniol.inf.is.odysseus.transform.rule;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import de.uniol.inf.is.odysseus.core.logicaloperator.ILogicalOperator;
import de.uniol.inf.is.odysseus.core.physicaloperator.IPhysicalOperator;
import de.uniol.inf.is.odysseus.core.server.logicaloperator.annotations.LogicalOperator;
import de.uniol.inf.is.odysseus.core.server.planmanagement.TransformationConfiguration;
import de.uniol.inf.is.odysseus.core.server.planmanagement.TransformationException;
import de.uniol.inf.is.odysseus.core.util.ClassHelper;
import de.uniol.inf.is.odysseus.ruleengine.rule.RuleException;
import de.uniol.inf.is.odysseus.ruleengine.ruleflow.IRuleFlowGroup;
import de.uniol.inf.is.odysseus.transform.flow.TransformRuleFlowGroup;

public class TGenericTransformationRule extends AbstractTransformationRule<ILogicalOperator> {

	
	@Override
	public int getPriority() {
		return 1000;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void execute(ILogicalOperator operator, TransformationConfiguration config) throws RuleException {
		LogicalOperator ann = operator.getClass().getAnnotation(LogicalOperator.class);
		IPhysicalOperator po = null;
		try {
			Class<? extends IPhysicalOperator> opClass = (Class<? extends IPhysicalOperator>) ClassHelper.resolveClass(ann.physicalOperatorClass());
			po = (IPhysicalOperator) opClass.getDeclaredConstructor(operator.getClass()).newInstance(operator);
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException | SecurityException | ClassNotFoundException | IOException e) {
			throw new TransformationException("Cannot translate "+operator, e);	
		}
		if (po == null) {
			throw new TransformationException("Cannot translate "+operator);
		}		
		defaultExecute(operator, po, config, true, true);
	}

	@Override
	public boolean isExecutable(ILogicalOperator operator, TransformationConfiguration config) {
		if (!operator.isAllPhysicalInputSet()) {
			return false;
		}
		LogicalOperator ann = operator.getClass().getAnnotation(LogicalOperator.class);
		if (ann == null) {
			return false;
		}
		return !(ann.physicalOperatorClass().isBlank());
	}

	@Override
	public IRuleFlowGroup getRuleFlowGroup() {
		return TransformRuleFlowGroup.TRANSFORMATION;
	}

}
