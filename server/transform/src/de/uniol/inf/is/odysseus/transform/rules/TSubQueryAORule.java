package de.uniol.inf.is.odysseus.transform.rules;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import de.uniol.inf.is.odysseus.core.collection.Context;
import de.uniol.inf.is.odysseus.core.collection.OptionMap;
import de.uniol.inf.is.odysseus.core.collection.Resource;
import de.uniol.inf.is.odysseus.core.metadata.IMetaAttribute;
import de.uniol.inf.is.odysseus.core.metadata.IStreamObject;
import de.uniol.inf.is.odysseus.core.physicaloperator.IPhysicalOperator;
import de.uniol.inf.is.odysseus.core.planmanagement.query.LogicalPlan;
import de.uniol.inf.is.odysseus.core.sdf.schema.SDFSchema;
import de.uniol.inf.is.odysseus.core.sdf.schema.SDFSchemaFactory;
import de.uniol.inf.is.odysseus.core.server.logicaloperator.MetadataAO;
import de.uniol.inf.is.odysseus.core.server.logicaloperator.SubQueryAO;
import de.uniol.inf.is.odysseus.core.server.metadata.MetadataRegistry;
import de.uniol.inf.is.odysseus.core.server.physicaloperator.OutputConnectorPO;
import de.uniol.inf.is.odysseus.core.server.physicaloperator.SubQueryPO;
import de.uniol.inf.is.odysseus.core.server.planmanagement.TransformationConfiguration;
import de.uniol.inf.is.odysseus.core.server.planmanagement.TransformationException;
import de.uniol.inf.is.odysseus.core.server.planmanagement.executor.IServerExecutor;
import de.uniol.inf.is.odysseus.core.server.planmanagement.optimization.configuration.ParameterAddMetadata;
import de.uniol.inf.is.odysseus.core.server.planmanagement.query.IPhysicalQuery;
import de.uniol.inf.is.odysseus.core.server.planmanagement.query.querybuiltparameter.IQueryBuildSetting;
import de.uniol.inf.is.odysseus.ruleengine.rule.RuleException;
import de.uniol.inf.is.odysseus.ruleengine.ruleflow.IRuleFlowGroup;
import de.uniol.inf.is.odysseus.transform.flow.TransformRuleFlowGroup;
import de.uniol.inf.is.odysseus.transform.rule.AbstractTransformationRule;

public class TSubQueryAORule extends AbstractTransformationRule<SubQueryAO> {

	@Override
	public int getPriority() {
		// Must be more than access ao rule
		return super.getPriority() + 10;
	}

	@Override
	public void execute(SubQueryAO operator, TransformationConfiguration config) throws RuleException {
		IServerExecutor executor = config.getOption(IServerExecutor.class.getName());
		if (executor == null) {
			throw new TransformationException(
					"Cannot create SubQueryPO. Executor not set in Transformation Configuration!");
		}

		IPhysicalQuery pquery;

		if (operator.getQueryID() != null) {
			pquery = executor.getPhysicalQueryByString(operator.getQueryID() + "", getCaller());
		} else if (operator.getQueryName() != null) {
			pquery = executor.getPhysicalQueryByString(operator.getQueryName(), getCaller());
		} else {
			pquery = compileQuery(operator, config, executor);
		}

		if (pquery == null) {
			throw new TransformationException("Could not create query for SubqueryAO!");
		}

		SubQueryPO<IStreamObject<?>> po;
		try {
			po = new SubQueryPO<>(pquery, executor, getCaller());
		} catch (Exception e) {
			executor.removeQuery(pquery.getID(), getCaller());
			throw new TransformationException(e);
		}

		List<IPhysicalOperator> roots = pquery.getRoots();
		if (operator.getAttributes() == null) {

			// Output schema of subquery can be calculated from participating roots
			for (IPhysicalOperator root : roots) {
				if (root instanceof OutputConnectorPO<?>) {
					OutputConnectorPO<?> outConn = (OutputConnectorPO<?>) root;
					SDFSchema outschema = outConn.getOutputSchema();
					operator.setOutputSchema(outConn.getPort(), outschema);
				}
			}
			LogicalPlan.recalcOutputSchemas(operator, false);

		}
				

		processMetaData(operator, pquery, config);

		
		defaultExecute(operator, po, config, true, true);

	}

	private IPhysicalQuery compileQuery(SubQueryAO operator, TransformationConfiguration config,
			IServerExecutor executor) {
		IPhysicalQuery pquery;
		final String queryText = getQueryText(operator);
		Context context = config.getContext().copy();
		OptionMap options = operator.getOptionsMap();
		for (String key : options.getOptions().keySet()) {
			context.putOrReplace(key, options.getValue(key));
		}

		List<IQueryBuildSetting<?>> addQueryBuildSettings = new ArrayList<>();
		// SubQueryAO could be a source
		if (operator.getInputSchema() != null) {
			ParameterAddMetadata setting = new ParameterAddMetadata(operator.getInputSchema(0).getMetaAttributeNames());
			addQueryBuildSettings.add(setting);
		}else if (operator.getPhysSubscriptionsTo() != null && !operator.getPhysSubscriptionsTo().isEmpty()) {
			ParameterAddMetadata setting = new ParameterAddMetadata(operator.getPhysSubscriptionTo(0).getSchema().getMetaAttributeNames());
			addQueryBuildSettings.add(setting);
		}

		Collection<Integer> q = executor.addQuery(queryText, operator.getQueryParser(), getCaller(),
				IServerExecutor.STANDARD_BUILD_CONFIG_NAME, context, addQueryBuildSettings);
		if (q.size() != 1) {
			for (Integer queryId : q) {
				executor.removeQuery(queryId, getCaller());
			}
			throw new TransformationException("SubQueryPO can only handle a single query!");
		}

		pquery = executor.getPhysicalQueryByString(q.iterator().next() + "", getCaller());
		pquery.getLogicalQuery().setName(new Resource(getCaller().getUser().getName(), operator.getName()));
		pquery.setSubQuery(true);
		return pquery;
	}

	private String getQueryText(SubQueryAO operator) {
		final String queryText;
		if (operator.getQueryFileName() != null) {
			StringBuilder querTextBuilder = new StringBuilder();
			if (operator.getQueryFileName().contains("://")) {
				URL url;
				try {
					url = new URL(operator.getQueryFileName());
				} catch (MalformedURLException e1) {
					throw new TransformationException("Error reading from " + operator.getQueryFileName(), e1);
				}
				try (BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));) {
					String inputLine;
					while ((inputLine = in.readLine()) != null) {
						querTextBuilder.append(inputLine).append(System.lineSeparator());
					}
				} catch (IOException e) {
					throw new TransformationException("Error reading from " + operator.getQueryFileName(), e);
				}
				queryText = querTextBuilder.toString();
			} else {
				File file = new File(operator.getQueryFileName());
				try (BufferedReader in = new BufferedReader(new FileReader(file));) {
					String inputLine;
					while ((inputLine = in.readLine()) != null) {
						querTextBuilder.append(inputLine).append(System.lineSeparator());
					}
				} catch (IOException e) {
					throw new TransformationException("Error reading from " + operator.getQueryFileName(), e);
				}
				queryText = querTextBuilder.toString();
			}
		} else {
			if (operator.getQueryText() != null) {
				queryText = operator.getQueryText();	
			}else {
				queryText = null;
			}
		}
		if (queryText == null) {
			throw new TransformationException("Subquery operator has no query!");
		}
		return queryText;
	}

	private void processMetaData(SubQueryAO operator, IPhysicalQuery pquery, TransformationConfiguration config) {	
		// Handle only cases where subquery is source operator
		if (operator.getSubscribedToSource().size() == 0) {
			
			// Special case: subquery is source and source does not define same metadata as global
			// --> output schema is calculated wrong
			List<IPhysicalOperator> roots = pquery.getRoots();
			
			// Replace meta data in this case, with meta data from subquery (i.e. outConn.root Metadata)
			// Can be done in any case, no need to check
			for (IPhysicalOperator root : roots) {
				if (root instanceof OutputConnectorPO<?>) {
					OutputConnectorPO<?> outConn = (OutputConnectorPO<?>) root;
					SDFSchema outschema = SDFSchemaFactory.createNewWithMetaSchema(operator.getOutputSchema(), outConn.getOutputSchema().getMetaschema());
					operator.setOutputSchema(outConn.getPort(), outschema);
				}
			}
			
			IMetaAttribute subQueryType = MetadataRegistry.getMetadataType(operator.getOutputSchema().getMetaAttributeNames());
			IMetaAttribute scriptType = MetadataRegistry.getMetadataType(config.getDefaultMetaTypeSet());
			if (scriptType != null) {
				Set<Class<?>> classes1 = new HashSet<>(Arrays.asList(scriptType.getClasses()));
				Set<Class<?>> classes2 = new HashSet<>(Arrays.asList(subQueryType.getClasses()));
				if (classes1.size() == classes2.size() && classes1.containsAll(classes2) && classes2.containsAll(classes1)) {
					// metadata in both cases are identical
				} else if (classes1.size() > classes2.size() && classes1.containsAll(classes2)) {
					// check, if the metadata with #METADATA is an upper set of the type given in
					// the query
					MetadataAO metadataAO = new MetadataAO();
					metadataAO.setLocalMetaAttribute(scriptType);
					LogicalPlan.insertOperatorBefore(metadataAO, operator);
					insert(metadataAO);
				} else {
					throw new TransformationException("Metadata in the query file "
							+ config.getDefaultMetaTypeSet() + " is not compatible with the subquery source operator "
							+ operator.getLocalMetaAttribute());
				}

			}
		}
	}
	
	
	@Override
	public boolean isExecutable(SubQueryAO operator, TransformationConfiguration config) {
		return operator.isAllPhysicalInputSet();
	}

	@Override
	public IRuleFlowGroup getRuleFlowGroup() {
		return TransformRuleFlowGroup.TRANSFORMATION;
	}
	

}
