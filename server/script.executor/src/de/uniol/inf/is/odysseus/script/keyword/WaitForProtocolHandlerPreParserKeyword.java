package de.uniol.inf.is.odysseus.script.keyword;

import de.uniol.inf.is.odysseus.script.executor.KeywordProvider;

public class WaitForProtocolHandlerPreParserKeyword extends AbstractWaitForHandlerPreParserKeyword {

	public static final String NAME = "WAITFORPROTOCOLHANDLER";

	@Override
	protected boolean isAvailable(String name) {
		return KeywordProvider.protocolHandlerRegistry.getIProtocolHandlerClass(name) != null;
	}

}
