package de.uniol.inf.is.odysseus.script.keyword;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import de.uniol.inf.is.odysseus.core.collection.Context;
import de.uniol.inf.is.odysseus.core.server.planmanagement.executor.IServerExecutor;
import de.uniol.inf.is.odysseus.core.server.planmanagement.executor.command.IExecutorCommand;
import de.uniol.inf.is.odysseus.core.usermanagement.ISession;
import de.uniol.inf.is.odysseus.script.parser.AbstractPreParserKeyword;
import de.uniol.inf.is.odysseus.script.parser.OdysseusScriptException;

public abstract class AbstractWaitForHandlerPreParserKeyword extends AbstractPreParserKeyword {

	@Override
	public void validate(Map<String, Object> variables, String parameter,
			ISession caller, Context context, IServerExecutor executor) throws OdysseusScriptException {
	}

	@Override
	public List<IExecutorCommand> execute(Map<String, Object> variables,
			String parameter, ISession caller, Context context, IServerExecutor executor)
			throws OdysseusScriptException {
		String[] para = getSimpleParameters(parameter);
		String name = para[0];
		long testPeriod = 1000;
		if (para.length > 1&&  para[1].length() > 0){
			testPeriod = Long.parseLong(para[1]);
		}
		long maxWaitingTime = 120000;
		if (para.length > 2){
			maxWaitingTime = Long.parseLong(para[2]);
		}
		List<IExecutorCommand> ret = new LinkedList<>();
	
		// Directly execute this, and delay parsing of query
		try {
			long start = System.currentTimeMillis();
			while (!isAvailable(name)
					&& !(maxWaitingTime > 0 && System.currentTimeMillis() > start + maxWaitingTime)) {
				Thread.sleep(testPeriod);
			}
		} catch (Exception e) {

		}
		
		
		return ret;
	}

	protected abstract boolean isAvailable(String name); 
	
}
