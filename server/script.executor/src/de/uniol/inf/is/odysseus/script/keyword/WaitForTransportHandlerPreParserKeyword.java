package de.uniol.inf.is.odysseus.script.keyword;

import de.uniol.inf.is.odysseus.script.executor.KeywordProvider;

public class WaitForTransportHandlerPreParserKeyword extends AbstractWaitForHandlerPreParserKeyword {

	public static final String NAME = "WAITFORTRANSPORTHANDLER";

	@Override
	protected boolean isAvailable(String name) {
		return KeywordProvider.transportHandlerRegistry.getITransportHandlerClass(name) != null;
	}

}
