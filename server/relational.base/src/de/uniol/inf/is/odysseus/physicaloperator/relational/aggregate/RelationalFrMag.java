package de.uniol.inf.is.odysseus.physicaloperator.relational.aggregate;

import de.uniol.inf.is.odysseus.core.collection.Tuple;
import de.uniol.inf.is.odysseus.core.sdf.schema.SDFDatatype;
import de.uniol.inf.is.odysseus.core.server.physicaloperator.aggregate.basefunctions.IPartialAggregate;
import de.uniol.inf.is.odysseus.core.server.physicaloperator.aggregate.functions.FrMag;
import de.uniol.inf.is.odysseus.core.server.physicaloperator.aggregate.functions.FrMagPartialAggregate;

/**
 * @author Oormila Ramanandan Kottayi Pilapprathodi
 * @author Michael Suenkel
 *
 */

@SuppressWarnings({ "rawtypes" })
public class RelationalFrMag extends FrMag<Tuple<?>, Tuple<?>> {

    /**
     * 
     */
    private static final long serialVersionUID = -7768784425424062403L;
    private int pos;
    private final int size;

    static public RelationalFrMag getInstance(final int pos, int size, final boolean partialAggregateInput) {
        return new RelationalFrMag(pos, size, partialAggregateInput);
    }

    private RelationalFrMag(final int pos, int size, final boolean partialAggregateInput) {
        super(size, partialAggregateInput);
        this.pos = pos;
        this.size = size;
    }

    /**
     * 
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
	@Override
    public IPartialAggregate<Tuple<?>> init(Tuple in) {
        if (isPartialAggregateInput()) {
            return init((FrMagPartialAggregate<Tuple<?>>) in.getAttribute(pos));
        }
        FrMagPartialAggregate<Tuple<?>> pa = new FrMagPartialAggregate<>(size);
        pa.add(((Number) in.getAttribute(this.pos)));
        return pa;
    }

    /**
     * 
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
	@Override
    protected IPartialAggregate<Tuple<?>> process_merge(IPartialAggregate p, Tuple toMerge) {
    	FrMagPartialAggregate pa = (FrMagPartialAggregate) p;
        if (isPartialAggregateInput()) {
            return merge(p, (IPartialAggregate) toMerge.getAttribute(pos), false);
        }
        return pa.add(((Number) toMerge.getAttribute(pos)));
    }

    /**
     * 
     * {@inheritDoc}
     */
    @Override
    public IPartialAggregate<Tuple<?>> merge(IPartialAggregate<Tuple<?>> p, IPartialAggregate<Tuple<?>> toMerge, boolean createNew) {
        final FrMagPartialAggregate<Tuple<?>> pa;
        if (createNew) {
        	FrMagPartialAggregate<Tuple<?>> h = (FrMagPartialAggregate<Tuple<?>>) p;
            pa = new FrMagPartialAggregate<>(h);
        }
        else {
            pa = (FrMagPartialAggregate<Tuple<?>>) p;
        }
        return pa.merge((FrMagPartialAggregate) toMerge);
    }

    /**
     * 
     * {@inheritDoc}
     */
    @Override
    public Tuple evaluate(IPartialAggregate p) {
    	FrMagPartialAggregate pa = (FrMagPartialAggregate) p;
        Tuple r = new Tuple(1, false);
        r.setAttribute(0, pa.getFrMag());
        return r;
    }

    /**
     * 
     * {@inheritDoc}
     */
    @Override
    public SDFDatatype getPartialAggregateType() {
        return SDFDatatype.MEDIAN_PARTIAL_AGGREGATE;
    }
}
