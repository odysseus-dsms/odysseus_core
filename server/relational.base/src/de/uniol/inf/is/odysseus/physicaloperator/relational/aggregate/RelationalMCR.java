package de.uniol.inf.is.odysseus.physicaloperator.relational.aggregate;

import de.uniol.inf.is.odysseus.core.collection.Tuple;
import de.uniol.inf.is.odysseus.core.sdf.schema.SDFDatatype;
import de.uniol.inf.is.odysseus.core.server.physicaloperator.aggregate.basefunctions.IPartialAggregate;
import de.uniol.inf.is.odysseus.core.server.physicaloperator.aggregate.functions.MCR;
import de.uniol.inf.is.odysseus.core.server.physicaloperator.aggregate.functions.MCRPartialAggregate;

/**
 * @author Oormila Ramanandan Kottayi Pilapprathodi
 * @author Michael Suenkel
 *
 */

@SuppressWarnings({ "rawtypes" })
public class RelationalMCR extends MCR<Tuple<?>, Tuple<?>> {

    /**
     * 
     */
    private static final long serialVersionUID = -7768784425424062403L;
    private int pos;

    static public RelationalMCR getInstance(final int pos, final boolean partialAggregateInput) {
        return new RelationalMCR(pos, partialAggregateInput);
    }

    private RelationalMCR(final int pos, final boolean partialAggregateInput) {
        super(partialAggregateInput);
        this.pos = pos;
    }

    /**
     * 
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
	@Override
    public IPartialAggregate<Tuple<?>> init(Tuple in) {
        if (isPartialAggregateInput()) {
            return init((MCRPartialAggregate<Tuple<?>>) in.getAttribute(pos));
        }
        MCRPartialAggregate<Tuple<?>> pa = new MCRPartialAggregate<>();
        pa.add(((Number) in.getAttribute(this.pos)));
        return pa;
    }

    /**
     * 
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
	@Override
    protected IPartialAggregate<Tuple<?>> process_merge(IPartialAggregate p, Tuple toMerge) {
    	MCRPartialAggregate pa = (MCRPartialAggregate) p;
        if (isPartialAggregateInput()) {
            return merge(p, (IPartialAggregate) toMerge.getAttribute(pos), false);
        }
        return pa.add(((Number) toMerge.getAttribute(pos)));
    }

    /**
     * 
     * {@inheritDoc}
     */
    @Override
    public IPartialAggregate<Tuple<?>> merge(IPartialAggregate<Tuple<?>> p, IPartialAggregate<Tuple<?>> toMerge, boolean createNew) {
        final MCRPartialAggregate<Tuple<?>> pa;
        if (createNew) {
        	MCRPartialAggregate<Tuple<?>> h = (MCRPartialAggregate<Tuple<?>>) p;
            pa = new MCRPartialAggregate<>(h);
        }
        else {
            pa = (MCRPartialAggregate<Tuple<?>>) p;
        }
        return pa.merge((MCRPartialAggregate) toMerge);
    }

    /**
     * 
     * {@inheritDoc}
     */
    @Override
    public Tuple evaluate(IPartialAggregate p) {
    	MCRPartialAggregate pa = (MCRPartialAggregate) p;
        Tuple r = new Tuple(1, false);
        r.setAttribute(0, Double.valueOf(pa.getMCRValue().doubleValue()));
        return r;
    }

    /**
     * 
     * {@inheritDoc}
     */
    @Override
    public SDFDatatype getPartialAggregateType() {
        return SDFDatatype.MEDIAN_PARTIAL_AGGREGATE;
    }
}
