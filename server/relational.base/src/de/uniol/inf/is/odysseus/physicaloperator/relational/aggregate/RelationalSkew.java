package de.uniol.inf.is.odysseus.physicaloperator.relational.aggregate;

/**
 * @author Oormila Ramanandan Kottayi Pilapprathodi
 * @author Michael Suenkel
 *
 */

import de.uniol.inf.is.odysseus.core.collection.Tuple;
import de.uniol.inf.is.odysseus.core.sdf.schema.SDFDatatype;
import de.uniol.inf.is.odysseus.core.server.physicaloperator.aggregate.basefunctions.IPartialAggregate;
import de.uniol.inf.is.odysseus.core.server.physicaloperator.aggregate.functions.Skew;
import de.uniol.inf.is.odysseus.core.server.physicaloperator.aggregate.functions.SkewPartialAggregate;

@SuppressWarnings({ "rawtypes" })
public class RelationalSkew extends Skew<Tuple<?>, Tuple<?>> {

    /**
     * 
     */
    private static final long serialVersionUID = -7768784425424062403L;
    private int pos;

    static public RelationalSkew getInstance(final int pos, final boolean partialAggregateInput) {
        return new RelationalSkew(pos, partialAggregateInput);
    }

    private RelationalSkew(final int pos, final boolean partialAggregateInput) {
        super(partialAggregateInput);
        this.pos = pos;
    }

    /**
     * 
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
	@Override
    public IPartialAggregate<Tuple<?>> init(Tuple in) {
        if (isPartialAggregateInput()) {
            return init((SkewPartialAggregate<Tuple<?>>) in.getAttribute(pos));
        }
        SkewPartialAggregate<Tuple<?>> pa = new SkewPartialAggregate<>();
        pa.add(((Number) in.getAttribute(this.pos)));
        return pa;
    }

    /**
     * 
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
	@Override
    protected IPartialAggregate<Tuple<?>> process_merge(IPartialAggregate p, Tuple toMerge) {
    	SkewPartialAggregate pa = (SkewPartialAggregate) p;
        if (isPartialAggregateInput()) {
            return merge(p, (IPartialAggregate) toMerge.getAttribute(pos), false);
        }
        return pa.add(((Number) toMerge.getAttribute(pos)));
    }

    /**
     * 
     * {@inheritDoc}
     */
    @Override
    public IPartialAggregate<Tuple<?>> merge(IPartialAggregate<Tuple<?>> p, IPartialAggregate<Tuple<?>> toMerge, boolean createNew) {
        final SkewPartialAggregate<Tuple<?>> pa;
        if (createNew) {
        	SkewPartialAggregate<Tuple<?>> h = (SkewPartialAggregate<Tuple<?>>) p;
            pa = new SkewPartialAggregate<>(h);
        }
        else {
            pa = (SkewPartialAggregate<Tuple<?>>) p;
        }
        return pa.merge((SkewPartialAggregate) toMerge);
    }

    /**
     * 
     * {@inheritDoc}
     */
    @Override
    public Tuple evaluate(IPartialAggregate p) {
    	SkewPartialAggregate pa = (SkewPartialAggregate) p;
        Tuple r = new Tuple(1, false);
        r.setAttribute(0, Double.valueOf(pa.getSkewnessValue().doubleValue()));
        return r;
    }

    /**
     * 
     * {@inheritDoc}
     */
    @Override
    public SDFDatatype getPartialAggregateType() {
        return SDFDatatype.MEDIAN_PARTIAL_AGGREGATE;
    }
}
