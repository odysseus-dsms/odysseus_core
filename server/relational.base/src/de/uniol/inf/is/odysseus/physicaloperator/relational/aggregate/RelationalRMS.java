package de.uniol.inf.is.odysseus.physicaloperator.relational.aggregate;

import de.uniol.inf.is.odysseus.core.collection.Tuple;
import de.uniol.inf.is.odysseus.core.sdf.schema.SDFDatatype;
import de.uniol.inf.is.odysseus.core.server.physicaloperator.aggregate.basefunctions.IPartialAggregate;
import de.uniol.inf.is.odysseus.core.server.physicaloperator.aggregate.functions.RMS;
import de.uniol.inf.is.odysseus.core.server.physicaloperator.aggregate.functions.RMSPartialAggregate;

/**
 * @author Oormila Ramanandan Kottayi Pilapprathodi
 * @author Michael Suenkel
 *
 */

@SuppressWarnings({ "rawtypes" })
public class RelationalRMS extends RMS<Tuple<?>, Tuple<?>> {

    /**
     * 
     */
    private static final long serialVersionUID = -7768784425424062403L;
    private int pos;

    static public RelationalRMS getInstance(final int pos, final boolean partialAggregateInput) {
        return new RelationalRMS(pos, partialAggregateInput);
    }

    private RelationalRMS(final int pos, final boolean partialAggregateInput) {
        super(partialAggregateInput);
        this.pos = pos;
    }

    /**
     * 
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
	@Override
    public IPartialAggregate<Tuple<?>> init(Tuple in) {
        if (isPartialAggregateInput()) {
            return init((RMSPartialAggregate<Tuple<?>>) in.getAttribute(pos));
        }
        RMSPartialAggregate<Tuple<?>> pa = new RMSPartialAggregate<>();
        pa.add(((Number) in.getAttribute(this.pos)));
        return pa;
    }

    /**
     * 
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
	@Override
    protected IPartialAggregate<Tuple<?>> process_merge(IPartialAggregate p, Tuple toMerge) {
    	RMSPartialAggregate pa = (RMSPartialAggregate) p;
        if (isPartialAggregateInput()) {
            return merge(p, (IPartialAggregate) toMerge.getAttribute(pos), false);
        }
        return pa.add(((Number) toMerge.getAttribute(pos)));
    }

    /**
     * 
     * {@inheritDoc}
     */
    @Override
    public IPartialAggregate<Tuple<?>> merge(IPartialAggregate<Tuple<?>> p, IPartialAggregate<Tuple<?>> toMerge, boolean createNew) {
        final RMSPartialAggregate<Tuple<?>> pa;
        if (createNew) {
        	RMSPartialAggregate<Tuple<?>> h = (RMSPartialAggregate<Tuple<?>>) p;
            pa = new RMSPartialAggregate<>(h);
        }
        else {
            pa = (RMSPartialAggregate<Tuple<?>>) p;
        }
        return pa.merge((RMSPartialAggregate) toMerge);
    }

    /**
     * 
     * {@inheritDoc}
     */
    @Override
    public Tuple evaluate(IPartialAggregate p) {
    	RMSPartialAggregate pa = (RMSPartialAggregate) p;
        Tuple r = new Tuple(1, false);
        r.setAttribute(0, Double.valueOf(pa.getRMSValue().doubleValue()));
        return r;
    }

    /**
     * 
     * {@inheritDoc}
     */
    @Override
    public SDFDatatype getPartialAggregateType() {
        return SDFDatatype.MEDIAN_PARTIAL_AGGREGATE;
    }
}
