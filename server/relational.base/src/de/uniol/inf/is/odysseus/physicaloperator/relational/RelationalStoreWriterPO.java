package de.uniol.inf.is.odysseus.physicaloperator.relational;

import de.uniol.inf.is.odysseus.core.collection.Tuple;
import de.uniol.inf.is.odysseus.core.metadata.IMetaAttribute;
import de.uniol.inf.is.odysseus.core.physicaloperator.IPunctuation;
import de.uniol.inf.is.odysseus.core.server.physicaloperator.AbstractSink;
import de.uniol.inf.is.odysseus.core.server.store.IStore;

public class RelationalStoreWriterPO<T extends IMetaAttribute> extends AbstractSink<Tuple<T>> {

	final IStore<Comparable<?>, Object> store;
	final int keyPos;
	
	public RelationalStoreWriterPO(IStore<Comparable<?>, Object> store, int keyPos) {
		this.store = store;
		this.keyPos = keyPos;
	}
	
	@Override
	public void processPunctuation(IPunctuation punctuation, int port) {
		// ignore 
	}

	@Override
	protected void process_next(Tuple<T> object, int port) {
		store.put(object.getAttribute(keyPos), object);
	}

}
