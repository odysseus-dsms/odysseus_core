package de.uniol.inf.is.odysseus.relational.transform;

import de.uniol.inf.is.odysseus.core.metadata.IMetaAttribute;
import de.uniol.inf.is.odysseus.core.server.physicaloperator.StoreWriterAO;
import de.uniol.inf.is.odysseus.core.server.planmanagement.TransformationConfiguration;
import de.uniol.inf.is.odysseus.core.server.store.IStore;
import de.uniol.inf.is.odysseus.physicaloperator.relational.RelationalStoreWriterPO;
import de.uniol.inf.is.odysseus.ruleengine.rule.RuleException;

public class TRelationalStoreWriterAORule extends AbstractRelationalTransformationRule<StoreWriterAO> {

	@Override
	public void execute(StoreWriterAO operator, TransformationConfiguration config) throws RuleException {
		IStore<Comparable<?>, Object> store = getDataDictionary().getStore(operator.getStoreName(), getCaller());		
		RelationalStoreWriterPO<IMetaAttribute> po = new RelationalStoreWriterPO<>(store, operator.getOutputSchema().indexOf(operator.getIdAttribute()));
		defaultExecute(operator, po, config, true, true);
	}

}
