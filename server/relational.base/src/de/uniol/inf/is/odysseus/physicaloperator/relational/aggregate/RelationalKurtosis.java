package de.uniol.inf.is.odysseus.physicaloperator.relational.aggregate;

import de.uniol.inf.is.odysseus.core.collection.Tuple;
import de.uniol.inf.is.odysseus.core.sdf.schema.SDFDatatype;
import de.uniol.inf.is.odysseus.core.server.physicaloperator.aggregate.basefunctions.IPartialAggregate;
import de.uniol.inf.is.odysseus.core.server.physicaloperator.aggregate.functions.Kurtosis;
import de.uniol.inf.is.odysseus.core.server.physicaloperator.aggregate.functions.KurtosisPartialAggregate;

/**
 * @author Oormila Ramanandan Kottayi Pilapprathodi
 * @author Michael Suenkel
 *
 */

@SuppressWarnings({ "rawtypes" })
public class RelationalKurtosis extends Kurtosis<Tuple<?>, Tuple<?>> {

    /**
     * 
     */
    private static final long serialVersionUID = -7768784425424062403L;
    private int pos;

    static public RelationalKurtosis getInstance(final int pos, final boolean partialAggregateInput) {
        return new RelationalKurtosis(pos, partialAggregateInput);
    }

    private RelationalKurtosis(final int pos, final boolean partialAggregateInput) {
        super(partialAggregateInput);
        this.pos = pos;
    }

    /**
     * 
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
	@Override
    public IPartialAggregate<Tuple<?>> init(Tuple in) {
        if (isPartialAggregateInput()) {
            return init((KurtosisPartialAggregate<Tuple<?>>) in.getAttribute(pos));
        }
        KurtosisPartialAggregate<Tuple<?>> pa = new KurtosisPartialAggregate<>();
        pa.add(((Number) in.getAttribute(this.pos)));
        return pa;
    }

    /**
     * 
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
	@Override
    protected IPartialAggregate<Tuple<?>> process_merge(IPartialAggregate p, Tuple toMerge) {
    	KurtosisPartialAggregate pa = (KurtosisPartialAggregate) p;
        if (isPartialAggregateInput()) {
            return merge(p, (IPartialAggregate) toMerge.getAttribute(pos), false);
        }
        return pa.add(((Number) toMerge.getAttribute(pos)));
    }

    /**
     * 
     * {@inheritDoc}
     */
    @Override
    public IPartialAggregate<Tuple<?>> merge(IPartialAggregate<Tuple<?>> p, IPartialAggregate<Tuple<?>> toMerge, boolean createNew) {
        final KurtosisPartialAggregate<Tuple<?>> pa;
        if (createNew) {
        	KurtosisPartialAggregate<Tuple<?>> h = (KurtosisPartialAggregate<Tuple<?>>) p;
            pa = new KurtosisPartialAggregate<>(h);
        }
        else {
            pa = (KurtosisPartialAggregate<Tuple<?>>) p;
        }
        return pa.merge((KurtosisPartialAggregate) toMerge);
    }

    /**
     * 
     * {@inheritDoc}
     */
    @Override
    public Tuple evaluate(IPartialAggregate p) {
    	KurtosisPartialAggregate pa = (KurtosisPartialAggregate) p;
        Tuple r = new Tuple(1, false);
        r.setAttribute(0, Double.valueOf(pa.getKurtosisValue().doubleValue()));
        return r;
    }

    /**
     * 
     * {@inheritDoc}
     */
    @Override
    public SDFDatatype getPartialAggregateType() {
        return SDFDatatype.MEDIAN_PARTIAL_AGGREGATE;
    }
}
