package de.uniol.inf.is.odysseus.physicaloperator.relational.aggregate;

import de.uniol.inf.is.odysseus.core.collection.Tuple;
import de.uniol.inf.is.odysseus.core.sdf.schema.SDFDatatype;
import de.uniol.inf.is.odysseus.core.server.physicaloperator.aggregate.basefunctions.IPartialAggregate;
import de.uniol.inf.is.odysseus.core.server.physicaloperator.aggregate.functions.IQR;
import de.uniol.inf.is.odysseus.core.server.physicaloperator.aggregate.functions.IQRPartialAggregate;

/**
 * @author Oormila Ramanandan Kottayi Pilapprathodi
 * @author Michael Suenkel
 *
 */

@SuppressWarnings({ "rawtypes" })
public class RelationalIQR extends IQR<Tuple<?>, Tuple<?>> {

    /**
     * 
     */
    private static final long serialVersionUID = -7768784425424062403L;
    private int pos;

    static public RelationalIQR getInstance(final int pos, final boolean partialAggregateInput) {
        return new RelationalIQR(pos, partialAggregateInput);
    }

    private RelationalIQR(final int pos, final boolean partialAggregateInput) {
        super(partialAggregateInput);
        this.pos = pos;
    }

    /**
     * 
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
	@Override
    public IPartialAggregate<Tuple<?>> init(Tuple in) {
        if (isPartialAggregateInput()) {
            return init((IQRPartialAggregate<Tuple<?>>) in.getAttribute(pos));
        }
        IQRPartialAggregate<Tuple<?>> pa = new IQRPartialAggregate<>();
        pa.add(((Number) in.getAttribute(this.pos)));
        return pa;
    }

    /**
     * 
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
	@Override
    protected IPartialAggregate<Tuple<?>> process_merge(IPartialAggregate p, Tuple toMerge) {
    	IQRPartialAggregate pa = (IQRPartialAggregate) p;
        if (isPartialAggregateInput()) {
            return merge(p, (IPartialAggregate) toMerge.getAttribute(pos), false);
        }
        return pa.add(((Number) toMerge.getAttribute(pos)));
    }

    /**
     * 
     * {@inheritDoc}
     */
    @Override
    public IPartialAggregate<Tuple<?>> merge(IPartialAggregate<Tuple<?>> p, IPartialAggregate<Tuple<?>> toMerge, boolean createNew) {
        final IQRPartialAggregate<Tuple<?>> pa;
        if (createNew) {
        	IQRPartialAggregate<Tuple<?>> h = (IQRPartialAggregate<Tuple<?>>) p;
            pa = new IQRPartialAggregate<>(h);
        }
        else {
            pa = (IQRPartialAggregate<Tuple<?>>) p;
        }
        return pa.merge((IQRPartialAggregate) toMerge);
    }

    /**
     * 
     * {@inheritDoc}
     */
    @Override
    public Tuple evaluate(IPartialAggregate p) {
    	IQRPartialAggregate pa = (IQRPartialAggregate) p;
        Tuple r = new Tuple(1, false);
        r.setAttribute(0, Double.valueOf(pa.getIQRValue().doubleValue()));
        return r;
    }

    /**
     * 
     * {@inheritDoc}
     */
    @Override
    public SDFDatatype getPartialAggregateType() {
        return SDFDatatype.MEDIAN_PARTIAL_AGGREGATE;
    }
}
