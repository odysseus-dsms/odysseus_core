package de.uniol.inf.is.odysseus.physicaloperator.relational.aggregate;

import de.uniol.inf.is.odysseus.core.collection.Tuple;
import de.uniol.inf.is.odysseus.core.sdf.schema.SDFDatatype;
import de.uniol.inf.is.odysseus.core.server.physicaloperator.aggregate.basefunctions.IPartialAggregate;
import de.uniol.inf.is.odysseus.core.server.physicaloperator.aggregate.functions.FrPeakFreq;
import de.uniol.inf.is.odysseus.core.server.physicaloperator.aggregate.functions.FrPeakFreqPartialAggregate;
@SuppressWarnings({ "rawtypes" })
public class RelationalFrPeakFreq extends FrPeakFreq<Tuple<?>, Tuple<?>> {

    /**
     * 
     */
    private static final long serialVersionUID = -7768784425424062403L;
    private int pos;

    static public RelationalFrPeakFreq getInstance(final int pos, final boolean partialAggregateInput) {
        return new RelationalFrPeakFreq(pos, partialAggregateInput);
    }

    private RelationalFrPeakFreq(final int pos, final boolean partialAggregateInput) {
        super(partialAggregateInput);
        this.pos = pos;
    }

    /**
     * 
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
	@Override
    public IPartialAggregate<Tuple<?>> init(Tuple in) {
        if (isPartialAggregateInput()) {
            return init((FrPeakFreqPartialAggregate<Tuple<?>>) in.getAttribute(pos));
        }
        FrPeakFreqPartialAggregate<Tuple<?>> pa = new FrPeakFreqPartialAggregate<>();
        pa.add(((Number) in.getAttribute(this.pos)));
        return pa;
    }

    /**
     * 
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
	@Override
    protected IPartialAggregate<Tuple<?>> process_merge(IPartialAggregate p, Tuple toMerge) {
    	FrPeakFreqPartialAggregate pa = (FrPeakFreqPartialAggregate) p;
        if (isPartialAggregateInput()) {
            return merge(p, (IPartialAggregate) toMerge.getAttribute(pos), false);
        }
        return pa.add(((Number) toMerge.getAttribute(pos)));
    }

    /**
     * 
     * {@inheritDoc}
     */
    @Override
    public IPartialAggregate<Tuple<?>> merge(IPartialAggregate<Tuple<?>> p, IPartialAggregate<Tuple<?>> toMerge, boolean createNew) {
        final FrPeakFreqPartialAggregate<Tuple<?>> pa;
        if (createNew) {
        	FrPeakFreqPartialAggregate<Tuple<?>> h = (FrPeakFreqPartialAggregate<Tuple<?>>) p;
            pa = new FrPeakFreqPartialAggregate<>(h);
        }
        else {
            pa = (FrPeakFreqPartialAggregate<Tuple<?>>) p;
        }
        return pa.merge((FrPeakFreqPartialAggregate) toMerge);
    }

    /**
     * 
     * {@inheritDoc}
     */
    @Override
    public Tuple evaluate(IPartialAggregate p) {
    	FrPeakFreqPartialAggregate pa = (FrPeakFreqPartialAggregate) p;
        Tuple r = new Tuple(1, false);
        r.setAttribute(0, Double.valueOf(pa.getFrPeakFreq().doubleValue()));
        return r;
    }

    /**
     * 
     * {@inheritDoc}
     */
    @Override
    public SDFDatatype getPartialAggregateType() {
        return SDFDatatype.MEDIAN_PARTIAL_AGGREGATE;
    }
}
