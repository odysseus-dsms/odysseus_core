package de.uniol.inf.is.odysseus.physicaloperator.relational.aggregate;

import de.uniol.inf.is.odysseus.core.collection.Tuple;
import de.uniol.inf.is.odysseus.core.sdf.schema.SDFDatatype;
import de.uniol.inf.is.odysseus.core.server.physicaloperator.aggregate.basefunctions.IPartialAggregate;
import de.uniol.inf.is.odysseus.core.server.physicaloperator.aggregate.functions.FrEnergy;
import de.uniol.inf.is.odysseus.core.server.physicaloperator.aggregate.functions.FrEnergyPartialAggregate;

/**
 * @author Oormila Ramanandan Kottayi Pilapprathodi
 * @author Michael Suenkel
 *
 */
@SuppressWarnings({ "rawtypes" })
public class RelationalFrEnergy extends FrEnergy<Tuple<?>, Tuple<?>> {

    /**
     * 
     */
    private static final long serialVersionUID = -7768784425424062403L;
    private int pos;

    static public RelationalFrEnergy getInstance(final int pos, final boolean partialAggregateInput) {
        return new RelationalFrEnergy(pos, partialAggregateInput);
    }

    private RelationalFrEnergy(final int pos, final boolean partialAggregateInput) {
        super(partialAggregateInput);
        this.pos = pos;
    }

    /**
     * 
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
	@Override
    public IPartialAggregate<Tuple<?>> init(Tuple in) {
        if (isPartialAggregateInput()) {
            return init((FrEnergyPartialAggregate<Tuple<?>>) in.getAttribute(pos));
        }
        FrEnergyPartialAggregate<Tuple<?>> pa = new FrEnergyPartialAggregate<>();
        pa.add(((Number) in.getAttribute(this.pos)));
        return pa;
    }

    /**
     * 
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
	@Override
    protected IPartialAggregate<Tuple<?>> process_merge(IPartialAggregate p, Tuple toMerge) {
    	FrEnergyPartialAggregate pa = (FrEnergyPartialAggregate) p;
        if (isPartialAggregateInput()) {
            return merge(p, (IPartialAggregate) toMerge.getAttribute(pos), false);
        }
        return pa.add(((Number) toMerge.getAttribute(pos)));
    }

    /**
     * 
     * {@inheritDoc}
     */
    @Override
    public IPartialAggregate<Tuple<?>> merge(IPartialAggregate<Tuple<?>> p, IPartialAggregate<Tuple<?>> toMerge, boolean createNew) {
        final FrEnergyPartialAggregate<Tuple<?>> pa;
        if (createNew) {
        	FrEnergyPartialAggregate<Tuple<?>> h = (FrEnergyPartialAggregate<Tuple<?>>) p;
            pa = new FrEnergyPartialAggregate<>(h);
        }
        else {
            pa = (FrEnergyPartialAggregate<Tuple<?>>) p;
        }
        return pa.merge((FrEnergyPartialAggregate) toMerge);
    }

    /**
     * 
     * {@inheritDoc}
     */
    @Override
    public Tuple evaluate(IPartialAggregate p) {
    	FrEnergyPartialAggregate pa = (FrEnergyPartialAggregate) p;
        Tuple r = new Tuple(1, false);
        r.setAttribute(0, Double.valueOf(pa.getFrEnergy().doubleValue()));
        return r;
    }

    /**
     * 
     * {@inheritDoc}
     */
    @Override
    public SDFDatatype getPartialAggregateType() {
        return SDFDatatype.MEDIAN_PARTIAL_AGGREGATE;
    }
}
