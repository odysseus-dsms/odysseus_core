package de.uniol.inf.is.odysseus.physicaloperator.relational.aggregate;

import de.uniol.inf.is.odysseus.core.collection.Tuple;
import de.uniol.inf.is.odysseus.core.sdf.schema.SDFDatatype;
import de.uniol.inf.is.odysseus.core.server.physicaloperator.aggregate.basefunctions.IPartialAggregate;
import de.uniol.inf.is.odysseus.core.server.physicaloperator.aggregate.functions.FrDmEntroPy;
import de.uniol.inf.is.odysseus.core.server.physicaloperator.aggregate.functions.FrDmEntroPyPartialAggregate;

/**
 * @author Oormila Ramanandan Kottayi Pilapprathodi
 * @author Michael Suenkel
 *
 */
@SuppressWarnings({ "rawtypes" })
public class RelationalFrDmEntroPy extends FrDmEntroPy<Tuple<?>, Tuple<?>> {

    /**
     * 
     */
    private static final long serialVersionUID = -7768784425424062403L;
    private int pos;

    static public RelationalFrDmEntroPy getInstance(final int pos, final boolean partialAggregateInput) {
        return new RelationalFrDmEntroPy(pos, partialAggregateInput);
    }

    private RelationalFrDmEntroPy(final int pos, final boolean partialAggregateInput) {
        super(partialAggregateInput);
        this.pos = pos;
    }

    /**
     * 
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
	@Override
    public IPartialAggregate<Tuple<?>> init(Tuple in) {
        if (isPartialAggregateInput()) {
            return init((FrDmEntroPyPartialAggregate<Tuple<?>>) in.getAttribute(pos));
        }
        FrDmEntroPyPartialAggregate<Tuple<?>> pa = new FrDmEntroPyPartialAggregate<>();
        pa.add(((Number) in.getAttribute(this.pos)));
        return pa;
    }

    /**
     * 
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
	@Override
    protected IPartialAggregate<Tuple<?>> process_merge(IPartialAggregate p, Tuple toMerge) {
    	FrDmEntroPyPartialAggregate pa = (FrDmEntroPyPartialAggregate) p;
        if (isPartialAggregateInput()) {
            return merge(p, (IPartialAggregate) toMerge.getAttribute(pos), false);
        }
        return pa.add(((Number) toMerge.getAttribute(pos)));
    }

    /**
     * 
     * {@inheritDoc}
     */
    @Override
    public IPartialAggregate<Tuple<?>> merge(IPartialAggregate<Tuple<?>> p, IPartialAggregate<Tuple<?>> toMerge, boolean createNew) {
        final FrDmEntroPyPartialAggregate<Tuple<?>> pa;
        if (createNew) {
        	FrDmEntroPyPartialAggregate<Tuple<?>> h = (FrDmEntroPyPartialAggregate<Tuple<?>>) p;
            pa = new FrDmEntroPyPartialAggregate<>(h);
        }
        else {
            pa = (FrDmEntroPyPartialAggregate<Tuple<?>>) p;
        }
        return pa.merge((FrDmEntroPyPartialAggregate) toMerge);
    }

    /**
     * 
     * {@inheritDoc}
     */
    @Override
    public Tuple evaluate(IPartialAggregate p) {
    	FrDmEntroPyPartialAggregate pa = (FrDmEntroPyPartialAggregate) p;
        Tuple r = new Tuple(1, false);
        r.setAttribute(0, Double.valueOf(pa.getFrDmEntroPy().doubleValue()));
        return r;
    }

    /**
     * 
     * {@inheritDoc}
     */
    @Override
    public SDFDatatype getPartialAggregateType() {
        return SDFDatatype.MEDIAN_PARTIAL_AGGREGATE;
    }
}
