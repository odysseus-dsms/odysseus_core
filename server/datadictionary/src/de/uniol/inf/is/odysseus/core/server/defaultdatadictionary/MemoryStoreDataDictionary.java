/*******************************************************************************
 * Copyright 2012 The Odysseus Team
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.uniol.inf.is.odysseus.core.server.defaultdatadictionary;

import java.util.ArrayList;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.uniol.inf.is.odysseus.core.collection.Resource;
import de.uniol.inf.is.odysseus.core.planmanagement.query.ILogicalPlan;
import de.uniol.inf.is.odysseus.core.procedure.StoredProcedure;
import de.uniol.inf.is.odysseus.core.sdf.schema.SDFDatatype;
import de.uniol.inf.is.odysseus.core.server.datadictionary.AbstractDataDictionary;
import de.uniol.inf.is.odysseus.core.server.datadictionary.IDataDictionary;
import de.uniol.inf.is.odysseus.core.server.store.IStore;
import de.uniol.inf.is.odysseus.core.server.store.MemoryStore;
import de.uniol.inf.is.odysseus.core.server.usermanagement.UserManagementProvider;
import de.uniol.inf.is.odysseus.core.usermanagement.ITenant;
import de.uniol.inf.is.odysseus.core.usermanagement.IUser;
import de.uniol.inf.is.odysseus.core.util.OSGI;

public class MemoryStoreDataDictionary extends AbstractDataDictionary {

	@SuppressWarnings("unused")
	private static final Logger LOG = LoggerFactory.getLogger(MemoryStoreDataDictionary.class);

	public static final String NAME = "Memorystore";

	
	@Override
	public String getStoreTypeName() {
		return NAME;
	}
	

	public MemoryStoreDataDictionary(){
		super(null, null);
	}

	public MemoryStoreDataDictionary(ITenant t, UserManagementProvider ump){
		super(t, ump);
	}

	@Override
	public IDataDictionary createInstance(ITenant t) {
		IDataDictionary dd = new MemoryStoreDataDictionary(t, OSGI.get(UserManagementProvider.class));
		return dd;
	}

	@Override
	protected IStore<Resource, ILogicalPlan> createStreamDefinitionsStore() {
		return new MemoryStore<>();
	}

	@Override
	protected IStore<Resource, IUser> createViewOrStreamFromUserStore() {
		return new MemoryStore<>();
	}

	@Override
	protected IStore<Resource, ILogicalPlan> createViewDefinitionsStore() {
		return new MemoryStore<>();
	}

	@Override
	protected IStore<Resource, HashMap<String, ArrayList<Resource>>> createEntityUsedByStore() {
		return new MemoryStore<>();
	}

	@Override
	protected IStore<Resource, IUser> createEntityFromUserStore() {
		return new MemoryStore<>();
	}

	@Override
	protected IStore<String, SDFDatatype> createDatatypesStore() {
		return new MemoryStore<>();
	}

	@Override
	protected IStore<Integer, String> createSavedQueriesStore() {
		return new MemoryStore<>();
	}

	@Override
	protected IStore<Integer, IUser> createSavedQueriesForUserStore() {
		return new MemoryStore<>();
	}

	@Override
	protected IStore<Integer, String> createSavedQueriesBuildParameterNameStore() {
		return new MemoryStore<>();
	}

	@Override
	protected IStore<Resource, ILogicalPlan> createSinkDefinitionsStore() {
		return new MemoryStore<>();
	}

	@Override
	protected IStore<Resource, IUser> createSinkFromUserStore() {
		return new MemoryStore<>();
	}


	@Override
	protected IStore<Resource, StoredProcedure> createStoredProceduresStore() {
		return new MemoryStore<>();
	}

	@Override
	protected IStore<Resource, IUser> createStoredProceduresFromUserStore() {
		return new MemoryStore<>();
	}

	@Override
	protected IStore<Resource, IStore<Comparable<?>, Object>> createStoresStore() {
		return new MemoryStore<>();
	}

	@Override
	protected IStore<Resource, IUser> createStoresFromUserStore() {
		return new MemoryStore<>();
	}


}
