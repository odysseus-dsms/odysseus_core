package de.uniol.inf.is.odysseus.aggregation.functions.factory;

import java.util.List;

/**
 * Some aggregation function factories can create more than one aggregation function
 * (created an extra function to be downward compatible)
 * 
 * @author Marco Grawunder
 *
 */
public interface IMultiAggregationFunctionFactory extends IAggregationFunctionFactory {

	List<String> getFunctionNames();
	
}
