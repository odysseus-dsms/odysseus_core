package de.uniol.inf.is.odysseus.aggregation.functions.impl;

import java.util.Map;

import de.uniol.inf.is.odysseus.aggregation.functions.AbstractIncrementalAggregationFunction;
import de.uniol.inf.is.odysseus.aggregation.functions.IAggregationFunction;
import de.uniol.inf.is.odysseus.aggregation.functions.factory.AggregationFunctionParseOptionsHelper;
import de.uniol.inf.is.odysseus.core.collection.Tuple;
import de.uniol.inf.is.odysseus.core.metadata.ITimeInterval;
import de.uniol.inf.is.odysseus.core.metadata.PointInTime;
import de.uniol.inf.is.odysseus.core.sdf.schema.IAttributeResolver;

public class StdDev<M extends ITimeInterval, T extends Tuple<M>> extends Variance<M,T> {

	private static final long serialVersionUID = -8249236814763767841L;

	public StdDev(final StdDev<M, T> other) {
		super(other);
	}

	public StdDev() {
		super();
	}

	public StdDev(final int[] attributes, final String[] outputNames) {
		super(attributes, outputNames);
	}

	public StdDev(final int inputAttributesLength, final String[] outputNames) {
		super(null, outputNames);
	}
	
	
	@Override
	public IAggregationFunction createInstance(Map<String, Object> parameters, IAttributeResolver attributeResolver) {
		final int[] attributes = AggregationFunctionParseOptionsHelper.getInputAttributeIndices(parameters,
				attributeResolver);
		final String[] outputNames = AggregationFunctionParseOptionsHelper.getOutputAttributeNames(parameters,
				attributeResolver);

		if (attributes == null) {
			return new Variance<>(attributeResolver.getSchema().get(0).size(), outputNames);
		}
		return new StdDev<>(attributes, outputNames);
	}
	
	@Override
	public Object[] evaluate(T trigger, PointInTime pointInTime) {
		Object[] variance = super.evaluate(trigger, pointInTime);
		Object[] result = new Double[variance.length];
		for (int i=0;i<variance.length; i++) {
			result[i] = Math.sqrt(((Double)variance[i]));
		}
		return result;
	}

	@Override
	public AbstractIncrementalAggregationFunction<M, T> clone() {
		return new StdDev<>(this);
	}
	
}
