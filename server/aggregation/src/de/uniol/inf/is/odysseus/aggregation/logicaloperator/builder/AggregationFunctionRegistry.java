/**
 * Copyright 2015 The Odysseus Team
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.uniol.inf.is.odysseus.aggregation.logicaloperator.builder;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

import de.uniol.inf.is.odysseus.aggregation.functions.IAggregationFunction;
import de.uniol.inf.is.odysseus.aggregation.functions.factory.AggregationFunctionParseOptionsHelper;
import de.uniol.inf.is.odysseus.aggregation.functions.factory.IAggregationFunctionFactory;
import de.uniol.inf.is.odysseus.aggregation.functions.factory.IMultiAggregationFunctionFactory;
import de.uniol.inf.is.odysseus.core.sdf.schema.IAttributeResolver;
import de.uniol.inf.is.odysseus.core.server.planmanagement.QueryParseException;

/**
 * @author Cornelius Ludmann
 * @author Marco Grawunder
 *
 */
public class AggregationFunctionRegistry {

	static private final Multimap<String, IAggregationFunctionFactory> functionFactories = ArrayListMultimap.create();

	// Must be public for service
	public AggregationFunctionRegistry() {
		;
	}

	public void addFunction(final IAggregationFunctionFactory function) {
		if (function instanceof IMultiAggregationFunctionFactory) {
			((IMultiAggregationFunctionFactory)function).getFunctionNames().forEach(f->functionFactories.put(f.toUpperCase(), function));
		}else{
			functionFactories.put(function.getFunctionName().toUpperCase(), function);
		}
	}
	
	public void removeFunction(final IAggregationFunctionFactory function){
		if (function instanceof IMultiAggregationFunctionFactory) {
			((IMultiAggregationFunctionFactory)function).getFunctionNames().forEach(f->functionFactories.remove(f.toUpperCase(), function));
		}else{
			functionFactories.remove(function.getFunctionName().toUpperCase(), function);
		}
	}

	/**
	 * @param value
	 * @param iAttributeResolver
	 * @return
	 */
	static public IAggregationFunction createFunction(final Map<String, Object> inputValues,
			final IAttributeResolver attributeResolver) {

		Map<String, Object> valueMap = new HashMap<>();
		// Ignore case for keys!
		inputValues.entrySet().forEach(e -> valueMap.put(e.getKey().toUpperCase(), e.getValue()));
		
		final String functionName = AggregationFunctionParseOptionsHelper.getFunctionParameterAsString(valueMap,
				AggregationFunctionParseOptionsHelper.FUNCTION_NAME);

		if (functionName == null) {
			throw new QueryParseException(
					"Could not find parameter '" + AggregationFunctionParseOptionsHelper.FUNCTION_NAME + "'.");
		}

		final Collection<IAggregationFunctionFactory> matchingFunctions = functionFactories.get(functionName.toUpperCase());

		for (final IAggregationFunctionFactory candidate : matchingFunctions) {
			if (candidate.checkParameters(valueMap, attributeResolver)) {
				return candidate.createInstance(valueMap, attributeResolver);
			}
		}

		throw new QueryParseException("Could not find function " + functionName+ " with paramterset "+valueMap);
	}

}
