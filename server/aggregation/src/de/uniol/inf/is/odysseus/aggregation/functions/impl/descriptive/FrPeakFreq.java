package de.uniol.inf.is.odysseus.aggregation.functions.impl.descriptive;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.math3.complex.Complex;
import org.apache.commons.math3.exception.MathIllegalArgumentException;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import org.apache.commons.math3.transform.DftNormalization;
import org.apache.commons.math3.transform.FastFourierTransformer;
import org.apache.commons.math3.transform.TransformType;

import com.google.common.math.IntMath;

/**
 * @author Oormila Ramanandan Kottayi Pilapprathodi
 * @author Michael Suenkel
 *
 */

public class FrPeakFreq extends AbstractAggregationDescriptiveStatisticsFunction {

	@Override
	public double evaluate(double[] values) throws MathIllegalArgumentException {
		
		double[] meanArr = new double[(IntMath.isPowerOfTwo(values.length))?
				values.length:IntMath.ceilingPowerOfTwo(values.length)];
		DescriptiveStatistics da = new DescriptiveStatistics();
		for(double d:values) {
		da.addValue(d);
		}
		double mean=da.getMean();
		for(double d:values) {
    		da.addValue(d);
    		}
		for(int i=0;i<values.length;i++) {
			meanArr[i]=values[i]-mean;
		}
		if(values.length!=meanArr.length) {
			for(int i=values.length;i<meanArr.length;i++)
			meanArr[i]=0.0;
		}
		// TODO: Check if UNITARY and FORWARD are correct
		FastFourierTransformer fft=new FastFourierTransformer(DftNormalization.UNITARY);
		Complex[] result = fft.transform(meanArr, TransformType.FORWARD);
		double[] magArr = new double[result.length];
		for(int i=0;i<result.length;i++) {
			magArr[i]=result[i].abs()/values.length;
			
		}
		double[] lnSpace = this.linspace(0, 5, magArr.length);
		List<Double> list = new ArrayList<>(magArr.length);
		for (double v: magArr) {
			list.add(v);
		}
		Arrays.sort(magArr);
		int index=list.indexOf(Double.valueOf(magArr[magArr.length-1]));
		return lnSpace[index];  
	}
	
    private  double[] linspace(double min, double max, int points) {  
        double[] d = new double[points];  
        for (int i = 0; i < points; i++){  
            d[i] = min + i * (max - min) / (points - 1);  
        }  
        return d;  
    }  

}
