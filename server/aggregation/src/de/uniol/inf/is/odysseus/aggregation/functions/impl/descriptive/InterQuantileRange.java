package de.uniol.inf.is.odysseus.aggregation.functions.impl.descriptive;

import org.apache.commons.math3.exception.MathIllegalArgumentException;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;

public class InterQuantileRange extends AbstractAggregationDescriptiveStatisticsFunction{

	@Override
	public double evaluate(double[] values) throws MathIllegalArgumentException {
		DescriptiveStatistics da = new DescriptiveStatistics(values);
		return da.getPercentile(75) - da.getPercentile(25);
	}
	
}
