package de.uniol.inf.is.odysseus.aggregation.functions.impl.descriptive;

import org.apache.commons.math3.exception.MathIllegalArgumentException;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;

/**
 * @author Oormila Ramanandan Kottayi Pilapprathodi
 * @author Michael Suenkel
 *
 */

public class MeanCrossingRate extends AbstractAggregationDescriptiveStatisticsFunction{

	@Override
	public double evaluate(double[] values) throws MathIllegalArgumentException {

		DescriptiveStatistics da = new DescriptiveStatistics(values);
		double mean = da.getMean();
		double[] meanArr = new double[values.length];
		for (int i=0;i<values.length;i++) {
			meanArr[i] = values[i] - mean;
		}
		int sum = 0;
		for (int i = 0; i < meanArr.length - 1; i++) {
			if (meanArr[i] * meanArr[i + 1] < 0) {
				sum = sum + 1;
			}
		}
		Double mcr = (double)sum / values.length;
		if(mcr.isNaN())
			mcr=0.0;
		
		return mcr;
	}
 
	
}
