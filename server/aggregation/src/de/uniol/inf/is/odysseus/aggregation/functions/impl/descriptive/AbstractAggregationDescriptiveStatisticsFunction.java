package de.uniol.inf.is.odysseus.aggregation.functions.impl.descriptive;

import org.apache.commons.math3.exception.MathIllegalArgumentException;
import org.apache.commons.math3.stat.descriptive.AbstractUnivariateStatistic;
import org.apache.commons.math3.stat.descriptive.UnivariateStatistic;

public abstract class AbstractAggregationDescriptiveStatisticsFunction extends AbstractUnivariateStatistic {

	@Override
	public UnivariateStatistic copy() {
		// no state, no need to copy
		return this;
	}

	@Override
	public double evaluate(double[] arg0, int arg1, int arg2) throws MathIllegalArgumentException {
		throw new RuntimeException("This method is not implemented!");
	}

	@Override
	public abstract double evaluate(double[] values) throws MathIllegalArgumentException;
	
}
