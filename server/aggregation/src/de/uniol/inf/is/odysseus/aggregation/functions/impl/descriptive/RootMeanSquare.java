package de.uniol.inf.is.odysseus.aggregation.functions.impl.descriptive;

import org.apache.commons.math3.exception.MathIllegalArgumentException;

public class RootMeanSquare extends AbstractAggregationDescriptiveStatisticsFunction {

	@Override
	public double evaluate(double[] values) throws MathIllegalArgumentException {
        double sum = 0.0;
        for (double num : values) {
            sum += num * num;
        }
        return Math.sqrt(sum / values.length);
	}

}
