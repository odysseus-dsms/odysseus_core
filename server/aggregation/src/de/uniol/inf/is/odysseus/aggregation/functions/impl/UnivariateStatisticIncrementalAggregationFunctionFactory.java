package de.uniol.inf.is.odysseus.aggregation.functions.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.math3.stat.descriptive.UnivariateStatistic;
import org.apache.commons.math3.stat.descriptive.moment.GeometricMean;
import org.apache.commons.math3.stat.descriptive.moment.Kurtosis;
import org.apache.commons.math3.stat.descriptive.moment.Skewness;

import de.uniol.inf.is.odysseus.aggregation.functions.impl.descriptive.FrDmEntropy;
import de.uniol.inf.is.odysseus.aggregation.functions.impl.descriptive.FrMag;
import de.uniol.inf.is.odysseus.aggregation.functions.impl.descriptive.FrPeakFreq;
import de.uniol.inf.is.odysseus.aggregation.functions.impl.descriptive.InterQuantileRange;
import de.uniol.inf.is.odysseus.aggregation.functions.impl.descriptive.MeanCrossingRate;
import de.uniol.inf.is.odysseus.aggregation.functions.impl.descriptive.RootMeanSquare;
import de.uniol.inf.is.odysseus.aggregation.functions.impl.descriptive.SpectralEnergy;

public class UnivariateStatisticIncrementalAggregationFunctionFactory {

	static final Map<String, UnivariateStatistic> functions = new HashMap<>();
	
	static {
		addFunction(new GeometricMean());
		addFunction(new Kurtosis());
		addFunction(new Skewness());
		addFunction("IQR", new InterQuantileRange());
		addFunction("MCQ", new MeanCrossingRate());
		addFunction("RMS", new RootMeanSquare());
		addFunction("FrEnergy", new SpectralEnergy());
		addFunction("FrDmEntropy", new FrDmEntropy());
		addFunction("FrPeakFreq", new FrPeakFreq());
		addFunction("FrMag5", new FrMag(5));
	}
	
	static Optional<UnivariateStatistic> getFunction(String name) {
		UnivariateStatistic func = functions.get(name.toUpperCase());
		if (func != null) {
			return Optional.of(func.copy());
		}
		return Optional.empty();
	}
	
	static void addFunction(UnivariateStatistic function) {
		functions.put(function.getClass().getName().toUpperCase(), function);
	}

	static void addFunction(String name, UnivariateStatistic function) {
		functions.put(name.toUpperCase(), function);
	}
	
	public static List<String> getNames() {
		return new ArrayList<String>(functions.keySet());
	}
	

	
	

}
