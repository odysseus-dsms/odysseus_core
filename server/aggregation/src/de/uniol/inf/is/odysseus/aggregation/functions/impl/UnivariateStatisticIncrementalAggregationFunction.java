package de.uniol.inf.is.odysseus.aggregation.functions.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.math3.stat.descriptive.UnivariateStatistic;
import org.apache.commons.math3.util.ResizableDoubleArray;

import de.uniol.inf.is.odysseus.aggregation.functions.AbstractIncrementalAggregationFunction;
import de.uniol.inf.is.odysseus.aggregation.functions.IAggregationFunction;
import de.uniol.inf.is.odysseus.aggregation.functions.factory.AggregationFunctionParseOptionsHelper;
import de.uniol.inf.is.odysseus.aggregation.functions.factory.IMultiAggregationFunctionFactory;
import de.uniol.inf.is.odysseus.core.collection.Tuple;
import de.uniol.inf.is.odysseus.core.metadata.ITimeInterval;
import de.uniol.inf.is.odysseus.core.metadata.PointInTime;
import de.uniol.inf.is.odysseus.core.sdf.schema.IAttributeResolver;
import de.uniol.inf.is.odysseus.core.sdf.schema.SDFAttribute;

public class UnivariateStatisticIncrementalAggregationFunction<M extends ITimeInterval, T extends Tuple<M>>
		extends AbstractIncrementalAggregationFunction<M, T> implements IMultiAggregationFunctionFactory {

	private static final long serialVersionUID = 4083295265819573407L;
	private final ResizableDoubleArray values = new ResizableDoubleArray();
	private final int valuePos;
	private final UnivariateStatistic statistics;
	private final Collection<SDFAttribute> outputAttributes;

	protected UnivariateStatisticIncrementalAggregationFunction(UnivariateStatistic statistics, int valuePos,
			final Collection<SDFAttribute> outputAttributes, final int[] attributes, final String[] outputAttrNames) {
		super(attributes, outputAttrNames);
		this.statistics = statistics;
		this.valuePos = valuePos;
		this.outputAttributes = outputAttributes;
	}

	public UnivariateStatisticIncrementalAggregationFunction() {
		// just for service
		valuePos = -1;
		statistics = null;
		outputAttributes = new ArrayList<>();
	}

	public UnivariateStatisticIncrementalAggregationFunction(
			UnivariateStatisticIncrementalAggregationFunction<M, T> other) {
		super(other);
		this.valuePos = other.valuePos;
		this.statistics = other.statistics;
		this.outputAttributes = other.outputAttributes;
	}

	@Override
	public boolean checkParameters(Map<String, Object> parameters, IAttributeResolver attributeResolver) {
		// TODO Auto-generated method stub
		// TODO: Check if data type of attribute is numeric
		// TODO: Check if only a single attribute is choosen
		// TODO: Check if the function is available
		return true;
	}

	@Override
	public IAggregationFunction createInstance(Map<String, Object> parameters, IAttributeResolver attributeResolver) {
		final int[] attributes = AggregationFunctionParseOptionsHelper.getInputAttributeIndices(parameters,
				attributeResolver);
		final String[] outputNames = AggregationFunctionParseOptionsHelper.getOutputAttributeNames(parameters,
				attributeResolver);
		final String functionName = AggregationFunctionParseOptionsHelper.getFunctionName(parameters);

		final Collection<SDFAttribute> outputAttr = AggregationFunctionParseOptionsHelper
				.determineOutputAttributes(attributeResolver, attributes, outputNames);

		Optional<UnivariateStatistic> func = UnivariateStatisticIncrementalAggregationFunctionFactory
				.getFunction(functionName);

		if (func.isPresent()) {
			return new UnivariateStatisticIncrementalAggregationFunction<>(func.get(), attributes[0], outputAttr,
					attributes, outputNames);
		}
		throw new IllegalArgumentException("Cannot find aggregation function " + functionName);
	}

	@Override
	public void addNew(T newElement) {
		values.addElement(newElement.getAttribute(valuePos));
	}

	@Override
	public void removeOutdated(Collection<T> outdatedElements, T trigger, PointInTime pointInTime) {
		// Remark: This aggregation works only for in order cases, so the elements that
		// should be
		// removed are the first n elements of the currently stored content
		values.discardFrontElements(outdatedElements.size());
	}

	@Override
	public Object[] evaluate(T trigger, PointInTime pointInTime) {
		final Object[] result = new Object[1];
		result[0] = statistics.evaluate(values.getElements());
		return result;
	}

	@Override
	public Collection<SDFAttribute> getOutputAttributes() {
		return outputAttributes;
	}

	@Override
	public AbstractIncrementalAggregationFunction<M, T> clone() {
		return new UnivariateStatisticIncrementalAggregationFunction<>(this);
	}

	@Override
	public String getFunctionName() {
		return null;
	}

	@Override
	public List<String> getFunctionNames() {
		return UnivariateStatisticIncrementalAggregationFunctionFactory.getNames();
	}

}
