package de.uniol.inf.is.odysseus.aggregation.functions.impl.descriptive;

import org.apache.commons.math3.complex.Complex;
import org.apache.commons.math3.exception.MathIllegalArgumentException;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import org.apache.commons.math3.transform.DftNormalization;
import org.apache.commons.math3.transform.FastFourierTransformer;
import org.apache.commons.math3.transform.TransformType;

import com.google.common.math.IntMath;

/**
 * @author Oormila Ramanandan Kottayi Pilapprathodi
 * @author Michael Suenkel
 *
 */

public class FrMag extends AbstractAggregationDescriptiveStatisticsFunction {
	
	private final int number;

	public FrMag(int number) {
		this.number = number;
	}
	

	@Override
	public double evaluate(double[] values) throws MathIllegalArgumentException {
		double[] meanArr = new double[(IntMath.isPowerOfTwo(values.length)) ? values.length
				: IntMath.ceilingPowerOfTwo(values.length)];
		DescriptiveStatistics da = new DescriptiveStatistics();
		for (double d : values) {
			da.addValue(d);
		}
		double mean = da.getMean();
		for (double d : values) {
			da.addValue(d);
		}
		for (int i = 0; i < values.length; i++) {
			meanArr[i] = values[i] - mean;
		}
		if (values.length != meanArr.length) {
			for (int i = values.length; i < meanArr.length; i++)
				meanArr[i] = 0.0;
		}
		// TODO: Check if UNITARY and FORWARD are correct
		FastFourierTransformer fft = new FastFourierTransformer(DftNormalization.UNITARY);
		Complex[] result = fft.transform(meanArr, TransformType.FORWARD);
		double[] magArr = new double[result.length];
		for (int i = 0; i < result.length; i++) {
			magArr[i] = result[i].abs() / values.length;

		}

		if (magArr.length > number)
			return magArr[number];
		
		return 0.0;
	}

}
