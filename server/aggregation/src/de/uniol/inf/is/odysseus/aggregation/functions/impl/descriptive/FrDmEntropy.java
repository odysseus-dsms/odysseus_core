package de.uniol.inf.is.odysseus.aggregation.functions.impl.descriptive;

import java.util.Arrays;

import org.apache.commons.math3.complex.Complex;
import org.apache.commons.math3.exception.MathIllegalArgumentException;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import org.apache.commons.math3.transform.DftNormalization;
import org.apache.commons.math3.transform.FastFourierTransformer;
import org.apache.commons.math3.transform.TransformType;

import com.google.common.math.IntMath;

/**
 * @author Oormila Ramanandan Kottayi Pilapprathodi
 * @author Michael Suenkel
 *
 */

public class FrDmEntropy extends AbstractAggregationDescriptiveStatisticsFunction {

	@Override
	public double evaluate(double[] values) throws MathIllegalArgumentException {
			
		double[] meanArr = new double[(IntMath.isPowerOfTwo(values.length)) ? values.length
				: IntMath.ceilingPowerOfTwo(values.length)];
		double[] probabilityArr = new double[(IntMath.isPowerOfTwo(values.length)) ? values.length
				: IntMath.ceilingPowerOfTwo(values.length)];
		double[] magArray = new double[(IntMath.isPowerOfTwo(values.length)) ? values.length
				: IntMath.ceilingPowerOfTwo(values.length)];

		DescriptiveStatistics da = new DescriptiveStatistics();
		for (double d : values) {
			da.addValue(d);
		}
		double mean = da.getMean();
		for (double d : values) {
			da.addValue(d);
		}
		for (int i = 0; i < values.length; i++) {
			meanArr[i] = values[i] - mean;
		}
		if (values.length != meanArr.length) {
			for (int i = values.length; i < meanArr.length; i++)
				meanArr[i] = 0.0;
		}
		// TODO: Check if UNITARY and FORWARD are correct
		FastFourierTransformer fft = new FastFourierTransformer(DftNormalization.UNITARY);
		Complex[] result = fft.transform(meanArr, TransformType.FORWARD);

		for (int i = 0; i < result.length; i++) {
			magArray[i] = result[i].abs() / values.length;
		}
		for (int i = 0; i < result.length; i++) {
			probabilityArr[i] = magArray[i] / Arrays.stream(magArray).sum();
		}

		Double frDmEntroPy = calcEntropy(probabilityArr);
		if(frDmEntroPy.isNaN())
			frDmEntroPy=0.0;
		return frDmEntroPy;
	}
	
	private double calcEntropy(double[] probabilityArr) {
		double entropy = 0.0;
		for (int i = 0; i < probabilityArr.length; i++) {
			entropy = entropy + (-(probabilityArr[i]) * ((int) (Math.log(probabilityArr[i]) / Math.log(2))));
		}
		return entropy;
	}

}
