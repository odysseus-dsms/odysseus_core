package de.uniol.inf.is.odysseus.rest2.server.logicaloperator.transform;

import java.util.List;
import java.util.stream.Collectors;

import de.uniol.inf.is.odysseus.core.collection.OptionMap;
import de.uniol.inf.is.odysseus.core.physicaloperator.access.protocol.OdysseusProtocolHandler;
import de.uniol.inf.is.odysseus.core.server.logicaloperator.AccessAO;
import de.uniol.inf.is.odysseus.core.server.metadata.MetadataRegistry;
import de.uniol.inf.is.odysseus.core.server.planmanagement.TransformationConfiguration;
import de.uniol.inf.is.odysseus.core.usermanagement.ISession;
import de.uniol.inf.is.odysseus.core.util.Constants;
import de.uniol.inf.is.odysseus.rest2.common.model.Operator;
import de.uniol.inf.is.odysseus.rest2.common.model.OperatorPortWebsockets;
import de.uniol.inf.is.odysseus.rest2.common.model.Query;
import de.uniol.inf.is.odysseus.rest2.odysseus_client.RESTClient;
import de.uniol.inf.is.odysseus.rest2.server.logicaloperator.OdysseusSourceAO;
import de.uniol.inf.is.odysseus.rest2.server.query.AbstractQueryResultWebsocketEndpoint;
import de.uniol.inf.is.odysseus.ruleengine.rule.RuleException;
import de.uniol.inf.is.odysseus.ruleengine.ruleflow.IRuleFlowGroup;
import de.uniol.inf.is.odysseus.transform.flow.TransformRuleFlowGroup;
import de.uniol.inf.is.odysseus.transform.rule.AbstractTransformationRule;
import de.uniol.inf.is.odysseus.wrapper.websocket.WebSocketClientTransportHandler;

public class OdysseusSourceAOTransformationRule extends AbstractTransformationRule<OdysseusSourceAO> {

	@Override
	public void execute(OdysseusSourceAO operator, TransformationConfiguration config) throws RuleException {
		AccessAO access = new AccessAO();
		access.setTransportHandler(WebSocketClientTransportHandler.NAME);
		access.setDataHandler(operator.getOutputSchema().getType().getSimpleName());
		access.setWrapper(Constants.GENERIC_PUSH);
		access.setProtocolHandler(OdysseusProtocolHandler.NAME);

		RESTClient client = new RESTClient();
		ISession session = client.login(operator.getUsername(), operator.getPassword(), operator.getTenantName(),
				operator.getURL(), false);

		Query q = client.getQuery(operator.getQuery(), session);

		String webSocketURL = null;

		if (q != null) {
			List<Operator> rootOps = q.getRootOperators();
			if (operator.getOperator() != null) {
				rootOps = q.getRootOperators().stream()
						.filter(o -> o.getOperatorDisplayName().equals(operator.getOperator()))
						.collect(Collectors.toList());
				if (rootOps == null || rootOps.isEmpty()) {
					throw new RuleException(
							"Operator " + operator.getOperator() + " not found in Query " + operator.getQuery());
				}
			}

			List<OperatorPortWebsockets> websockets = rootOps.get(0).getPorts().get(0).getWebsockets();

			List<OperatorPortWebsockets> res = websockets.stream()
					.filter(e -> e.getProtocol().equalsIgnoreCase(AbstractQueryResultWebsocketEndpoint.PROTOCOL_BINARY))
					.collect(Collectors.toList());
			if (res != null && !res.isEmpty())
				webSocketURL = res.get(0).getUri();
		}

		if (webSocketURL == null) {
			throw new RuleException("Query " + operator.getQuery() + " not found on " + operator.getURL());
		}

		OptionMap options = new OptionMap();
		options.setOption("uri", operator.getURL() + webSocketURL);

		access.setOptionMap(options);
		access.setAttributes(operator.getOutputSchema().getAttributes());
		access.setLocalMetaAttribute(
				MetadataRegistry.getMetadataType(operator.getOutputSchema().getMetaAttributeNames()));
		access.setReadMetaData(true);
		access.setOverWriteSchemaSourceName(false);

		access.setAccessAOName(operator.getAccessAOName());

		replace(operator, access, config);

		retract(operator);
		insert(access);
	}

	@Override
	public boolean isExecutable(OdysseusSourceAO operator, TransformationConfiguration config) {
		return true;
	}

	@Override
	public IRuleFlowGroup getRuleFlowGroup() {
		return TransformRuleFlowGroup.SUBSTITUTION;
	}

}
