package de.uniol.inf.is.odysseus.rest2.server.logicaloperator;

import de.uniol.inf.is.odysseus.core.logicaloperator.LogicalOperatorCategory;
import de.uniol.inf.is.odysseus.core.server.logicaloperator.AbstractAccessAO;
import de.uniol.inf.is.odysseus.core.server.logicaloperator.AbstractLogicalOperator;
import de.uniol.inf.is.odysseus.core.server.logicaloperator.annotations.LogicalOperator;
import de.uniol.inf.is.odysseus.core.server.logicaloperator.annotations.Parameter;
import de.uniol.inf.is.odysseus.core.server.logicaloperator.builder.StringParameter;
import de.uniol.inf.is.odysseus.core.util.Constants;

@LogicalOperator(maxInputPorts = 0, minInputPorts = 0, name = "OdysseusSource", category={LogicalOperatorCategory.SOURCE}, doc = "Allows to read input via Web-Socket from another Odysseus instance")
public class OdysseusSourceAO extends AbstractAccessAO {

	private static final long serialVersionUID = 2332805915263810044L;
	private String username;
	private String tenantName = "";
	private String password;
	private String url;
	private String operator;
	private String query;
	
	public OdysseusSourceAO() {
		setWrapper(Constants.GENERIC_PUSH);
	}
	
	public OdysseusSourceAO(OdysseusSourceAO other) {
		super(other);
		this.username = other.username;
		this.password = other.password;
		this.url = other.url;
		this.operator = other.operator;
		this.query = other.query;
	}
	
	public String getUsername() {
		return username;
	}

	@Parameter(type=StringParameter.class, doc="The username of the Odysseus serving instance")
	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	@Parameter(type=StringParameter.class, doc="The password of the Odysseus serving instance")
	public void setPassword(String password) {
		this.password = password;
	}

	public String getURL() {
		return url;
	}

	@Parameter(type=StringParameter.class, doc="The url of the Odysseus serving instance")
	public void setURL(String url) {
		this.url = url;
	}

	public String getOperator() {
		return operator;
	}

	@Parameter(type=StringParameter.class, optional = true, doc="The name/id of the sink to connect to at the Odysseus serving instance")
	public void setOperator(String operator) {
		this.operator = operator;
	}

	public String getQuery() {
		return query;
	}

	// TODO: is this param always needed?
	@Parameter(type=StringParameter.class, optional = false, doc="The name/id of the query to connect to at the Odysseus serving instance")
	public void setQuery(String query) {
		this.query = query;
	}

	@Override
	public AbstractLogicalOperator clone() {
		return new OdysseusSourceAO(this);
	}

	@Parameter(type=StringParameter.class, optional = true, doc="The tenant of the Odysseus serving instance. Default is an empty string")
	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}
	
	public String getTenantName() {
		return tenantName;
	}

}
