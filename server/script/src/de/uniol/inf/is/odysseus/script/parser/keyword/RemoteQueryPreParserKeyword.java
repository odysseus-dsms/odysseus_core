package de.uniol.inf.is.odysseus.script.parser.keyword;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import de.uniol.inf.is.odysseus.core.collection.Context;
import de.uniol.inf.is.odysseus.core.collection.OptionMap;
import de.uniol.inf.is.odysseus.core.server.planmanagement.executor.IServerExecutor;
import de.uniol.inf.is.odysseus.core.server.planmanagement.executor.command.IExecutorCommand;
import de.uniol.inf.is.odysseus.core.server.planmanagement.executor.command.RemoteQueryCommand;
import de.uniol.inf.is.odysseus.core.usermanagement.ISession;
import de.uniol.inf.is.odysseus.script.parser.AbstractPreParserKeyword;
import de.uniol.inf.is.odysseus.script.parser.OdysseusScriptException;

/**
 * This is just a wrapper class to wrap a whole script that should be executed somewhere else
 * @author Marco Grawunder
 *
 */
public class RemoteQueryPreParserKeyword extends AbstractPreParserKeyword {

	public static final String NAME = "REMOTEQUERY";
	final List<String> queryText;
	
	public RemoteQueryPreParserKeyword(List<String> queryText) {
		this.queryText = new ArrayList<>(queryText);
	}

	@Override
	public void validate(Map<String, Object> variables, String parameter, ISession caller, Context context,
			IServerExecutor executor) throws OdysseusScriptException {
		// Ignore
	}

	@Override
	public List<IExecutorCommand> execute(Map<String, Object> variables, String parameter, ISession caller,
			Context context, IServerExecutor executor) throws OdysseusScriptException {
		// Create RemoteQueryCommand from queryText, ignore parameters
		// first line:
		// #REMOTEQUERY (key=value) (key=value)
		String params = queryText.get(0).trim().substring(NAME.length()+1);
		OptionMap options = OptionMap.fromString(params);
		// and add all the rest of the query content
		StringBuilder query = new StringBuilder();
		for (int i=1;i<queryText.size();i++) {
			query.append(queryText.get(i)).append("\n");
		}
		RemoteQueryCommand cmd = new RemoteQueryCommand(caller, options, query.toString());
		
		List<IExecutorCommand> list = new ArrayList<>();
		list.add(cmd);
		
		return list;
	}

}
