package de.uniol.inf.is.odysseus.rest2.server.query;

import java.util.UUID;

import javax.websocket.CloseReason;
import javax.websocket.CloseReason.CloseCodes;
import javax.websocket.OnOpen;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;

import org.wso2.transport.http.netty.contract.websocket.WebSocketConnection;

import de.uniol.inf.is.odysseus.core.physicaloperator.IPhysicalOperator;
import de.uniol.inf.is.odysseus.core.server.planmanagement.plan.IExecutionPlan;
import de.uniol.inf.is.odysseus.core.server.planmanagement.query.IPhysicalQuery;
import de.uniol.inf.is.odysseus.core.server.usermanagement.UserManagementProvider;
import de.uniol.inf.is.odysseus.core.usermanagement.ISession;
import de.uniol.inf.is.odysseus.rest2.server.ExecutorServiceBinding;

@ServerEndpoint(value = "/queries/{id}/{operator}/{port}/{protocol}/{securityToken}")
public class QueryResultWebsocketEndpoint extends AbstractQueryResultWebsocketEndpoint {

	@OnOpen
	public void onOpen(@PathParam("id") String id, @PathParam("operator") String operatorName,
			@PathParam("port") String port, @PathParam("protocol") String protocol,
			@PathParam("securityToken") String securityToken, WebSocketConnection session) {
		try {
			ISession odysseusSession = UserManagementProvider.instance.getSessionManagement().login(securityToken);

			if (odysseusSession != null) {
				synchronized (this) {
					IExecutionPlan currentPlan = ExecutorServiceBinding.getExecutor().getExecutionPlan(odysseusSession);
					final IPhysicalQuery query = getQuery(id, odysseusSession, currentPlan);

					IPhysicalOperator operatorP = query.getOperator(UUID.fromString(operatorName));
					handleOperator(port, protocol, session, odysseusSession, operatorP);
				}
			} else {
				// TODO: Handle "Connection refused"
				CloseReason reason = new CloseReason(CloseCodes.CANNOT_ACCEPT, "Login failed");
				closeConnection(session, reason);
			}
		} catch (Exception e) {
			LOGGER.error("Error in onOpen", e);
		}
	}

	
	public static String toWebsocketUrl(ISession session, int queryid, String operator, int port, String protocol) {
		return String.format("/queries/%s/%s/%s/%s/%s",
					String.valueOf(queryid),
					operator,
					String.valueOf(port),
					protocol,
					session.getToken()
				);
	}
}


