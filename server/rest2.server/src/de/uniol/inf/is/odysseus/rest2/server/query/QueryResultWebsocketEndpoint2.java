package de.uniol.inf.is.odysseus.rest2.server.query;

import javax.websocket.CloseReason;
import javax.websocket.CloseReason.CloseCodes;
import javax.websocket.OnOpen;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;

import org.wso2.transport.http.netty.contract.websocket.WebSocketConnection;

import de.uniol.inf.is.odysseus.core.collection.Resource;
import de.uniol.inf.is.odysseus.core.physicaloperator.IPhysicalOperator;
import de.uniol.inf.is.odysseus.core.server.datadictionary.IDataDictionaryWritable;
import de.uniol.inf.is.odysseus.core.server.usermanagement.UserManagementProvider;
import de.uniol.inf.is.odysseus.core.usermanagement.ISession;
import de.uniol.inf.is.odysseus.rest2.server.ExecutorServiceBinding;

@ServerEndpoint(value = "/operators/{id}/{port}/{protocol}/{securityToken}")
public class QueryResultWebsocketEndpoint2 extends AbstractQueryResultWebsocketEndpoint {

	@OnOpen
	public void onOpen(@PathParam("id") String id, @PathParam("port") String port,
			@PathParam("protocol") String protocol, @PathParam("securityToken") String securityToken,
			WebSocketConnection session) {
		try {
			ISession odysseusSession = UserManagementProvider.instance.getSessionManagement().login(securityToken);

			if (odysseusSession != null) {
				synchronized (this) {
					IDataDictionaryWritable dd = ExecutorServiceBinding.getExecutor()
							.getDataDictionary(odysseusSession);
					
					IPhysicalOperator operatorP = dd.getOperator(Resource.specialCreateResource(id, odysseusSession.getUser()), odysseusSession);;
					handleOperator(port, protocol, session, odysseusSession, operatorP);
				}
			} else {
				// TODO: Handle "Connection refused"
				CloseReason reason = new CloseReason(CloseCodes.CANNOT_ACCEPT, "Login failed");
				closeConnection(session, reason);
			}
		} catch (Exception e) {
			LOGGER.error("Error in onOpen", e);
		}
	}

	public static String toWebsocketUrl(ISession session, String operator, int port, String protocol) {
		return String.format("/operator/%s/%s/%s/%s", operator, String.valueOf(port),
				protocol, session.getToken());
	}
}
