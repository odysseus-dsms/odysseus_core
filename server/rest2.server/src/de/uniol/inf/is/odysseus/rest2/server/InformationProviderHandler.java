package de.uniol.inf.is.odysseus.rest2.server;

import java.util.HashMap;
import java.util.Map;

import de.uniol.inf.is.odysseus.core.server.information.IInformationProvider;

public class InformationProviderHandler {

	private static final Map<String, IInformationProvider> information = new HashMap<>();
	
	public void bindInformationProvider(IInformationProvider provider) {
		information.put(provider.getProviderName().toLowerCase(), provider);
	}
	
	public void unbindInformationProvider(IInformationProvider provider) {
		information.remove(provider.getProviderName().toLowerCase());
	}
	
	public static IInformationProvider getProvider(String name) {
		return information.get(name);
	}
	
	
}
