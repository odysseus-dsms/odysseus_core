package de.uniol.inf.is.odysseus.rest2.server.api;

public class NotFoundException extends ApiException {
 
	private static final long serialVersionUID = -2395265728287781054L;
	private int code;
    public NotFoundException (int code, String msg) {
        super(code, msg);
        this.code = code;
    }
    
    public int getCode() {
		return code;
	}
}
