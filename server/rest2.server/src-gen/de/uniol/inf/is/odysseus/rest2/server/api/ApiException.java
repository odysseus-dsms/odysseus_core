package de.uniol.inf.is.odysseus.rest2.server.api;

public class ApiException extends Exception{
 
	private static final long serialVersionUID = 7575175121074140439L;
	private int code;
    public ApiException (int code, String msg) {
        super(msg);
        this.code = code;
    }
    
    public int getCode() {
		return code;
	}
}
