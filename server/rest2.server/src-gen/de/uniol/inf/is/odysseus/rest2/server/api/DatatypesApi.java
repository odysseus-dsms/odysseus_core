package de.uniol.inf.is.odysseus.rest2.server.api;

import java.util.Optional;

import javax.ws.rs.GET;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.wso2.msf4j.Request;

import de.uniol.inf.is.odysseus.core.server.usermanagement.UserManagementProvider;
import de.uniol.inf.is.odysseus.core.usermanagement.ISession;
import de.uniol.inf.is.odysseus.rest2.common.model.Datatype;
import de.uniol.inf.is.odysseus.rest2.server.SecurityAuthInterceptor;
import de.uniol.inf.is.odysseus.rest2.server.api.factories.DatatypesApiServiceFactory;

@Path("/datatypes")


@io.swagger.annotations.Api(description = "the datatypes API")
public class DatatypesApi  extends AbstractApi  {
   private final DatatypesApiService delegate = DatatypesApiServiceFactory.getDatatypesApi();

    @GET
    
    
    @Produces({ "application/json" })
    @io.swagger.annotations.ApiOperation(value = "Returns a list of all available data types.", notes = "", response = Datatype.class, responseContainer = "List", tags={  })
    @io.swagger.annotations.ApiResponses(value = { 
        @io.swagger.annotations.ApiResponse(code = 200, message = "OK", response = Datatype.class, responseContainer = "List") })
    public Response datatypesGet(@Context Request request) {
    	final String securityToken = (String) request.getSession()
				.getAttribute(SecurityAuthInterceptor.SESSION_ATTRIBUTE_NAME);
		final Optional<ISession> session = Optional.ofNullable(UserManagementProvider.instance.getSessionManagement().login(securityToken));
        return delegate.datatypesGet(session);
    }
    
	@OPTIONS
    @Path("/")
	public Response queriesOptionsPath(@Context Request request) throws NotFoundException {
		return super.queriesOptions(request);
	}

}
