package de.uniol.inf.is.odysseus.updater;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.Platform;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.uniol.inf.is.odysseus.core.collection.Pair;
import de.uniol.inf.is.odysseus.core.planmanagement.executor.IExecutor;
import de.uniol.inf.is.odysseus.core.server.usermanagement.UserManagementProvider;
import de.uniol.inf.is.odysseus.core.usermanagement.ISession;

public class RequirementHandler {

	Logger LOG = LoggerFactory.getLogger(RequirementHandler.class);

	void activate() {
	}

	void bindExecutor(IExecutor executor) {

		String v = System.getProperty("INSTALLATIONMODE");
		Boolean runInstaller = v == null ? Boolean.FALSE : Boolean.valueOf(v);
		
		if (runInstaller) {

			Thread runner = new Thread() {
				@Override
				public void run() {
					try {
						handleRequiredInstallations();
					} catch (IOException | JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			};
			runner.start();
		}
	}

	void unbindExecutor(IExecutor executor) {
		// ignore for the moment
	}
	
	private void handleRequiredInstallations() throws IOException, JSONException {

		// wait for application start
		while (!Platform.isRunning()) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		ISession superUser = UserManagementProvider.instance.getSessionManagement().loginSuperUser(null);

		LOG.info("Looking for requirement file in installation dir " + Platform.getInstallLocation().getURL());


		// Before installation of new features, updates needs to be installed
		boolean installRequiredUpdates = true;

		// and install any requirement BEFORE restart
		boolean updatesAvailable = FeatureUpdateUtility.checkForUpdates(superUser);

		if (updatesAvailable && !installRequiredUpdates) {
			LOG.warn("Updates available. Installtion of requirements may fail!");
		}
		
		List<Pair<String, List<String>>> installations = new ArrayList<>();

		
		// Remark Patform.getInstallLocation delivers e.g.
		// file:/H:/git/odysseus_core/.metadata/.plugins/org.eclipse.pde.core/.bundle_pool/
		// getPath:
		// /H:/git/odysseus_core/.metadata/.plugins/org.eclipse.pde.core/.bundle_pool/
		// remove leading /
		
		String filePath = Platform.getInstallLocation().getURL().getPath().substring(1)+"/requirements.json";;
		String jsonString = new String(Files.readAllBytes(Paths.get(filePath)), StandardCharsets.UTF_8);
		System.out.println(jsonString);
		
		JSONArray array = null; 
		try {
			array = new JSONArray(jsonString);
		} catch (JSONException e1) {
			throw new RuntimeException(e1);
		}
		
		for (int i=0;i<array.length();i++) {
				JSONObject entry = array.getJSONObject(i);
				String repoLocation = entry.getString("location");
				JSONArray featureArray = entry.getJSONArray("features");
				List<String> features = new ArrayList<String>();
				for (int f=0;f<featureArray.length();f++) {
					features.add(featureArray.getString(f));
				}
				installations.add(new Pair<>(repoLocation, features));

		}
		

		try {
			// First check, if there is anything to install to avoid infinite loops
			if (FeatureUpdateUtility.anyInstallNeeded(installations, superUser)) {
				FeatureUpdateUtility.install(installations, superUser, false);
				FeatureUpdateUtility.restart(superUser);
			} else {
				// TODO: seems not work
				//FeatureUpdateUtility.exit(superUser);			
				// Just shut down, no state is required
				System.exit(0);			
			}
		} catch (Exception e) {
			LOG.error("Error installing requirements ", e);
		} finally {
			FeatureUpdateUtility.clearRepositoryLocation(superUser);
		}

	}
}
