package de.uniol.inf.is.odysseus.updater;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import org.eclipse.core.runtime.Platform;

public class LoggingHelper {

	private static final String ODYSSEUS = "odysseus";
	private static final String SUFFIX = ".log";

	private static final String NO_LOGS_FOUND = "no logs found";

	static final String[] logLocations = { Platform.getInstallLocation().getURL().getPath().substring(1) + "logs",
			"/var/lib/odysseus/logs/", "/var/log/odysseus/", "/usr/lib/odysseus/" };

	public static final Optional<String> getLogFileLocation() {
		for (String filePath : logLocations) {
			try {
				Path path = Paths.get(filePath);
				if (path.toFile().exists()) {
					return Optional.of(filePath);
				}
			} catch (Exception e) {
				// ignore
			}
		}

		return Optional.ofNullable(null);
	}

	public static String getDefaultLogFile() {
		return ODYSSEUS + SUFFIX;
	}

	public static final String getLogFile() throws IOException {
		return getLogFile(getDefaultLogFile());
	}

	public static final String getLogFile(String name) throws IOException {
		if (name != null && name.startsWith(ODYSSEUS)) {
			return readLogFile(name);
		} else {
			return NO_LOGS_FOUND;
		}
	}

	public static final List<String> getLogFiles() throws IOException {
		List<String> logFiles = new ArrayList<>();
		Optional<String> filePathFolder = getLogFileLocation();
		if (filePathFolder.isPresent()) {
			String path = filePathFolder.get();
			try (Stream<Path> paths = Files.walk(Paths.get(path))) {
				paths.filter(Files::isRegularFile).filter(p -> p.getFileName().startsWith(ODYSSEUS))
						.forEach(p -> logFiles.add(p.toString()));
			}

		}
		return logFiles;

	}

	private static String readLogFile(String logFile) throws IOException {
		Optional<String> filePathFolder = getLogFileLocation();
		if (filePathFolder.isPresent()) {
			Path filePath = Paths.get(filePathFolder.get() + logFile);
			return new String(Files.readAllBytes(filePath), StandardCharsets.UTF_8);
		} else {
			return NO_LOGS_FOUND;
		}
	}

}
