package de.uniol.inf.is.odysseus.core.server.store.function;

import de.uniol.inf.is.odysseus.core.sdf.schema.SDFDatatype;

public class KvStoreReadStringFunction extends AbstractKvStoreReadFunction<Object> {


	private static final long serialVersionUID = -3263090969225261507L;

	public static SDFDatatype[][] accTypes = new SDFDatatype[][]{
		{SDFDatatype.STRING}};

	public KvStoreReadStringFunction(){
		super("KvReadString",accTypes, SDFDatatype.STRING);
	}

	
}
