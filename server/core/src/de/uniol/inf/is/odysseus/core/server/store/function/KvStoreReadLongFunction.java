package de.uniol.inf.is.odysseus.core.server.store.function;

import de.uniol.inf.is.odysseus.core.sdf.schema.SDFDatatype;

public class KvStoreReadLongFunction extends AbstractKvStoreReadFunction<Object> {


	private static final long serialVersionUID = -3263090969225261507L;

	public static SDFDatatype[][] accTypes = new SDFDatatype[][]{
		{SDFDatatype.STRING}};

	public KvStoreReadLongFunction(){
		super("KvReadLong",accTypes, SDFDatatype.LONG);
	}

	
}
