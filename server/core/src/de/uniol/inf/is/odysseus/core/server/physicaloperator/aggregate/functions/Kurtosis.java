package de.uniol.inf.is.odysseus.core.server.physicaloperator.aggregate.functions;

import de.uniol.inf.is.odysseus.core.server.physicaloperator.aggregate.basefunctions.IPartialAggregate;

/**
 * @author Oormila Ramanandan Kottayi Pilapprathodi
 * @author Michael Suenkel
 *
 */

public abstract class Kurtosis<R, W> extends AbstractKurtosis<R, W> {

    /**
     * 
     */
    private static final long serialVersionUID = 5517136001368391558L;

    protected Kurtosis(boolean partialAggregateInput) {
        super("KURTOSIS", partialAggregateInput);
    }

    @Override
    public IPartialAggregate<R> init(IPartialAggregate<R> in) {
        return new KurtosisPartialAggregate<R>((KurtosisPartialAggregate<R>) in);
    }

    @Override
    public IPartialAggregate<R> merge(IPartialAggregate<R> p, R toMerge, boolean createNew) {
        return process_merge(createNew ? p.clone() : p, toMerge);
    }

    @Override
	abstract protected IPartialAggregate<R> process_merge(IPartialAggregate<R> iPartialAggregate, R toMerge);

}
