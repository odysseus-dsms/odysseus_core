/********************************************************************************** 
  * Copyright 2011 The Odysseus Team
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.uniol.inf.is.odysseus.core.server;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.uniol.inf.is.odysseus.core.server.logicaloperator.LogicalOperatorBuilder;

public class Activator implements BundleActivator {
	
	private static Logger LOGGER = LoggerFactory.getLogger("Core");

	private static BundleContext bundleContext;
	private LogicalOperatorBuilder logicalOperatorBuilder;

	private static long startTime;
	
	
	public static BundleContext getBundleContext(){
		return bundleContext;
	
	}
	
	public static long getStartTime() {
		return startTime;
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.osgi.framework.BundleActivator#start(org.osgi.framework.BundleContext
	 * )
	 */
	@Override
	public void start(BundleContext context) throws Exception {	
		LOGGER.info("Starting core server");
		startTime = System.currentTimeMillis();
		logicalOperatorBuilder = new LogicalOperatorBuilder();
		logicalOperatorBuilder.start(context);
		
		bundleContext = context;
		LOGGER.info("Starting core server done.");
	}
	
		/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
	 */
	@Override
	public void stop(BundleContext context) throws Exception {
		logicalOperatorBuilder.stop(context);
		bundleContext = null;
	}

}
