package de.uniol.inf.is.odysseus.core.server.logicaloperator;

import de.uniol.inf.is.odysseus.core.logicaloperator.LogicalOperatorCategory;
import de.uniol.inf.is.odysseus.core.server.logicaloperator.annotations.LogicalOperator;
import de.uniol.inf.is.odysseus.core.server.logicaloperator.annotations.Parameter;
import de.uniol.inf.is.odysseus.core.server.logicaloperator.builder.LongParameter;

@LogicalOperator(name = "SLEEP", minInputPorts = 1, maxInputPorts = 1, doc = "Delays the processing", url = "https://wiki.odysseus.informatik.uni-oldenburg.de/display/ODYSSEUS/Sleep+Operator", category = {
		LogicalOperatorCategory.PROCESSING })
public class SleepAO extends AbstractLogicalOperator {

	private static final long serialVersionUID = 3395239536184373172L;
	
	private long time;
	
	public SleepAO() {
	}
	
	public SleepAO(SleepAO sleepAO) {
		super(sleepAO);
		this.time = sleepAO.time;
	}

	@Parameter(name = "time", type = LongParameter.class, optional = false, doc = "The time this operator should wait before each element is sent to the next operator.")
	public void setTime(long time) {
		this.time = time;
	}
	
	public long getTime() {
		return time;
	}
	
	@Override
	public AbstractLogicalOperator clone() {
		return new SleepAO(this);
	}

}
