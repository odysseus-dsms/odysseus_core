package de.uniol.inf.is.odysseus.core.server.physicaloperator.aggregate.functions;

import java.util.ArrayList;
import java.util.List;

import de.uniol.inf.is.odysseus.core.server.physicaloperator.aggregate.basefunctions.AbstractPartialAggregate;
import org.apache.commons.math3.stat.descriptive.moment.Kurtosis;

/**
 * @author Oormila Ramanandan Kottayi Pilapprathodi
 * @author Michael Suenkel
 *
 */

public class KurtosisPartialAggregate<T> extends AbstractPartialAggregate<T> {

    private static final long serialVersionUID = -7798856857638228860L;

    private final ArrayList<Double> windowSet = new ArrayList<>();
    

    public KurtosisPartialAggregate() {

    }

    public KurtosisPartialAggregate(final KurtosisPartialAggregate<T> medianPartialAggregate) {
        this.windowSet.addAll(medianPartialAggregate.windowSet);
    }

    public Double getKurtosisValue() {
      	    	Double kurtosis = null;
        if (this.windowSet.isEmpty()) {
            return null;
        }
        else {
        	try {
        	Kurtosis k=new Kurtosis();
        	kurtosis = k.evaluate( this.windowSet.stream().mapToDouble(Double::doubleValue).toArray());
        	if(kurtosis.isNaN())
        		kurtosis=0.0;
        	}catch(Exception e) {
        		e.printStackTrace();
        	}
        }
         return kurtosis;
    }

    public KurtosisPartialAggregate<T> add(final Number value) {
        if (value != null) {
                this.windowSet.add(value.doubleValue());
        }
        return this;
    }

    public KurtosisPartialAggregate<T> merge(final KurtosisPartialAggregate<?> value) {
        this.windowSet.addAll(value.windowSet);
        return this;
    }

    public void addAll(final double[] values) {
        for (final double val : values) {
            this.add(val);
        }
    }

    public void addAll(final List<Double> values) {
        for (final double val : values) {
            this.add(val);
        }
    }

    @Override
    public void clear() {
        windowSet.clear();
    }

    @Override
    public KurtosisPartialAggregate<T> clone() {
        return new KurtosisPartialAggregate<>(this);
    }

    @Override
    public String toString() {
        return "KURTOSIS= " + this.getKurtosisValue();
    }

    public static void main(final String[] args) {
        final KurtosisPartialAggregate<?> agg = new KurtosisPartialAggregate<>();
        agg.add(1.0);
        agg.add(3.0);
        agg.add(2.0);
        agg.add(0.0);
        agg.add(5.0);
        agg.add(4.0);
        assert (agg.getKurtosisValue() == 2.5);
        System.out.println(agg);
    }

}
