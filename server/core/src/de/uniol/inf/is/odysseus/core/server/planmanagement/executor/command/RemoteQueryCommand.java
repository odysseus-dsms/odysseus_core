package de.uniol.inf.is.odysseus.core.server.planmanagement.executor.command;

import de.uniol.inf.is.odysseus.core.collection.OptionMap;
import de.uniol.inf.is.odysseus.core.server.datadictionary.IDataDictionaryWritable;
import de.uniol.inf.is.odysseus.core.server.planmanagement.executor.IServerExecutor;
import de.uniol.inf.is.odysseus.core.server.usermanagement.IUserManagementWritable;
import de.uniol.inf.is.odysseus.core.usermanagement.ISession;

public class RemoteQueryCommand extends AbstractExecutorCommand {

	private static final long serialVersionUID = -8629259410619500888L;
	private OptionMap options;
	private String queryText;

	public RemoteQueryCommand(ISession caller, OptionMap options, String queryText) {
		super(caller);
		this.options = options;
		this.queryText = queryText;
	}

	@Override
	public void execute(IDataDictionaryWritable dd, IUserManagementWritable um, IServerExecutor executor) {
		// No need to do anything, as this class is just a wrapper
	}
	
	public OptionMap getOptions() {
		return options;
	}
	
	public String getQueryText() {
		return queryText;
	}

}
