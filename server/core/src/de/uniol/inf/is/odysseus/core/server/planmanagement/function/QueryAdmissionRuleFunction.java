package de.uniol.inf.is.odysseus.core.server.planmanagement.function;

import de.uniol.inf.is.odysseus.core.sdf.schema.SDFDatatype;
import de.uniol.inf.is.odysseus.core.server.planmanagement.query.IPhysicalQuery;

public class QueryAdmissionRuleFunction extends
		AbstractQueryInformationFunction<String> {

	private static final long serialVersionUID = -948427057065250120L;

	public QueryAdmissionRuleFunction() {
		super("AdmissionRule", SDFDatatype.STRING);
	}

	@Override
	public String getValue() {
		int queryId = getNumericalInputValue(0).intValue();
		IPhysicalQuery query = getQuery(queryId);
		System.out.println(query.getDisallowedAdmissionAction().toString());
		return query.getDisallowedAdmissionAction().toString();
	}
}
