package de.uniol.inf.is.odysseus.core.server.physicaloperator.aggregate.functions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.math3.complex.Complex;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import org.apache.commons.math3.transform.DftNormalization;
import org.apache.commons.math3.transform.FastFourierTransformer;
import org.apache.commons.math3.transform.TransformType;

import com.google.common.math.IntMath;

import de.uniol.inf.is.odysseus.core.server.physicaloperator.aggregate.basefunctions.AbstractPartialAggregate;

/**
 * @author Oormila Ramanandan Kottayi Pilapprathodi
 * @author Michael Suenkel
 * @author Marco Grawunder
 *
 */

public class FrPeakFreqPartialAggregate<T> extends AbstractPartialAggregate<T> {

    private static final long serialVersionUID = -7798856857638228860L;

    private final ArrayList<Double> windowSet = new ArrayList<>();
    

    public FrPeakFreqPartialAggregate() {

    }

    public FrPeakFreqPartialAggregate(final FrPeakFreqPartialAggregate<T> medianPartialAggregate) {
        this.windowSet.addAll(medianPartialAggregate.windowSet);
    }

    public Double getFrPeakFreq() {
    	double[] meanArr = null;	
    	double[] magArr = null;	
    	double[] data = null;
    	Double frPeakFreq = null;
        if (this.windowSet.isEmpty()) {
            return null;
        }
        else {
        	try {
        		data = this.windowSet.stream().mapToDouble(Double::doubleValue).toArray();
        		meanArr=new double[(IntMath.isPowerOfTwo(data.length))?
        				data.length:IntMath.ceilingPowerOfTwo(data.length)];
        		DescriptiveStatistics da = new DescriptiveStatistics();
        		for(double d:data) {
        		da.addValue(d);
        		}
        		double mean=da.getMean();
        		for(double d:data) {
            		da.addValue(d);
            		}
        		for(int i=0;i<data.length;i++) {
        			meanArr[i]=data[i]-mean;
        		}
        		if(data.length!=meanArr.length) {
        			for(int i=data.length;i<meanArr.length;i++)
        			meanArr[i]=0.0;
        		}
        		// TODO: Check parameter
        		FastFourierTransformer fft=new FastFourierTransformer(DftNormalization.STANDARD);
        		Complex[] result = fft.transform(meanArr, TransformType.FORWARD);
        		magArr=new double[result.length];
        		for(int i=0;i<result.length;i++) {
        			magArr[i]=result[i].abs()/data.length;
        			
        		}
        		double[] lnSpace = this.linspace(0, 5, magArr.length);
                List<Double> list = new ArrayList<>(magArr.length);
                for (double v: magArr) {
                       list.add(v);
                }
                Arrays.sort(magArr);
                int index=list.indexOf(Double.valueOf(magArr[magArr.length-1]));
        		frPeakFreq = lnSpace[index];   
        	}catch(Exception e) {
        		e.printStackTrace();
        	}
        }
        return frPeakFreq;
    }
    
    private  double[] linspace(double min, double max, int points) {  
        double[] d = new double[points];  
        for (int i = 0; i < points; i++){  
            d[i] = min + i * (max - min) / (points - 1);  
        }  
        return d;  
    }  

    public FrPeakFreqPartialAggregate<T> add(final Number value) {
        if (value != null) {
                this.windowSet.add(value.doubleValue());
        }
        return this;
    }

    public FrPeakFreqPartialAggregate<T> merge(final FrPeakFreqPartialAggregate<?> value) {
        this.windowSet.addAll(value.windowSet);
        return this;
    }

    public void addAll(final double[] values) {
        for (final double val : values) {
            this.add(val);
        }
    }

    public void addAll(final List<Double> values) {
        for (final double val : values) {
            this.add(val);
        }
    }

    @Override
    public void clear() {
        windowSet.clear();
    }

    @Override
    public FrPeakFreqPartialAggregate<T> clone() {
        return new FrPeakFreqPartialAggregate<>(this);
    }

    @Override
    public String toString() {
        return "FrPeakFreq= " + this.getFrPeakFreq();
    }

    public static void main(final String[] args) {
        final FrPeakFreqPartialAggregate<?> agg = new FrPeakFreqPartialAggregate<>();
        agg.add(1.0);
        agg.add(3.0);
        agg.add(2.0);
        agg.add(0.0);
        agg.add(5.0);
        agg.add(4.0);
        assert (agg.getFrPeakFreq() == 2.5);
        System.out.println(agg);
    }

}
