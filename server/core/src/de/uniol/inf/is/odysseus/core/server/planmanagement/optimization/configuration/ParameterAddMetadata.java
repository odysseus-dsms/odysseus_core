package de.uniol.inf.is.odysseus.core.server.planmanagement.optimization.configuration;

import java.util.List;

import de.uniol.inf.is.odysseus.core.server.planmanagement.configuration.Setting;
import de.uniol.inf.is.odysseus.core.server.planmanagement.query.querybuiltparameter.IQueryBuildSetting;

public class ParameterAddMetadata extends Setting<List<String>> implements IQueryBuildSetting<List<String>> {

	private static final long serialVersionUID = 4795246450721103016L;

	public ParameterAddMetadata(List<String> value) {
		super(value);
	}
	
}
