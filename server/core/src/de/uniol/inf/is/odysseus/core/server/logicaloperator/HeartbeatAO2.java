package de.uniol.inf.is.odysseus.core.server.logicaloperator;

import de.uniol.inf.is.odysseus.core.logicaloperator.LogicalOperatorCategory;
import de.uniol.inf.is.odysseus.core.server.logicaloperator.annotations.LogicalOperator;

/**
 * Should no longer be used. Use Punctuation instead
 * 
 * @author Marco Grawunder
 *
 */
@LogicalOperator(minInputPorts = 0, maxInputPorts = 1, name = "Heartbeat2", category = {
		LogicalOperatorCategory.PROCESSING }, deprecation = true, hidden = true, doc = "This operator is deprecated. Use Punctuation instead.")
public class HeartbeatAO2 extends PunctuationAO{


	private static final long serialVersionUID = 3037548406065050976L;

	public HeartbeatAO2() {
	}

	public HeartbeatAO2(HeartbeatAO2 other) {
		super(other);
	}


}
