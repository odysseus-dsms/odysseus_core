package de.uniol.inf.is.odysseus.core.server.logicaloperator;

import de.uniol.inf.is.odysseus.core.logicaloperator.InputOrderRequirement;
import de.uniol.inf.is.odysseus.core.logicaloperator.LogicalOperatorCategory;
import de.uniol.inf.is.odysseus.core.physicaloperator.Heartbeat;
import de.uniol.inf.is.odysseus.core.physicaloperator.PunctuationFactory;
import de.uniol.inf.is.odysseus.core.sdf.schema.SDFExpression;
import de.uniol.inf.is.odysseus.core.server.logicaloperator.annotations.LogicalOperator;
import de.uniol.inf.is.odysseus.core.server.logicaloperator.annotations.Parameter;
import de.uniol.inf.is.odysseus.core.server.logicaloperator.builder.BooleanParameter;
import de.uniol.inf.is.odysseus.core.server.logicaloperator.builder.NamedExpression;
import de.uniol.inf.is.odysseus.core.server.logicaloperator.builder.NamedExpressionParameter;
import de.uniol.inf.is.odysseus.core.server.logicaloperator.builder.StringParameter;

/**
 * TODO
 * 
 * @author Marco Grawunder
 *
 */
@LogicalOperator(minInputPorts = 0, maxInputPorts = 1, name = "Punctuation", category = {
		LogicalOperatorCategory.PROCESSING }, doc = "This operator can create new punctuations.")
public class PunctuationAO extends UnaryLogicalOp{

	private static final long serialVersionUID = -5715561730592890314L;

	private boolean createOnHeartbeat;
	private SDFExpression timeExpression;
	private SDFExpression fireOn;
	private boolean suppressFireOnElement = false;
	private boolean punctuationAfterElement = false;
	private byte punctuationType = Heartbeat.NUMBER;

	public PunctuationAO() {
	}

	public PunctuationAO(PunctuationAO other) {
		super(other);
		this.createOnHeartbeat = other.createOnHeartbeat;
		this.timeExpression = other.timeExpression.clone();
		if (other.fireOn != null) {
			this.fireOn = other.fireOn.clone();
		}
		this.suppressFireOnElement = other.suppressFireOnElement;
		this.punctuationType = other.punctuationType;
		this.punctuationAfterElement = other.punctuationAfterElement;
	}

	@Parameter(name="punctuationType", type = StringParameter.class, optional = true, doc = "The type of the punctuation that should be created. Only Heartbeat and ClearState are supported at the moment. If not given, Heartbeat will be used.")
	public void setPunctuationType2(String punctuationType) {
		this.punctuationType = PunctuationFactory.getType(punctuationType);
	}
	
	public byte getPunctuationType() {
		return punctuationType;
	}
	
	@Parameter(type = BooleanParameter.class, optional = true, doc = "If set to true, for every new incomming punctuation the will be a new HEARTBEAT with the value of the current time parameter.")
	public void setCreateOnHeartbeat(boolean createOnHeartbeat) {
		this.createOnHeartbeat = createOnHeartbeat;
	}

	public boolean isCreateOnHeartbeat() {
		return createOnHeartbeat;
	}

	@Parameter(name = "TIME", type = NamedExpressionParameter.class, optional = false, doc = "Calculate the time value for the HEARTBEAT that is created.")
	public void setTimeExpression2(NamedExpression timeExpression) {
		this.timeExpression = timeExpression.expression;
	}

	public SDFExpression getTimeExpression() {
		return timeExpression;
	}

	public SDFExpression getFireOn() {
		return fireOn;
	}

	@Parameter(name = "fireOn", type = NamedExpressionParameter.class, optional = true, doc = "If this expression is evaluated to true, a PUNCTUATION with the value of the current time parameter ist send.")
	public void setFireOn2(NamedExpression createWhen) {
		this.fireOn = createWhen.expression;
	}

	public boolean isSuppressFireOnElement() {
		return suppressFireOnElement;
	}

	@Parameter(name = "suppressFireOnElement", type = BooleanParameter.class, optional = true, doc = "Typically, all read elements are written to output. Set this flag to false to suppres those elements that trigger the PUNCTUATION")
	public void setSuppressFireOnElement(boolean suppressFireOnElement) {
		this.suppressFireOnElement = suppressFireOnElement;
	}
	
	@Parameter(name = "punctuationAfterElement", type = BooleanParameter.class, optional = true, doc = "Typically, the PUNCTUATION is written before the element. Set to true, to send first the element and after that the PUNCTUATION.")
	public void setPunctuationAfterElement(boolean punctuationAfterElement) {
		this.punctuationAfterElement = punctuationAfterElement;
	}

	public boolean isPunctuationAfterElement() {
		return punctuationAfterElement;
	}
	
	@Override
	public AbstractLogicalOperator clone() {
		return new PunctuationAO(this);
	}

	@Override
	public InputOrderRequirement getInputOrderRequirement(int inputPort) {
		return InputOrderRequirement.NONE;
	}
	
	@Override
	public boolean isValid() {
		boolean isValid = true;
		if (punctuationType < 0) {
			addError("Sorry. Not supported punctuation type");
			isValid = false;
		}
		return isValid && super.isValid() ;
	}

}
