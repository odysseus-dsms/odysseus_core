package de.uniol.inf.is.odysseus.core.server.physicaloperator.aggregate.functions;

import de.uniol.inf.is.odysseus.core.server.physicaloperator.aggregate.basefunctions.AbstractAggregateFunction;
import de.uniol.inf.is.odysseus.core.server.physicaloperator.aggregate.basefunctions.IPartialAggregate;

/**
 * @author Oormila Ramanandan Kottayi Pilapprathodi
 * @author Michael Suenkel
 *
 */

abstract public class AbstractFrPeakFreq<R, W> extends AbstractAggregateFunction<R, W> {

    /**
     * 
     */
    private static final long serialVersionUID = -6053900953263781699L;

    /**
     * 
     * @param partialAggregateInput
     */
    protected AbstractFrPeakFreq(final boolean partialAggregateInput) {
        super("FrPeakFreq", partialAggregateInput);
    }

    /**
     * 
     * @param name
     * @param partialAggregateInput
     */
    protected AbstractFrPeakFreq(String name, final boolean partialAggregateInput) {
        super(name, partialAggregateInput);
    }

    /**
     * 
     * {@inheritDoc}
     */
    @Override
    public IPartialAggregate<R> merge(IPartialAggregate<R> p, R toMerge, boolean createNew) {
        return process_merge(createNew ? p.clone() : p, toMerge);
    }

    /**
     * 
     * @param iPartialAggregate
     * @param toMerge
     * @return
     */
    abstract protected IPartialAggregate<R> process_merge(IPartialAggregate<R> iPartialAggregate, R toMerge);

}
