package de.uniol.inf.is.odysseus.core.server.physicaloperator;

import de.uniol.inf.is.odysseus.core.IClone;
import de.uniol.inf.is.odysseus.core.metadata.PointInTime;
import de.uniol.inf.is.odysseus.core.physicaloperator.IPunctuation;

public class MinProgressHandler implements IClone{

	
	private boolean outOfOrder = false;
	private PointInTime[] progresses = null;
	private PointInTime minProgress = null;
	private int portWithLeastProgress = 0;
	
	public MinProgressHandler(boolean outOfOrder) {
		this.outOfOrder = outOfOrder;
	}
	
	public MinProgressHandler(MinProgressHandler mpg) {
		this.outOfOrder = mpg.outOfOrder;
		this.progresses = mpg.progresses.clone();
		this.minProgress = mpg.minProgress;
		this.portWithLeastProgress = mpg.portWithLeastProgress;
	}
	
	@Override
	public MinProgressHandler clone() {
		return new MinProgressHandler(this);
	}
	
	public void init(int numOfPorts) {
		if(this.outOfOrder) {
			this.progresses = new PointInTime[numOfPorts];
		}
	}
	
	public PointInTime getMinProgress() {
		return minProgress;
	}
	
	public boolean isOutOfOrder() {
		return outOfOrder;
	}
	
	public PointInTime handleHeartbeat(IPunctuation punct, int port) {
		if(punct.isHeartbeat()) {
			PointInTime prog = progresses[port]; 
			if(prog == null || punct.after(prog)) {
				progresses[port] = punct.getTime();
				if(port == portWithLeastProgress) {
					PointInTime newMinProg = null;
					for(int i = 0; i < progresses.length; i++) {
						PointInTime prog_i = progresses[i];
						if(prog_i == null) {
							portWithLeastProgress = i;
							return null; // there is no minimal Progress
							//because there are ports that haven't received any progress; 
						}
						if(newMinProg == null) {
							portWithLeastProgress = i;
							newMinProg = prog_i;
							continue;
						}
						if(prog_i.before(newMinProg)) {
							portWithLeastProgress = i;
							newMinProg = prog_i;
						}
					}
					minProgress = newMinProg;
					return minProgress;
				}
			}
		}
		return null;
	}
}
