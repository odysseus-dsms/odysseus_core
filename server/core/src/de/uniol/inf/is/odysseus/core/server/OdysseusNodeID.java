package de.uniol.inf.is.odysseus.core.server;

import java.io.Serializable;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Preconditions;

import de.uniol.inf.is.odysseus.core.IOdysseusNodeID;
import de.uniol.inf.is.odysseus.core.server.information.IInformationProvider;
import de.uniol.inf.is.odysseus.core.usermanagement.ISession;

public final class OdysseusNodeID implements Serializable, IInformationProvider, IOdysseusNodeID{
	
	private static final Logger LOG = LoggerFactory.getLogger(OdysseusNodeID.class);

	private static final long serialVersionUID = -3068104616795173918L;
	
	private final UUID uuid;
	
	public final static IOdysseusNodeID NODE_ID = generateNew(); 
	public final static IOdysseusNodeID UNDEFINED_ID = new OdysseusNodeID(); 
	
	
	// Default constructor for declarative service IInformationProvider
	public OdysseusNodeID(){
		uuid = null;
	}
	
	OdysseusNodeID(UUID uuid) {
		Preconditions.checkNotNull(uuid, "uuid must not be null!");

		this.uuid = uuid;
	}
	
	public static IOdysseusNodeID fromString( String text ) {
		try {
			UUID uuid = UUID.fromString(text.trim());
			return new OdysseusNodeID(uuid);
		}catch(Exception e) {
			throw new RuntimeException(e.getMessage()+ " from "+text,e);
		}
	}
	
	private static IOdysseusNodeID generateNew() {
		OdysseusNodeID node = new OdysseusNodeID(UUID.randomUUID());
		LOG.info("Odysseus ID created: {} ",node);
		return node;
	}
	
	@Override
	public String toString() {
		return uuid.toString();
	}	
	
	@Override
	public boolean equals(Object obj) {
		if( !(obj instanceof OdysseusNodeID)) {
			return false;
		}
		if( obj == this ) {
			return true;
		}
		
		OdysseusNodeID other = (OdysseusNodeID)obj;
		return this.uuid.equals(other.uuid);
	}
	
	@Override
	public int hashCode() {
		return uuid.hashCode();
	}

	@Override
	public int compareTo(IOdysseusNodeID other) {
		return uuid.compareTo(other.getUUID());
	}

	@Override
	public String getProviderName() {
		return "odysseusid";
	}

	@Override
	public String getInformation(ISession session) {
		StringBuilder idInfo = new StringBuilder();
		idInfo.append("{\"id\":\"").append(OdysseusNodeID.NODE_ID).append("\"}");
		return idInfo.toString();
	}

	@Override
	public UUID getUUID() {
		return uuid;
	}

}
