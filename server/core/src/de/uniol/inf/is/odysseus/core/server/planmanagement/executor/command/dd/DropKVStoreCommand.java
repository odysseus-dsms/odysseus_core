package de.uniol.inf.is.odysseus.core.server.planmanagement.executor.command.dd;

import de.uniol.inf.is.odysseus.core.collection.OptionMap;
import de.uniol.inf.is.odysseus.core.server.datadictionary.IDataDictionaryWritable;
import de.uniol.inf.is.odysseus.core.server.planmanagement.QueryParseException;
import de.uniol.inf.is.odysseus.core.server.planmanagement.executor.IServerExecutor;
import de.uniol.inf.is.odysseus.core.server.planmanagement.executor.command.AbstractExecutorCommand;
import de.uniol.inf.is.odysseus.core.server.usermanagement.IUserManagementWritable;
import de.uniol.inf.is.odysseus.core.usermanagement.ISession;

public class DropKVStoreCommand extends AbstractExecutorCommand {
	
	private static final long serialVersionUID = -5069166204918167959L;

	private static final String optionsNameIgnoreIfExists ="ignoreIfExists";
	
	private String name;
	private OptionMap options;

	public DropKVStoreCommand(String name, OptionMap options, ISession caller) {
		super(caller);
		this.name = name;
		this.options = options;
	}

	@Override
	public void execute(IDataDictionaryWritable dd, IUserManagementWritable um, IServerExecutor executor) {
		if (!dd.containsStore(name, getCaller())){
			if(options.getBoolean(optionsNameIgnoreIfExists, true)) {
				// store does not exist and it is wanted to ignore the drop
				return;
			} else {
				// store does not exist and it is NOT wanted to ignore the drop
				throw new QueryParseException("Store with name "+name+" does not exist.");
			}
		}
		dd.removeStore(name, getCaller());
	}

}
