package de.uniol.inf.is.odysseus.core.server.store.function;

import de.uniol.inf.is.odysseus.core.sdf.schema.SDFDatatype;
import de.uniol.inf.is.odysseus.core.server.store.IStore;

public abstract class AbstractKvStoreReadFunction<T> extends AbstractNamedStoreFunction<T> {


	private static final long serialVersionUID = -3263090969225261507L;

	public AbstractKvStoreReadFunction(String name, SDFDatatype[][] accTypes, SDFDatatype returnType){
		super(name, accTypes, returnType, false);
	}

	
	@SuppressWarnings("unchecked")
	@Override
	public T getValue() {

		IStore<Comparable<?>, Object> store = getStore(); 
		Comparable<?> valueKey;
		
		if (getArity() == 1) {
			valueKey = getInputValue(0);
		}else {
			valueKey = getInputValue(1);
		}

				
		if (store != null){
			return (T) store.get(valueKey);
		}else{
			return null;
		}
	}
}
