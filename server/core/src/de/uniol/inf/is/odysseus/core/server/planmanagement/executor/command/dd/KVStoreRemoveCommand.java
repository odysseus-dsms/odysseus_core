package de.uniol.inf.is.odysseus.core.server.planmanagement.executor.command.dd;

import de.uniol.inf.is.odysseus.core.server.datadictionary.IDataDictionaryWritable;
import de.uniol.inf.is.odysseus.core.server.planmanagement.executor.IServerExecutor;
import de.uniol.inf.is.odysseus.core.server.planmanagement.executor.command.AbstractExecutorCommand;
import de.uniol.inf.is.odysseus.core.server.store.IStore;
import de.uniol.inf.is.odysseus.core.server.usermanagement.IUserManagementWritable;
import de.uniol.inf.is.odysseus.core.usermanagement.ISession;

public class KVStoreRemoveCommand extends AbstractExecutorCommand {

	private static final long serialVersionUID = 9178099993534874528L;
	
	private String name;
	private String key;


	public KVStoreRemoveCommand(String name, String key, ISession caller) {
		super(caller);
		this.name = name;
		this.key = key;
	}

	@Override
	public void execute(IDataDictionaryWritable dd, IUserManagementWritable um, IServerExecutor executor) {
		IStore<Comparable<?>, Object> store = dd.getStore(name, getCaller()); 
		// getStore will throw an exception, so no handling necessary
		store.remove(key);
	}

}
