/********************************************************************************** 
 * Copyright 2011 The Odysseus Team
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*
 * Created on 07.12.2004
 *
 */
package de.uniol.inf.is.odysseus.core.server.logicaloperator;

import de.uniol.inf.is.odysseus.core.logicaloperator.LogicalOperatorCategory;
import de.uniol.inf.is.odysseus.core.predicate.IPredicate;
import de.uniol.inf.is.odysseus.core.sdf.schema.SDFSchema;
import de.uniol.inf.is.odysseus.core.sdf.schema.SDFSchemaFactory;
import de.uniol.inf.is.odysseus.core.server.logicaloperator.annotations.LogicalOperator;
import de.uniol.inf.is.odysseus.core.server.logicaloperator.annotations.Parameter;
import de.uniol.inf.is.odysseus.core.server.logicaloperator.builder.BooleanParameter;
import de.uniol.inf.is.odysseus.core.server.physicaloperator.IHasPredicate;

/**
 * @author Marco Grawunder
 *
 */
@LogicalOperator(name = "DIFFERENCE", minInputPorts = 2, maxInputPorts = 2, doc = "This operator calculates the difference between two input sets.", url = "https://wiki.odysseus.informatik.uni-oldenburg.de/display/ODYSSEUS/Difference+operator", category = {
        LogicalOperatorCategory.BASE, LogicalOperatorCategory.SET })
public class DifferenceAO extends BinaryLogicalOp implements IHasPredicate, IOutOfOrderHandler {
	
	OrderType order = OrderType.NOT_SET;

    private static final long serialVersionUID = 4518770628909423647L;
    private IPredicate<?> predicate;

    public DifferenceAO(DifferenceAO differenceAO) {
        super(differenceAO);
        if (differenceAO.predicate != null) {
            this.predicate = differenceAO.predicate;
        }
        this.order = differenceAO.order;
    }

    public DifferenceAO() {
        super();
    }

    public synchronized void setPredicate(IPredicate<?> predicate) {
        this.predicate = predicate;
    }

    @Override
    public IPredicate<?> getPredicate() {
        return predicate;
    }

    public @Override DifferenceAO clone() {
        return new DifferenceAO(this);
    }

	@Override
	public OrderType getOrderType() {
		return order;
	}

	@Override
	@Parameter(type = BooleanParameter.class, optional = true, doc = "If set to false, the operator will not garantee order in output. Default is true")
	public void setAssureOrder(boolean assureOrder) {
		if(assureOrder) {
			this.order = OrderType.IN_ORDER;
		} else {
			this.order = OrderType.OUT_OF_ORDER;
		}
	}

	@Override
	public SDFSchema getOutputSchemaIntern(int pos) {
		boolean outOfOrder = !(getInputSchema(0).isInOrder() && getInputSchema(1).isInOrder()) || isOutOfOrder();
		return SDFSchemaFactory.createNewWithOutOfOrder(outOfOrder, this.getInputSchema(LEFT));
	}

}
