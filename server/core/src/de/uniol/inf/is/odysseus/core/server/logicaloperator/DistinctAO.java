package de.uniol.inf.is.odysseus.core.server.logicaloperator;

import de.uniol.inf.is.odysseus.core.logicaloperator.LogicalOperatorCategory;
import de.uniol.inf.is.odysseus.core.sdf.schema.SDFSchema;
import de.uniol.inf.is.odysseus.core.sdf.schema.SDFSchemaFactory;
import de.uniol.inf.is.odysseus.core.server.logicaloperator.annotations.LogicalOperator;
import de.uniol.inf.is.odysseus.core.server.logicaloperator.annotations.Parameter;
import de.uniol.inf.is.odysseus.core.server.logicaloperator.builder.BooleanParameter;

/**
 * 
 * @author Marco Grawunder
 * @author Christian Kuka <christian@kuka.cc>
 * 
 */
@LogicalOperator(maxInputPorts = 1, minInputPorts = 1, name = "DISTINCT", doc = "This operator removes duplicates.", category = { LogicalOperatorCategory.BASE })
public class DistinctAO extends UnaryLogicalOp implements IOutOfOrderHandler {

    private static final long serialVersionUID = -1992998023364461468L;
    private OrderType order = OrderType.NOT_SET;

    // private List<SDFAttribute> attributes;
    /**
     * Class constructor.
     *
     */
    public DistinctAO() {
        super();
    }

    public DistinctAO(DistinctAO distinctAO) {
        super(distinctAO);
        this.order = distinctAO.order;
        // this.attributes = distinctAO.attributes;
    }

    // @Parameter(type = ResolvedSDFAttributeParameter.class, name =
    // "ATTRIBUTES", optional = true, isList = true, doc =
    // "A list of attributes that should be used.")
    // public void setAttr(List<SDFAttribute> outputSchema) {
    // this.attributes = outputSchema;
    // }
    //
    // @Override
    // protected SDFSchema getOutputSchemaIntern(int pos) {
    // if (pos > 0) {
    // throw new
    // IllegalArgumentException("No output port other than 0 defined!");
    // }
    // return new SDFSchema(getInputAO().getOutputSchema(), attributes);
    // }
    //
    // public List<SDFAttribute> getAttributes() {
    // return attributes;
    // }

    @Override
    public AbstractLogicalOperator clone() {
        return new DistinctAO(this);
    }

	@Override
	public OrderType getOrderType() {
		return order;
	}

	@Override
	@Parameter(type = BooleanParameter.class, optional = true, doc = "If set to false, the operator will not garantee order in output. Default is true")
	public void setAssureOrder(boolean assureOrder) {
		if(assureOrder) {
			this.order = OrderType.IN_ORDER;
		} else {
			this.order = OrderType.OUT_OF_ORDER;
		}
	}
	
	@Override
	public SDFSchema getOutputSchemaIntern(int pos) {
		boolean outOfOrder = !getInputSchema().isInOrder() || this.isOutOfOrder();
		return SDFSchemaFactory.createNewWithOutOfOrder(outOfOrder, getInputSchema());
	}

}
