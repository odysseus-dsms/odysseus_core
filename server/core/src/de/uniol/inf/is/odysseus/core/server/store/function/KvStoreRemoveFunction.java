package de.uniol.inf.is.odysseus.core.server.store.function;

import de.uniol.inf.is.odysseus.core.sdf.schema.SDFDatatype;
import de.uniol.inf.is.odysseus.core.server.store.IStore;

public class KvStoreRemoveFunction extends AbstractNamedStoreFunction<Object> {

	private static final long serialVersionUID = -3263090969225261507L;

	public static SDFDatatype[][] accTypes = new SDFDatatype[][] { { SDFDatatype.STRING } };

	public KvStoreRemoveFunction() {
		super("kvremove", accTypes, SDFDatatype.OBJECT, false);
	}

	@Override
	public Object getValue() {
		String valueKey = getInputValue(0);

		IStore<Comparable<?>, Object> store = getStore("");

		if (store != null) {
			Object val = store.remove(valueKey);
			return val;
		} else {
			return null;
		}
	}


}
