package de.uniol.inf.is.odysseus.core.server.remote;

import java.util.List;

import de.uniol.inf.is.odysseus.core.planmanagement.executor.IExecutor;
import de.uniol.inf.is.odysseus.core.server.planmanagement.executor.command.IExecutorCommand;
import de.uniol.inf.is.odysseus.core.usermanagement.ISession;

public interface IRemoteQueryCommandHandler {

	String getName();

	void handle(IExecutor standardExecutor, ISession caller, List<IExecutorCommand> remoteCommands);

}
