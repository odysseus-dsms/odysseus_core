package de.uniol.inf.is.odysseus.core.server.planmanagement.executor;

import de.uniol.inf.is.odysseus.core.collection.Context;
import de.uniol.inf.is.odysseus.core.collection.OptionMap;
import de.uniol.inf.is.odysseus.core.planmanagement.query.ILogicalQuery;
import de.uniol.inf.is.odysseus.core.server.planmanagement.query.querybuiltparameter.QueryBuildConfiguration;
import de.uniol.inf.is.odysseus.core.usermanagement.ISession;

public interface IPreTransformationHandler {
	String getName();

	void preTransform(IServerExecutor executor, ISession caller, ILogicalQuery query, QueryBuildConfiguration config,
			OptionMap handlerParameters, Context context);
}
