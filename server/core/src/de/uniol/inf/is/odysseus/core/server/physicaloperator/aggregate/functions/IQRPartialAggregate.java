package de.uniol.inf.is.odysseus.core.server.physicaloperator.aggregate.functions;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;

import de.uniol.inf.is.odysseus.core.server.physicaloperator.aggregate.basefunctions.AbstractPartialAggregate;

/**
 * @author Oormila Ramanandan Kottayi Pilapprathodi
 * @author Michael Suenkel
 *
 */

public class IQRPartialAggregate<T> extends AbstractPartialAggregate<T> {

	private static final long serialVersionUID = -7798856857638228860L;

	private final ArrayList<Double> windowSet = new ArrayList<>();

	public IQRPartialAggregate() {

	}

	public IQRPartialAggregate(final IQRPartialAggregate<T> medianPartialAggregate) {
		this.windowSet.addAll(medianPartialAggregate.windowSet);
	}

	public Double getIQRValue() {
		Double iqr = null;
		if (this.windowSet.isEmpty()) {
			return null;
		} else {
			try {
				double[] data = this.windowSet.stream().mapToDouble(Double::doubleValue).toArray();
				DescriptiveStatistics da = new DescriptiveStatistics();
				for (double d : data) {
					da.addValue(d);
				}
				iqr = da.getPercentile(75) - da.getPercentile(25);
				if(iqr.isNaN())
					iqr=0.0;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return iqr;
	}

	public IQRPartialAggregate<T> add(final Number value) {
		if (value != null) {
			this.windowSet.add(value.doubleValue());
		}
		return this;
	}

	public IQRPartialAggregate<T> merge(final IQRPartialAggregate<?> value) {
		this.windowSet.addAll(value.windowSet);
		return this;
	}

	public void addAll(final double[] values) {
		for (final double val : values) {
			this.add(val);
		}
	}

	public void addAll(final List<Double> values) {
		for (final double val : values) {
			this.add(val);
		}
	}

	@Override
	public void clear() {
		windowSet.clear();
	}

	@Override
	public IQRPartialAggregate<T> clone() {
		return new IQRPartialAggregate<>(this);
	}

	@Override
	public String toString() {
		return "IQR= " + this.getIQRValue();
	}

	public static void main(final String[] args) {
		final IQRPartialAggregate<?> agg = new IQRPartialAggregate<>();
		agg.add(1.0);
		agg.add(3.0);
		agg.add(2.0);
		agg.add(0.0);
		agg.add(5.0);
		agg.add(4.0);
		assert (agg.getIQRValue() == 2.5);
		System.out.println(agg);
	}

}
