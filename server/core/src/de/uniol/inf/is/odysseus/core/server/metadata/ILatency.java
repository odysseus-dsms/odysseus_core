/********************************************************************************** 
  * Copyright 2011 The Odysseus Team
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  *     http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package de.uniol.inf.is.odysseus.core.server.metadata;

import java.util.Map;

import de.uniol.inf.is.odysseus.core.metadata.IMetaAttribute;

/**
 * This interface is used to handle latency meta data
 * 
 * Latency is the time for a stream object from entering the system until setting latencyEnd
 * (typically by using an special operator CalcLatency). 
 * 
 * @author marco grawunder
 *
 */

public interface ILatency extends IMetaAttribute{
	void setMinLatencyStart(long timestamp);
	void setMaxLatencyStart(long timestamp);
	void setLatencyEnd(long timestamp);
	void setLatencyEnd(String key, long timestamp);
	void setLatencyMeasurements(Map<String, Long> measurements);
	Map<String, Long> getLatencyMeasurements();
	long getLatencyEnd(String key);
	long getLatencyStart();
	long getMaxLatency();
	long getLatencyEnd();
	long getLatency();
//	@Override
//	ILatency clone();
	long getMaxLatencyStart();
}
