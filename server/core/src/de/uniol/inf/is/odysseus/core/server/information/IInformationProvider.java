package de.uniol.inf.is.odysseus.core.server.information;

import de.uniol.inf.is.odysseus.core.usermanagement.ISession;

public interface IInformationProvider {

	/**
	 * The name of the information provider
	 * @return
	 */
	String getProviderName();
	
	/**
	 * The information that this provider delivers
	 * @return
	 */
	String getInformation(ISession session);
	

}
