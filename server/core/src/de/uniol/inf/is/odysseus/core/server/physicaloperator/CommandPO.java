package de.uniol.inf.is.odysseus.core.server.physicaloperator;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.uniol.inf.is.odysseus.core.collection.History;
import de.uniol.inf.is.odysseus.core.collection.Tuple;
import de.uniol.inf.is.odysseus.core.command.Command;
import de.uniol.inf.is.odysseus.core.expression.RelationalExpression;
import de.uniol.inf.is.odysseus.core.metadata.IMetaAttribute;
import de.uniol.inf.is.odysseus.core.physicaloperator.IPhysicalOperator;
import de.uniol.inf.is.odysseus.core.physicaloperator.IPunctuation;
import de.uniol.inf.is.odysseus.core.physicaloperator.OpenFailedException;
import de.uniol.inf.is.odysseus.core.sdf.schema.DirectAttributeResolver;
import de.uniol.inf.is.odysseus.core.sdf.schema.SDFExpression;
import de.uniol.inf.is.odysseus.core.server.logicaloperator.CommandAO;
import de.uniol.inf.is.odysseus.core.server.physicaloperator.aggregate.AggregateFunctionBuilderRegistry;
import de.uniol.inf.is.odysseus.core.server.physicaloperator.sink.SenderPO;
import de.uniol.inf.is.odysseus.core.server.planmanagement.executor.IServerExecutor;
import de.uniol.inf.is.odysseus.core.usermanagement.ISession;
import de.uniol.inf.is.odysseus.mep.MEP;

public class CommandPO extends AbstractPipe<Tuple<IMetaAttribute>, Tuple<IMetaAttribute>> {
	static Logger LOG = LoggerFactory.getLogger(SenderPO.class);

	private final RelationalExpression<IMetaAttribute> commandExpression;
	private final Map<Class<? extends IPunctuation>, RelationalExpression<IMetaAttribute>> punctuationExpression = new HashMap<>();
	private final IServerExecutor executor;
	private ISession caller;
	private final HashMap<String, String> reactOnPunctuations = new HashMap<>();
	private final boolean suppressPunctuationReactedOn;

	public CommandPO(CommandAO ao, IServerExecutor executor) {
		Objects.requireNonNull(ao, "ao must not be null!");

		this.executor = executor;
		if (ao.getCommandExpression() != null) {
			this.commandExpression = new RelationalExpression<IMetaAttribute>(ao.getCommandExpression());
			this.commandExpression.initVars(ao.getInputSchema(0));
		} else {
			commandExpression = null;
		}
		for (String key : ao.getReactOnPunctuations().getKeySet()) {
			this.reactOnPunctuations.put(key, ao.getReactOnPunctuations().getString(key));
		}
		this.suppressPunctuationReactedOn = ao.isSuppressPunctuationReactedOn();
	}

	@Override
	public OutputMode getOutputMode() {
		return OutputMode.INPUT;
	}

	@Override
	protected void process_open() throws OpenFailedException {
		List<ISession> callers = getSessions();
		if (callers.size() != 1) {
			throw new OpenFailedException("This operator cannot be shared");
		}
		caller = callers.get(0);
	}

	@Override
	protected void process_next(Tuple<IMetaAttribute> object, int port) {
		if (commandExpression != null) {
			try {
				evalutateAndRunCommand(commandExpression, object);

			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
		transfer(object);
	}

	private void evalutateAndRunCommand(RelationalExpression<IMetaAttribute> commandExpression,
			Tuple<IMetaAttribute> object) {
		History<Tuple<IMetaAttribute>> preProcessResult = null;
		Command command = (Command) commandExpression.evaluate(object, getSessions(), preProcessResult);

		executor.runCommand(command, caller);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void processPunctuation(IPunctuation punctuation, int port) {
		boolean reactedOn = false;
		String punctExpression = reactOnPunctuations.get(punctuation.getClass().getSimpleName().toLowerCase());
		if (punctExpression != null) {
			reactedOn = true;
			RelationalExpression<IMetaAttribute> expression = punctuationExpression.get(punctuation.getClass());
			if (expression == null) {
				SDFExpression sdfExpression = new SDFExpression("", punctExpression,
						new DirectAttributeResolver(punctuation.getSchema()), MEP.getInstance(),
						AggregateFunctionBuilderRegistry.getAggregatePattern());
				expression = new RelationalExpression<>(sdfExpression);
				expression.initVars(punctuation.getSchema());
				punctuationExpression.put(punctuation.getClass(), expression);
			}
			try {
				evalutateAndRunCommand(expression, (Tuple<IMetaAttribute>) punctuation.getValue());
			}catch(Exception e) {
				LOG.error("Error evaluating expression "+expression+" with input "+punctuation.getValue(),e);
			}
		}
		if (!suppressPunctuationReactedOn) {
			sendPunctuation(punctuation);
		} else {
			if (!reactedOn) {
				sendPunctuation(punctuation);
			}
		}

	}

	@Override
	public boolean isSemanticallyEqual(IPhysicalOperator ipo) {
		return false;
	}

}
