package de.uniol.inf.is.odysseus.core.server.information;

import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import de.uniol.inf.is.odysseus.core.option.OptionParameter;
import de.uniol.inf.is.odysseus.core.physicaloperator.access.protocol.IProtocolHandlerRegistry;
import de.uniol.inf.is.odysseus.core.usermanagement.ISession;

public class ProtocolHandlerInformationProvider implements IInformationProvider {

	final Gson gson;
	private IProtocolHandlerRegistry registry;

	public ProtocolHandlerInformationProvider() {
		GsonBuilder builder = new GsonBuilder();
		builder.registerTypeHierarchyAdapter(OptionParameter.class, new OptionParameterAdapter());
		gson = builder.create();
	}

	@Override
	public String getProviderName() {
		return "protocolhandler";
	}

	@Override
	public String getInformation(ISession session) {
		Map<String, Set<OptionParameter>> parameters = new TreeMap<>(registry.getParameters());
		return gson.toJson(parameters);
	}

	void bindProtocolHandlerRegistry(IProtocolHandlerRegistry registry) {
		this.registry = registry;
	}
	
	void unbindProtocolHandlerRegistry(IProtocolHandlerRegistry registry) {
		this.registry = null;
	}
	
	
}
