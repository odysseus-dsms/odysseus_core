package de.uniol.inf.is.odysseus.core.server.logicaloperator;

import de.uniol.inf.is.odysseus.core.logicaloperator.ILogicalOperator;

public interface IOutOfOrderHandler extends ILogicalOperator{

	/**
	 * Returns true, if this operator guarantees the right output oder or false, if
	 * the elements do not follow any order
	 * 
	 * @return
	 */
	default boolean isAssureOrder() {
		return this.getOrderType() == OrderType.IN_ORDER;
	}

	/**
	 * Returns true, if the Operator was explicitly set to handle Out of Order.
	 * 
	 * @return
	 */
	default boolean isOutOfOrder() {
		return this.getOrderType() == OrderType.OUT_OF_ORDER;
	}
	
	/*
	 * Returns true If the Opertor could be set to handle OOO.
	 */
	default boolean couldHandleOutOfOrder() {
		return true;
	}

	/**
	 * Returns the the output ordering of the Operator, or if wasn't set.
	 * 
	 * @return
	 */
	OrderType getOrderType();

	/**
	 * Allow to set ordering guarantees (e.g. for operators than can reorder their
	 * output)
	 * 
	 * @param assureOrder
	 */
	void setAssureOrder(boolean assureOrder);
	
	/**
	 * tags for the Output ordering.
	 *
	 */
	public static enum OrderType {
		NOT_SET, OUT_OF_ORDER, IN_ORDER;
	}

}
