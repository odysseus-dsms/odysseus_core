package de.uniol.inf.is.odysseus.core.server.store.function;

import java.util.List;

import de.uniol.inf.is.odysseus.core.sdf.schema.SDFDatatype;
import de.uniol.inf.is.odysseus.core.server.ExecutorBinder;
import de.uniol.inf.is.odysseus.core.server.datadictionary.IDataDictionary;
import de.uniol.inf.is.odysseus.core.server.planmanagement.executor.IServerExecutor;
import de.uniol.inf.is.odysseus.core.server.store.IStore;
import de.uniol.inf.is.odysseus.core.usermanagement.ISession;
import de.uniol.inf.is.odysseus.mep.AbstractFunction;

public abstract class AbstractNamedStoreFunction<T> extends AbstractFunction<T> {

	private static final long serialVersionUID = 8618484144361039066L;
	private IStore<Comparable<?>, Object> store;

	public AbstractNamedStoreFunction(String symbol, SDFDatatype[][] acceptedTypes, SDFDatatype returnType,
			boolean optimizeConstantParameter) {
		super(symbol, acceptedTypes, returnType, optimizeConstantParameter);
	}

	protected IStore<Comparable<?>, Object> getStore(String storeKey) {
		if (store == null) {
			IServerExecutor exec = ExecutorBinder.getExecutor();
			List<ISession> sessions = getSessions();
			if (sessions == null) {
				throw new RuntimeException("Sessions in function "+this.getSymbol()+" are not set! Maybe, this function is not allowed in this operator!");
			}
			ISession session = sessions.get(0);
			IDataDictionary dd = exec.getDataDictionary(getSessions().get(0));
			store = dd.getStore(storeKey, session);
		}
		return store;
	}

	protected IStore<Comparable<?>, Object> getStore() {
		if (getArity() == 1) {
			return getStore("");
		} else {
			return getStore(getInputValue(0));
		}
	}

}
