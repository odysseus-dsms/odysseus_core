package de.uniol.inf.is.odysseus.core.server.planmanagement.executor;

import java.util.List;

import de.uniol.inf.is.odysseus.core.collection.Context;
import de.uniol.inf.is.odysseus.core.collection.OptionMap;
import de.uniol.inf.is.odysseus.core.collection.Pair;
import de.uniol.inf.is.odysseus.core.planmanagement.query.ILogicalQuery;
import de.uniol.inf.is.odysseus.core.server.planmanagement.query.querybuiltparameter.QueryBuildConfiguration;
import de.uniol.inf.is.odysseus.core.usermanagement.ISession;

/**
 * This is a base class for all PreTransformation handler
 * 
 * @author Marco Grawunder
 *
 */
public abstract class AbstractPreTransformationHandler implements IPreTransformationHandler {

	@Override
	public String getName() {
		return this.getClass().getSimpleName();
	}

	/**
	 * @deprecated Use preTransform(IServerExecutor executor, ISession caller, ILogicalQuery query,
			QueryBuildConfiguration config, OptionMap handlerParameters, Context context)
	 */
	@Deprecated 
	protected void preTransform(IServerExecutor executor, ISession caller, ILogicalQuery query,
			QueryBuildConfiguration config, List<Pair<String, String>> handlerParameters, Context context) {
	}

	/**
	 * 
	 */
	@Override
	public void preTransform(IServerExecutor executor, ISession caller, ILogicalQuery query,
			QueryBuildConfiguration config, OptionMap handlerParameters, Context context) {
		preTransform(executor, caller, query, config, handlerParameters.getPairList(), context);
	}

}
