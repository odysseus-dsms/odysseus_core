package de.uniol.inf.is.odysseus.core.server.physicaloperator;

import de.uniol.inf.is.odysseus.core.logicaloperator.LogicalOperatorCategory;
import de.uniol.inf.is.odysseus.core.sdf.schema.SDFAttribute;
import de.uniol.inf.is.odysseus.core.server.logicaloperator.AbstractLogicalOperator;
import de.uniol.inf.is.odysseus.core.server.logicaloperator.annotations.LogicalOperator;
import de.uniol.inf.is.odysseus.core.server.logicaloperator.annotations.Parameter;
import de.uniol.inf.is.odysseus.core.server.logicaloperator.builder.ResolvedSDFAttributeParameter;
import de.uniol.inf.is.odysseus.core.server.logicaloperator.builder.StringParameter;

@LogicalOperator(maxInputPorts = 1, minInputPorts = 1, name = "STOREWRITER", doc = "Writes complete objects to an existing store", url = "", category = {
		LogicalOperatorCategory.SINK })
public class StoreWriterAO extends AbstractLogicalOperator {

	private static final long serialVersionUID = -963238276136276326L;

	String storeName;
	SDFAttribute idAttribute;
	
	public StoreWriterAO() {
	}
	
	public StoreWriterAO(StoreWriterAO storeWriterAO) {
		super(storeWriterAO);
		this.storeName = storeWriterAO.storeName;
		this.idAttribute = storeWriterAO.idAttribute;
	}
	
	@Parameter(type = StringParameter.class, name="Store", optional = false, doc = "The name of the given store, to write to.")
	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}
	
	public String getStoreName() {
		return storeName;
	}
	
	@Parameter(type = ResolvedSDFAttributeParameter.class, name = "IDAttribute", optional = false, doc = "The attribute that should be used as key for the store.")
	public void setIdAttribute(SDFAttribute idAttribute) {
		this.idAttribute = idAttribute;
	}
	
	public SDFAttribute getIdAttribute() {
		return idAttribute;
	}

	@Override
	public AbstractLogicalOperator clone() {
		return new StoreWriterAO(this);
	}

}
