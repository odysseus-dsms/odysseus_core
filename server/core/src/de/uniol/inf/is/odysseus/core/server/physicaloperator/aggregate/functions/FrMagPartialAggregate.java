package de.uniol.inf.is.odysseus.core.server.physicaloperator.aggregate.functions;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.math3.complex.Complex;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import org.apache.commons.math3.transform.DftNormalization;
import org.apache.commons.math3.transform.FastFourierTransformer;
import org.apache.commons.math3.transform.TransformType;

import com.google.common.math.IntMath;

import de.uniol.inf.is.odysseus.core.server.physicaloperator.aggregate.basefunctions.AbstractPartialAggregate;

/**
 * @author Oormila Ramanandan Kottayi Pilapprathodi
 * @author Michael Suenkel
 * @author Marco Grawunder
 *
 */

public class FrMagPartialAggregate<T> extends AbstractPartialAggregate<T> {

	private static final long serialVersionUID = -7798856857638228860L;

	private final ArrayList<Double> windowSet = new ArrayList<>();

	private double res= 0.0;
	
	private final int size;

	public FrMagPartialAggregate(int size) {
		this.size = size;
	}

	public FrMagPartialAggregate(final FrMagPartialAggregate<T> partialAggregate) {
		this.windowSet.addAll(partialAggregate.windowSet);
		this.size = partialAggregate.size;
	}

	public Double getFrMag() {
		double[] meanArr = null;
		double[] magArr = null;
		double[] data = null;
		if (this.windowSet.isEmpty()) {
			return null;
		} else {
			try {
				data = this.windowSet.stream().mapToDouble(Double::doubleValue).toArray();

				meanArr = new double[(IntMath.isPowerOfTwo(data.length)) ? data.length
						: IntMath.ceilingPowerOfTwo(data.length)];
				DescriptiveStatistics da = new DescriptiveStatistics();
				for (double d : data) {
					da.addValue(d);
				}
				double mean = da.getMean();
				for (double d : data) {
					da.addValue(d);
				}
				for (int i = 0; i < data.length; i++) {
					meanArr[i] = data[i] - mean;
				}
				if (data.length != meanArr.length) {
					for (int i = data.length; i < meanArr.length; i++)
						meanArr[i] = 0.0;
				}
				// TODO: Check params				
				FastFourierTransformer fft = new FastFourierTransformer(DftNormalization.STANDARD);
				Complex[] result = fft.transform(meanArr, TransformType.FORWARD);
				magArr = new double[result.length];
				for (int i = 0; i < result.length; i++) {
					magArr[i] = result[i].abs() / data.length;

				}

				if (magArr.length > size)
					res = magArr[size];
			} catch (Exception e) {
				e.printStackTrace();
				;
			}
		}
	
		return res;
	}

	public FrMagPartialAggregate<T> add(final Number value) {
		if (value != null) {
			this.windowSet.add(value.doubleValue());
		}
		return this;
	}

	public FrMagPartialAggregate<T> merge(final FrMagPartialAggregate<?> value) {
		this.windowSet.addAll(value.windowSet);
		return this;
	}

	public void addAll(final double[] values) {
		for (final double val : values) {
			this.add(val);
		}
	}

	public void addAll(final List<Double> values) {
		for (final double val : values) {
			this.add(val);
		}
	}

	@Override
	public void clear() {
		windowSet.clear();
	}

	@Override
	public FrMagPartialAggregate<T> clone() {
		return new FrMagPartialAggregate<>(this);
	}

	@Override
	public String toString() {
		return "FrMag+"+size+"= " + this.getFrMag();
	}


}
