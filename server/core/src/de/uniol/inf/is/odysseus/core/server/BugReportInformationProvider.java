package de.uniol.inf.is.odysseus.core.server;

import de.uniol.inf.is.odysseus.core.server.information.IInformationProvider;
import de.uniol.inf.is.odysseus.core.usermanagement.ISession;
import de.uniol.inf.is.odysseus.report.IReport;
import de.uniol.inf.is.odysseus.report.IReportGenerator;

public class BugReportInformationProvider implements IInformationProvider {

	private static IReportGenerator reportGenerator;

	// called by OSGi-DS
	public void bindReportGenerator(IReportGenerator serv) {
		reportGenerator = serv;
	}

	// called by OSGi-DS
	public void unbindReportGenerator(IReportGenerator serv) {
		if (reportGenerator == serv) {
			reportGenerator = null;
		}
	}

	@Override
	public String getProviderName() {
		return "bugreport";
	}

	@Override
	public String getInformation(ISession session) {
		if (reportGenerator != null) {
			IReport report;
			try {
				report = reportGenerator.generateReport(session);
				return report.getReportMap().toString();
			} catch (Exception e) {
				// ignore
			}
		} 
		return "Report Generator not bound!";
	}

}
