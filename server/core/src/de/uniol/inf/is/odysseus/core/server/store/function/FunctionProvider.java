package de.uniol.inf.is.odysseus.core.server.store.function;

import java.util.LinkedList;
import java.util.List;

import de.uniol.inf.is.odysseus.core.mep.IMepFunction;
import de.uniol.inf.is.odysseus.mep.IFunctionProvider;

public class FunctionProvider implements IFunctionProvider {

	@Override
	public List<IMepFunction<?>> getFunctions() {
		List<IMepFunction<?>> funcs = new LinkedList<>();
		funcs.add(new KvNamedStoreWriteFunction());
		funcs.add(new KvNamedStoreRemoveFunction());
		funcs.add(new KvStoreWriteFunction());
		funcs.add(new KvStoreRemoveFunction());
		funcs.add(new KvStoreReadFunction());
		funcs.add(new KvStoreReadBoolFunction());
		funcs.add(new KvStoreReadDoubleFunction());
		funcs.add(new KvStoreReadLongFunction());
		funcs.add(new KvStoreReadStringFunction());
		funcs.add(new KvNamedStoreReadFunction());
		funcs.add(new KvNamedStoreReadBoolFunction());
		funcs.add(new KvNamedStoreReadDoubleFunction());
		funcs.add(new KvNamedStoreReadLongFunction());
		funcs.add(new KvNamedStoreReadStringFunction());
		return funcs;
	}

}
