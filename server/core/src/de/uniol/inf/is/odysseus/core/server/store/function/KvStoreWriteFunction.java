package de.uniol.inf.is.odysseus.core.server.store.function;

import de.uniol.inf.is.odysseus.core.sdf.schema.SDFDatatype;
import de.uniol.inf.is.odysseus.core.server.store.IStore;

public class KvStoreWriteFunction extends AbstractNamedStoreFunction<Boolean> {

	private static final long serialVersionUID = -3263090969225261507L;

	public static SDFDatatype[][] accTypes = new SDFDatatype[][]{
		{SDFDatatype.STRING}, (SDFDatatype[]) SDFDatatype.getTypes().toArray(new SDFDatatype[] {})};

	public KvStoreWriteFunction(){
		super("kvwrite",accTypes, SDFDatatype.OBJECT, false);
	}

	@Override
	public Boolean getValue() {
		Comparable<?> valueKey = getInputValue(0);
		Object value = getInputValue(1);

		IStore<Comparable<?>, Object> store = getStore("");

		store.put(valueKey, value);
		return true;
	}

}
