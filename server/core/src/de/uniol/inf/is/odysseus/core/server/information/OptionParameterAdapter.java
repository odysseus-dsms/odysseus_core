package de.uniol.inf.is.odysseus.core.server.information;

import java.io.IOException;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import de.uniol.inf.is.odysseus.core.option.OptionParameter;

public class OptionParameterAdapter extends TypeAdapter<OptionParameter> {

	@Override
	public OptionParameter read(JsonReader arg0) throws IOException {
		throw new RuntimeException("Currently not implemented, sorry");
	}

	@Override
	public void write(JsonWriter writer, OptionParameter option) throws IOException {
	      writer.beginObject(); 
	      writer.name("name"); 
	      writer.value(option.name()); 
	      writer.name("class"); 
	      writer.value(option.type().getCanonicalName());
	      writer.name("optional"); 
	      writer.value(option.optional());
	      writer.name("defaultValue"); 
	      writer.value(option.defaultValue());
	      writer.name("deprecated"); 
	      writer.value(option.deprecated());
	      writer.name("doc"); 
	      writer.value(option.doc());	      
	      writer.endObject(); 
	}

}
