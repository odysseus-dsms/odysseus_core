/********************************************************************************** 
  * Copyright 2011 The Odysseus Team
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  *     http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package de.uniol.inf.is.odysseus.core.server.planmanagement.query.querybuiltparameter;

import de.uniol.inf.is.odysseus.core.server.planmanagement.configuration.Setting;


/**
 * {@link IQueryBuildSetting} which provides a prioritization rule for the query.
 * 
 * @author Marcel Hamacher
 * 
 */
public  final class ParameterPrioritizationRule extends Setting<String> implements IQueryBuildSetting<String> {

	private static final long serialVersionUID = -4535137579676576L;

	/**
	 * Creates a Prioritization.
	 * 
	 * @param value Name of the prioritization rule for the query.
	 */
	public ParameterPrioritizationRule(String value) {
		super(value);
	}
}
