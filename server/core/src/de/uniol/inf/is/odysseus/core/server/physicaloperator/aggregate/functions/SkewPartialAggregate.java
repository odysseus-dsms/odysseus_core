package de.uniol.inf.is.odysseus.core.server.physicaloperator.aggregate.functions;

/**
 * @author Oormila Ramanandan Kottayi Pilapprathodi
 * @author Michael Suenkel
 *
 */

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.math3.stat.descriptive.moment.Skewness;

import de.uniol.inf.is.odysseus.core.server.physicaloperator.aggregate.basefunctions.AbstractPartialAggregate;

public class SkewPartialAggregate<T> extends AbstractPartialAggregate<T> {

    private static final long serialVersionUID = -7798856857638228860L;

    private final ArrayList<Double> windowSet = new ArrayList<>();
    

    public SkewPartialAggregate() {

    }

    public SkewPartialAggregate(final SkewPartialAggregate<T> medianPartialAggregate) {
        this.windowSet.addAll(medianPartialAggregate.windowSet);
    }

    public Double getSkewnessValue() {
    	Double skewness = null;
        if (this.windowSet.isEmpty()) {
            return null;
        }
        else {
        	try {
        	Skewness k=new Skewness();
        	skewness=k.evaluate( this.windowSet.stream()
        			.mapToDouble(Double::doubleValue).toArray(),0,this.windowSet.size());
        	if(skewness.isNaN())
        		skewness= 0.0;
        	}catch(Exception e) {
        		e.printStackTrace();
        	}
        }
        return skewness;
    }

    public SkewPartialAggregate<T> add(final Number value) {
        if (value != null) {
                this.windowSet.add(value.doubleValue());
        }
        return this;
    }

    public SkewPartialAggregate<T> merge(final SkewPartialAggregate<?> value) {
        this.windowSet.addAll(value.windowSet);
        return this;
    }

    public void addAll(final double[] values) {
        for (final double val : values) {
            this.add(val);
        }
    }

    public void addAll(final List<Double> values) {
        for (final double val : values) {
            this.add(val);
        }
    }

    @Override
    public void clear() {
        windowSet.clear();
    }

    @Override
    public SkewPartialAggregate<T> clone() {
        return new SkewPartialAggregate<>(this);
    }

    @Override
    public String toString() {
        return "SKEWNESS= " + this.getSkewnessValue();
    }

    public static void main(final String[] args) {
        final SkewPartialAggregate<?> agg = new SkewPartialAggregate<>();
        agg.add(1.0);
        agg.add(3.0);
        agg.add(2.0);
        agg.add(0.0);
        agg.add(5.0);
        agg.add(4.0);
        assert (agg.getSkewnessValue() == 2.5);
        System.out.println(agg);
    }

}
