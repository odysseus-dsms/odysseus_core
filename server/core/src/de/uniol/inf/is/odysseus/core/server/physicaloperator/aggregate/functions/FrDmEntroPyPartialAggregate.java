package de.uniol.inf.is.odysseus.core.server.physicaloperator.aggregate.functions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.math3.complex.Complex;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import org.apache.commons.math3.transform.DftNormalization;
import org.apache.commons.math3.transform.FastFourierTransformer;
import org.apache.commons.math3.transform.TransformType;

import com.google.common.math.IntMath;

import de.uniol.inf.is.odysseus.core.server.physicaloperator.aggregate.basefunctions.AbstractPartialAggregate;

/**
 * @author Oormila Ramanandan Kottayi Pilapprathodi
 * @author Michael Suenkel
 * @author Marco Grawunder
 *
 */

public class FrDmEntroPyPartialAggregate<T> extends AbstractPartialAggregate<T> {

	private static final long serialVersionUID = -7798856857638228860L;

	private final ArrayList<Double> windowSet = new ArrayList<>();

	public FrDmEntroPyPartialAggregate() {

	}

	public FrDmEntroPyPartialAggregate(final FrDmEntroPyPartialAggregate<T> medianPartialAggregate) {
		this.windowSet.addAll(medianPartialAggregate.windowSet);
	}

	public Double getFrDmEntroPy() {
		double[] meanArr = null;
		double[] data = null;
		double[] probabilityArr = null, magArray = null;
		Double frDmEntroPy = null;
		if (this.windowSet.isEmpty()) {
			return null;
		} else {
			try {
				data = this.windowSet.stream().mapToDouble(Double::doubleValue).toArray();
				meanArr = new double[(IntMath.isPowerOfTwo(data.length)) ? data.length
						: IntMath.ceilingPowerOfTwo(data.length)];
				probabilityArr = new double[(IntMath.isPowerOfTwo(data.length)) ? data.length
						: IntMath.ceilingPowerOfTwo(data.length)];
				magArray = new double[(IntMath.isPowerOfTwo(data.length)) ? data.length
						: IntMath.ceilingPowerOfTwo(data.length)];

				DescriptiveStatistics da = new DescriptiveStatistics();
				for (double d : data) {
					da.addValue(d);
				}
				double mean = da.getMean();
				for (double d : data) {
					da.addValue(d);
				}
				for (int i = 0; i < data.length; i++) {
					meanArr[i] = data[i] - mean;
				}
				if (data.length != meanArr.length) {
					for (int i = data.length; i < meanArr.length; i++)
						meanArr[i] = 0.0;
				}
				// TODO: Check types
				FastFourierTransformer fft = new FastFourierTransformer(DftNormalization.STANDARD);
				Complex[] result = fft.transform(meanArr, TransformType.FORWARD);

				for (int i = 0; i < result.length; i++) {
					magArray[i] = result[i].abs() / data.length;
				}
				for (int i = 0; i < result.length; i++) {
					probabilityArr[i] = magArray[i] / Arrays.stream(magArray).sum();
				}

				frDmEntroPy = calcEntropy(probabilityArr);
				if(frDmEntroPy.isNaN())
					frDmEntroPy=0.0;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return frDmEntroPy;
	}

	private double calcEntropy(double[] probabilityArr) {
		double entropy = 0.0;
		for (int i = 0; i < probabilityArr.length; i++) {
			entropy = entropy + (-(probabilityArr[i]) * ((int) (Math.log(probabilityArr[i]) / Math.log(2))));
		}
		return entropy;
	}

	public FrDmEntroPyPartialAggregate<T> add(final Number value) {
		if (value != null) {
			this.windowSet.add(value.doubleValue());
		}
		return this;
	}

	public FrDmEntroPyPartialAggregate<T> merge(final FrDmEntroPyPartialAggregate<?> value) {
		this.windowSet.addAll(value.windowSet);
		return this;
	}

	public void addAll(final double[] values) {
		for (final double val : values) {
			this.add(val);
		}
	}

	public void addAll(final List<Double> values) {
		for (final double val : values) {
			this.add(val);
		}
	}

	@Override
	public void clear() {
		windowSet.clear();
	}

	@Override
	public FrDmEntroPyPartialAggregate<T> clone() {
		return new FrDmEntroPyPartialAggregate<>(this);
	}

	@Override
	public String toString() {
		return "FrDmEntroPy= " + this.getFrDmEntroPy();
	}

	public static void main(final String[] args) {
		final FrDmEntroPyPartialAggregate<?> agg = new FrDmEntroPyPartialAggregate<>();
		agg.add(1.0);
		agg.add(3.0);
		agg.add(2.0);
		agg.add(0.0);
		agg.add(5.0);
		agg.add(4.0);
		assert (agg.getFrDmEntroPy() == 2.5);
		System.out.println(agg);
	}

}
