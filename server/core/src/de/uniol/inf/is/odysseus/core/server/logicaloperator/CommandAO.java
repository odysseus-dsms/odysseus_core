package de.uniol.inf.is.odysseus.core.server.logicaloperator;

import java.util.List;

import de.uniol.inf.is.odysseus.core.collection.Option;
import de.uniol.inf.is.odysseus.core.collection.OptionMap;
import de.uniol.inf.is.odysseus.core.logicaloperator.LogicalOperatorCategory;
import de.uniol.inf.is.odysseus.core.sdf.schema.SDFDatatype;
import de.uniol.inf.is.odysseus.core.sdf.schema.SDFExpression;
import de.uniol.inf.is.odysseus.core.server.logicaloperator.annotations.LogicalOperator;
import de.uniol.inf.is.odysseus.core.server.logicaloperator.annotations.Parameter;
import de.uniol.inf.is.odysseus.core.server.logicaloperator.builder.BooleanParameter;
import de.uniol.inf.is.odysseus.core.server.logicaloperator.builder.NamedExpression;
import de.uniol.inf.is.odysseus.core.server.logicaloperator.builder.NamedExpressionParameter;
import de.uniol.inf.is.odysseus.core.server.logicaloperator.builder.OptionParameter;

/**
 * 
 * @author Henrik Surm
 * @author Marco Grawunder
 * 
 */
@LogicalOperator(maxInputPorts = 1, minInputPorts = 1, name = "COMMAND", doc = "This operator executes commands on other operators or services.", category = { LogicalOperatorCategory.BASE })
public class CommandAO extends AbstractLogicalOperator 
{
	private static final long serialVersionUID = -578154679444642283L;

	private SDFExpression commandExpression;
	private OptionMap reactOnPunctuations = new OptionMap();
	private boolean suppressPunctuationReactedOn = false;
	
	public CommandAO() {
		super();
	}
	
	public CommandAO(CommandAO copy) {
		super(copy);
		if (commandExpression != null) {
			commandExpression = copy.commandExpression.clone();
		}
		reactOnPunctuations.addAll(copy.reactOnPunctuations);
		suppressPunctuationReactedOn = copy.suppressPunctuationReactedOn;
	}
	
	@Override
	public AbstractLogicalOperator clone() {
		return new CommandAO(this);
	}

	@Parameter(name="CommandExpression", optional = true, type = NamedExpressionParameter.class, doc="Expression for the commands, e.g. an attribute or a string")
	public void setCommandExpression2(NamedExpression commandExpression) {
		this.commandExpression = commandExpression.expression;
	}
	
	public SDFExpression getCommandExpression() {
		return commandExpression;
	}

	@Override
	public boolean isValid() {
		clearErrors();
		
		if(commandExpression != null && commandExpression.getType() != SDFDatatype.COMMAND) {
			addError("CommandExpression must have return type COMMAND, not " + commandExpression.getType());
		}
		
		return getErrors().isEmpty();
	}

	@Parameter(name="reactOnPunctuations", type = OptionParameter.class, isList = true, optional=true, doc="A set of pairs, the first is the name of the punctuation and the second a string with the expression over the punctuation")
	public void setReactOnPunctuations2(List<Option> reactOnPunctuations) {
		this.reactOnPunctuations.addAll(reactOnPunctuations);
	}
	
	public OptionMap getReactOnPunctuations() {
		return reactOnPunctuations;
	}

	public boolean isSuppressPunctuationReactedOn() {
		return suppressPunctuationReactedOn;
	}

	@Parameter(name="suppressPunctuationReactedOn", type = BooleanParameter.class, optional=true, doc="Used together with reactOnPunctuations. If set to true, punctuations with a reaction will be removed from stream. Default is false.")
	public void setSuppressPunctuationReactedOn(boolean suppressPunctuationReactedOn) {
		this.suppressPunctuationReactedOn = suppressPunctuationReactedOn;
	}
}
