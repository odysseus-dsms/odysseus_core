package de.uniol.inf.is.odysseus.core.server.physicaloperator.aggregate.functions;

import de.uniol.inf.is.odysseus.core.server.physicaloperator.aggregate.basefunctions.IPartialAggregate;

/**
 * @author Oormila Ramanandan Kottayi Pilapprathodi
 * @author Michael Suenkel
 *
 */

public abstract class FrDmEntroPy<R, W> extends AbstractFrDmEntroPy<R, W> {

    /**
     * 
     */
    private static final long serialVersionUID = 5517136001368391558L;

    protected FrDmEntroPy(boolean partialAggregateInput) {
        super("FrDmEntroPy", partialAggregateInput);
    }

    @Override
    public IPartialAggregate<R> init(IPartialAggregate<R> in) {
        return new FrDmEntroPyPartialAggregate<R>((FrDmEntroPyPartialAggregate<R>) in);
    }

    @Override
    public IPartialAggregate<R> merge(IPartialAggregate<R> p, R toMerge, boolean createNew) {
        return process_merge(createNew ? p.clone() : p, toMerge);
    }

    @Override
	abstract protected IPartialAggregate<R> process_merge(IPartialAggregate<R> iPartialAggregate, R toMerge);

}
