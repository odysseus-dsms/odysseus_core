package de.uniol.inf.is.odysseus.core.server.physicaloperator.aggregate.functions;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;

import de.uniol.inf.is.odysseus.core.server.physicaloperator.aggregate.basefunctions.AbstractPartialAggregate;

/**
 * @author Oormila Ramanandan Kottayi Pilapprathodi
 * @author Michael Suenkel
 *
 */

public class MCRPartialAggregate<T> extends AbstractPartialAggregate<T> {

	private static final long serialVersionUID = -7798856857638228860L;

	private final ArrayList<Double> windowSet = new ArrayList<>();

	public MCRPartialAggregate() {

	}

	public MCRPartialAggregate(final MCRPartialAggregate<T> medianPartialAggregate) {
		this.windowSet.addAll(medianPartialAggregate.windowSet);
	}

	public Double getMCRValue() {
		Double mcr = null;
		if (this.windowSet.isEmpty()) {
			return null;
		} else {
			try {
				double[] data = this.windowSet.stream().mapToDouble(Double::doubleValue).toArray();
				double[] meanArr = new double[data.length];
				DescriptiveStatistics da = new DescriptiveStatistics();
				for (double d : data) {
					da.addValue(d);
				}
				double mean = da.getMean();
				for (int i = 0; i < data.length; i++)
					meanArr[i] = data[i] - mean;
				int sum = 0;
				for (int i = 0; i < meanArr.length - 1; i++) {
					if (meanArr[i] * meanArr[i + 1] < 0)
						sum = sum + 1;
				}
				mcr = (double)sum / data.length;
				if(mcr.isNaN())
					mcr=0.0;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return mcr;
	}

	public MCRPartialAggregate<T> add(final Number value) {
		if (value != null) {
			this.windowSet.add(value.doubleValue());
		}
		return this;
	}

	public MCRPartialAggregate<T> merge(final MCRPartialAggregate<?> value) {
		this.windowSet.addAll(value.windowSet);
		return this;
	}

	public void addAll(final double[] values) {
		for (final double val : values) {
			this.add(val);
		}
	}

	public void addAll(final List<Double> values) {
		for (final double val : values) {
			this.add(val);
		}
	}

	@Override
	public void clear() {
		windowSet.clear();
	}

	@Override
	public MCRPartialAggregate<T> clone() {
		return new MCRPartialAggregate<>(this);
	}

	@Override
	public String toString() {
		return "MCR= " + this.getMCRValue();
	}

	public static void main(final String[] args) {
		final MCRPartialAggregate<?> agg = new MCRPartialAggregate<>();
		agg.add(1.0);
		agg.add(3.0);
		agg.add(2.0);
		agg.add(0.0);
		agg.add(5.0);
		agg.add(4.0);
		assert (agg.getMCRValue() == 2.5);
		System.out.println(agg);
	}

}
