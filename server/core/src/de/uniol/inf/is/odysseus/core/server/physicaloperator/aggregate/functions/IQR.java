package de.uniol.inf.is.odysseus.core.server.physicaloperator.aggregate.functions;

import de.uniol.inf.is.odysseus.core.server.physicaloperator.aggregate.basefunctions.IPartialAggregate;

public abstract class IQR<R, W> extends AbstractIQR<R, W> {

    /**
     * 
     */
    private static final long serialVersionUID = 5517136001368391558L;

    protected IQR(boolean partialAggregateInput) {
        super("IQR", partialAggregateInput);
    }

    @Override
    public IPartialAggregate<R> init(IPartialAggregate<R> in) {
        return new IQRPartialAggregate<R>((IQRPartialAggregate<R>) in);
    }

    @Override
    public IPartialAggregate<R> merge(IPartialAggregate<R> p, R toMerge, boolean createNew) {
        return process_merge(createNew ? p.clone() : p, toMerge);
    }

    @Override
	abstract protected IPartialAggregate<R> process_merge(IPartialAggregate<R> iPartialAggregate, R toMerge);

}
