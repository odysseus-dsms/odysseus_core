package de.uniol.inf.is.odysseus.core.server.physicaloperator.aggregate.functions;

import java.util.ArrayList;
import java.util.List;

import de.uniol.inf.is.odysseus.core.server.physicaloperator.aggregate.basefunctions.AbstractPartialAggregate;

/**
 * @author Oormila Ramanandan Kottayi Pilapprathodi
 * @author Michael Suenkel
 *
 */

public class RMSPartialAggregate<T> extends AbstractPartialAggregate<T> {

	private static final long serialVersionUID = -7798856857638228860L;

	private final ArrayList<Double> windowSet = new ArrayList<>();

	public RMSPartialAggregate() {

	}

	public RMSPartialAggregate(final RMSPartialAggregate<T> medianPartialAggregate) {
		this.windowSet.addAll(medianPartialAggregate.windowSet);
	}

	public Double getRMSValue() {
		Double rms = null;
		if (this.windowSet.isEmpty()) {
			return null;
		} else {
			try {
				double[] data = this.windowSet.stream().mapToDouble(Double::doubleValue).toArray();
				double sum = 0;
				for (double d : data) {
					sum = sum + d * d;
				}
				rms = Math.sqrt(sum / data.length);
				if(rms.isNaN())
					rms=0.0;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return rms;
	}

	public RMSPartialAggregate<T> add(final Number value) {
		if (value != null) {
			this.windowSet.add(value.doubleValue());
		}
		return this;
	}

	public RMSPartialAggregate<T> merge(final RMSPartialAggregate<?> value) {
		this.windowSet.addAll(value.windowSet);
		return this;
	}

	public void addAll(final double[] values) {
		for (final double val : values) {
			this.add(val);
		}
	}

	public void addAll(final List<Double> values) {
		for (final double val : values) {
			this.add(val);
		}
	}

	@Override
	public void clear() {
		windowSet.clear();
	}

	@Override
	public RMSPartialAggregate<T> clone() {
		return new RMSPartialAggregate<>(this);
	}

	@Override
	public String toString() {
		return "RMS= " + this.getRMSValue();
	}

	public static void main(final String[] args) {
		final RMSPartialAggregate<?> agg = new RMSPartialAggregate<>();
		agg.add(1.0);
		agg.add(3.0);
		agg.add(2.0);
		agg.add(0.0);
		agg.add(5.0);
		agg.add(4.0);
		assert (agg.getRMSValue() == 2.5);
		System.out.println(agg);
	}

}
