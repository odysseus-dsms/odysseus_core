package de.uniol.inf.is.odysseus.core.server.information;

import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import de.uniol.inf.is.odysseus.core.option.OptionParameter;
import de.uniol.inf.is.odysseus.core.physicaloperator.access.transport.ITransportHandlerRegistry;
import de.uniol.inf.is.odysseus.core.usermanagement.ISession;

public class TransportHandlerInformationProvider implements IInformationProvider {

	final Gson gson;
	private ITransportHandlerRegistry registry;

	public TransportHandlerInformationProvider() {
		GsonBuilder builder = new GsonBuilder();
		builder.registerTypeHierarchyAdapter(OptionParameter.class, new OptionParameterAdapter());
		gson = builder.create();
	}

	@Override
	public String getProviderName() {
		return "transporthandler";
	}

	@Override
	public String getInformation(ISession session) {
		Map<String, Set<OptionParameter>> parameters = new TreeMap<>(registry.getParameters());
		return gson.toJson(parameters);
	}

	void bindTransportHandlerRegistry(ITransportHandlerRegistry registry) {
		this.registry = registry;
	}
	
	void unbindTransportHandlerRegistry(ITransportHandlerRegistry registry) {
		this.registry = null;
	}
	
	
}
