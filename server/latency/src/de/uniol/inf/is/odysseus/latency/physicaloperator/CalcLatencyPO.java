/********************************************************************************** 
 * Copyright 2011 The Odysseus Team
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.uniol.inf.is.odysseus.latency.physicaloperator;

import java.util.Objects;

import de.uniol.inf.is.odysseus.core.metadata.IStreamObject;
import de.uniol.inf.is.odysseus.core.physicaloperator.IPhysicalOperator;
import de.uniol.inf.is.odysseus.core.physicaloperator.IPunctuation;
import de.uniol.inf.is.odysseus.core.server.metadata.ILatency;
import de.uniol.inf.is.odysseus.core.server.physicaloperator.AbstractPipe;
import de.uniol.inf.is.odysseus.core.server.planmanagement.TransformationException;
import de.uniol.inf.is.odysseus.logicaloperator.latency.CalcLatencyAO;

public class CalcLatencyPO<T extends IStreamObject<? extends ILatency>> extends AbstractPipe<T, T> {

	private final String measurementPoint;

	public CalcLatencyPO(String measurementPoint) {
		this.measurementPoint = measurementPoint;
	}

	public CalcLatencyPO(CalcLatencyAO ao) {
		if (!ao.getInputSchema().getMetaAttributeNames().contains(ILatency.class.getName())) {
			throw new TransformationException("Latency calculation needs latency metadata!");
		}
		this.measurementPoint = ao.getMeasurementPoint();
	}
	
	@Override
	public OutputMode getOutputMode() {
		return OutputMode.MODIFIED_INPUT;
	}

	@Override
	protected void process_next(T object, int port) {
		if (measurementPoint != null) {
			object.getMetadata().setLatencyEnd(measurementPoint, System.nanoTime());
		} else {
			object.getMetadata().setLatencyEnd(System.nanoTime());
		}
		transfer(object);
	}

	@Override
	public void processPunctuation(IPunctuation punctuation, int port) {
		sendPunctuation(punctuation);
	}

	@SuppressWarnings("rawtypes")
	@Override
	public boolean isSemanticallyEqual(IPhysicalOperator ipo) {
		if (!(ipo instanceof CalcLatencyPO)) {
			return false;
		}
		if (!Objects.equals(this.measurementPoint, ((CalcLatencyPO)ipo).measurementPoint)){
			return false;
		}
		return true;
	}

}
