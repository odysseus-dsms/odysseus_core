/**
 * Copyright 2015 The Odysseus Team
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.uniol.inf.is.odysseus.server.intervalapproach.logicaloperator;

import de.uniol.inf.is.odysseus.core.logicaloperator.LogicalOperatorCategory;
import de.uniol.inf.is.odysseus.core.predicate.IPredicate;
import de.uniol.inf.is.odysseus.core.sdf.schema.SDFSchema;
import de.uniol.inf.is.odysseus.core.sdf.schema.SDFSchemaFactory;
import de.uniol.inf.is.odysseus.core.server.logicaloperator.AbstractLogicalOperator;
import de.uniol.inf.is.odysseus.core.server.logicaloperator.IGeneratesHeartbeats;
import de.uniol.inf.is.odysseus.core.server.logicaloperator.UnaryLogicalOp;
import de.uniol.inf.is.odysseus.core.server.logicaloperator.annotations.LogicalOperator;
import de.uniol.inf.is.odysseus.core.server.logicaloperator.annotations.Parameter;
import de.uniol.inf.is.odysseus.core.server.logicaloperator.builder.BooleanParameter;
import de.uniol.inf.is.odysseus.core.server.logicaloperator.builder.DoubleParameter;
import de.uniol.inf.is.odysseus.core.server.logicaloperator.builder.IntegerParameter;
import de.uniol.inf.is.odysseus.core.server.logicaloperator.builder.PredicateParameter;
import de.uniol.inf.is.odysseus.core.server.logicaloperator.builder.TimeParameter;
import de.uniol.inf.is.odysseus.core.server.logicaloperator.builder.TimeValueItem;

/**
 * @author Cornelius Ludmann Tobias Brandt
 *
 */
@LogicalOperator(minInputPorts = 0, maxInputPorts = Integer.MAX_VALUE, name = "Watermark", category = {
		LogicalOperatorCategory.PROCESSING }, doc = "Sends a watermark (heartbeat) with a certain delay. The watermark then lags behind a certain timespan.")
public class WatermarkAO extends UnaryLogicalOp implements IGeneratesHeartbeats{

	private static final long serialVersionUID = -4282057482280455210L;

	private TimeValueItem timespanValue = null;
	
	private int elementCount = 0;
	private boolean useElements = false;
	
	private boolean useMPkSlack = false;
	private double lambda = 1;
	@SuppressWarnings("rawtypes")
	private IPredicate predicate = null;
	private int MpkElementsMax = 10;
	private boolean useMPkCount = false;
	private boolean useShrinkLinear = false;
	private boolean useShrinkExp = false;
	private TimeValueItem shrinkLinear = null;
	private double shrinkExp = 1;
	

	private boolean removeOutdated = true;
	
	
	/**
	 * Default constructor.
	 */
	public WatermarkAO() {
	}

	/**
	 * Copy constructor.
	 * 
	 * @param other
	 */
	public WatermarkAO(WatermarkAO other) {
		this.timespanValue = other.getTimespanValue();	
		this.removeOutdated = other.isRemoveOutdated();
		this.elementCount = other.elementCount;
		this.useElements = other.useElements;
		
		this.useMPkSlack = other.useMPkSlack;
		this.lambda = other.lambda;
		this.predicate = other.predicate;
		this.MpkElementsMax = other.MpkElementsMax;
		this.useMPkCount = other.useMPkCount;
		this.useShrinkLinear = other.useShrinkLinear;
		this.useShrinkExp = other.useShrinkExp;
		this.shrinkLinear = other.shrinkLinear;
		this.shrinkExp = other.shrinkExp;
	}

	@Parameter(type = TimeParameter.class, name = "TimespanValue", optional = true, doc = "How long the watermark lacks behind the data stream timestamps.")
	public void setTimespanValue(TimeValueItem timespan) {
		this.timespanValue = timespan;
	}
	
	public TimeValueItem getTimespanValue() {
		return timespanValue;
	}

	@Parameter(type = BooleanParameter.class, name = "removeOutdated", optional = true, doc = "By default, all elements older than the last send watermark are remove. Use this flag to send all element.")
	public void setRemoveOutdated(boolean removeOutdated) {
		this.removeOutdated = removeOutdated;
	}
	
	public boolean isRemoveOutdated() {
		return removeOutdated;
	}
	
	/*
	 * Element based
	 */
	
	public boolean isUseElements() {
		return useElements;
	}
	
	@Parameter(type = IntegerParameter.class, name = "ElementCount", optional = true, doc = "After how many Elements the Watermark is created")
	public void setElementCount(int elements) {
		this.elementCount = elements;
		this.useElements = true;
	}
	
	public int getElementCount() {
		return elementCount;
	}
	
	/*
	 * MpkSlack
	 */
	
	@Parameter(type = DoubleParameter.class, name = "lambda", optional = true, doc = "MPk-Slack lambda")
	public void setLambda(double lambda) {
		this.lambda = lambda;
		this.useMPkSlack = true;
	}
	
	public double getLambda() {
		return lambda;
	}
	
	public boolean isUseMPkSlack() {
		return useMPkSlack;
	}
	
	
	@SuppressWarnings("rawtypes")
	public IPredicate getPredicate() {
		return predicate;
	}

	@SuppressWarnings("rawtypes")
	@Parameter(type = PredicateParameter.class, name = "selectionPredicate", optional = true, doc = "MPk-Slack selectionPredicate")
	public void setPredicate(IPredicate predicate) {
		this.predicate = predicate;
		this.useMPkSlack = true;
	}
	
	public boolean isUsePredicate() {
		return predicate != null;
	}
	
	@Parameter(type = IntegerParameter.class, name = "MPkElementsMax", optional = true, doc = "after How many Elements the MpkSlack clock shoud be updatet regardless") 
	public void setMpkElementsMax(int max) {
		this.MpkElementsMax = max;
		this.useMPkCount = true;
		this.useMPkSlack = true;
	}
	
	public int getMpkElementsMax() {
		return this.MpkElementsMax;
	}
	
	
	public double getShrinkExp() {
		return shrinkExp;
	}

	@Parameter(type = DoubleParameter.class, name = "ShrinkExp", optional = true, doc = "dMax will be multiplied at the end of an update by this")
	public void setShrinkExp(double shrinkExp) {
		this.shrinkExp = shrinkExp;
		this.useShrinkExp = true;
		this.useMPkSlack = true;
	}

	public TimeValueItem getShrinkLinear() {
		return shrinkLinear;
	}

	public void setShrinkLinear(TimeValueItem shrinkLinear) {
		this.shrinkLinear = shrinkLinear;
	}
	
	public boolean isUseMpkCount() {
		return useMPkCount;
	}
	
	public boolean isUseShrinkLinear() {
		return useShrinkLinear;
	}
	
	public boolean isUseShrinkExp() {
		return useShrinkExp;
	}
	
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.uniol.inf.is.odysseus.core.server.logicaloperator.
	 * AbstractLogicalOperator#clone()
	 */
	@Override
	public AbstractLogicalOperator clone() {
		return new WatermarkAO(this);
	}
	
	@Override
	protected SDFSchema getOutputSchemaIntern(int pos) {
		setOutputSchema(SDFSchemaFactory.createNewWithOutOfOrder(true, getInputSchema()));
		return getOutputSchema();
	}

	

	

}
