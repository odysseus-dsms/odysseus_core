package de.uniol.inf.is.odysseus.server.intervalapproach;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.uniol.inf.is.odysseus.core.Order;
import de.uniol.inf.is.odysseus.core.collection.Pair;
import de.uniol.inf.is.odysseus.core.metadata.IMetadataMergeFunction;
import de.uniol.inf.is.odysseus.core.metadata.IStreamObject;
import de.uniol.inf.is.odysseus.core.metadata.ITimeInterval;
import de.uniol.inf.is.odysseus.core.physicaloperator.Heartbeat;
import de.uniol.inf.is.odysseus.core.physicaloperator.IDataMergeFunction;
import de.uniol.inf.is.odysseus.core.physicaloperator.IPhysicalOperator;
import de.uniol.inf.is.odysseus.core.physicaloperator.IPhysicalOperatorKeyValueProvider;
import de.uniol.inf.is.odysseus.core.physicaloperator.IPunctuation;
import de.uniol.inf.is.odysseus.core.physicaloperator.OpenFailedException;
import de.uniol.inf.is.odysseus.core.server.cache.ICache;
import de.uniol.inf.is.odysseus.core.server.physicaloperator.AbstractPipe;
import de.uniol.inf.is.odysseus.core.server.physicaloperator.ILeftMergeFunction;

abstract public class AbstractEnrichPO<T extends IStreamObject<M>, M extends ITimeInterval> extends AbstractPipe<T, T>
		implements IPhysicalOperatorKeyValueProvider {

	static final private Logger logger = LoggerFactory.getLogger(AbstractEnrichPO.class);

	private final ICache cache;
	private final IDataMergeFunction<T, M> dataMergeFunction;
	private final ILeftMergeFunction<T, M> dataLeftMergeFunction;
	private final IMetadataMergeFunction<M> metaMergeFunction;
	private final int[] uniqueKeys;

	private boolean sendHeartbeat;
	private boolean nesting;

	private boolean singleThreaded = true;
	private boolean keepOrder = true;
	private ExecutorService executor;
	private Thread resultProducer;
	private BlockingQueue<Future<List<IStreamObject<?>>>> futureQueue = new LinkedBlockingQueue<>();
	private Map<Future<List<IStreamObject<?>>>, Pair<T, Object>> resultMetadata = new HashMap<>();

	public AbstractEnrichPO(ICache cache, IDataMergeFunction<T, M> dataMergeFunction,
			ILeftMergeFunction<T, M> dataLeftMergeFunction, IMetadataMergeFunction<M> metaMergeFunction,
			int[] uniqueKeys) {
		super();
		this.cache = cache;
		this.dataMergeFunction = dataMergeFunction;
		this.dataLeftMergeFunction = dataLeftMergeFunction;
		this.metaMergeFunction = metaMergeFunction;
		this.uniqueKeys = uniqueKeys;
	}

	public AbstractEnrichPO(ICache cache, IDataMergeFunction<T, M> dataMergeFunction,
			IMetadataMergeFunction<M> metaMergeFunction, int[] uniqueKeys) {
		super();
		this.cache = cache;
		this.dataMergeFunction = dataMergeFunction;
		this.dataLeftMergeFunction = null;
		this.metaMergeFunction = metaMergeFunction;
		this.uniqueKeys = uniqueKeys;
	}

	public AbstractEnrichPO(AbstractEnrichPO<T, M> abstractEnrichPO) {
		super(abstractEnrichPO);
		this.cache = abstractEnrichPO.cache;
		this.dataMergeFunction = abstractEnrichPO.dataMergeFunction;
		this.dataLeftMergeFunction = abstractEnrichPO.dataLeftMergeFunction;
		this.metaMergeFunction = abstractEnrichPO.metaMergeFunction;
		this.uniqueKeys = abstractEnrichPO.uniqueKeys;
	}

	/**
	 * Set the number of workers that should be used
	 * 
	 * @param noOfWorkers number of workers that should be used, at least 1
	 * @param keepOrder   if set to false, the workers produce there output when the
	 *                    result is there, else order is kept
	 */
	public void setNoOfWorkers(int noOfWorkers, boolean keepOrder) {
		if (noOfWorkers < 1) {
			throw new IllegalArgumentException("Must be at least 1 worker!");
		}
		executor = Executors.newFixedThreadPool(noOfWorkers);
		this.singleThreaded = false;
		this.keepOrder = keepOrder;
	}

	@Override
	public de.uniol.inf.is.odysseus.core.server.physicaloperator.AbstractPipe.OutputMode getOutputMode() {
		return OutputMode.NEW_ELEMENT;
	}

	public void setSendHeartbeat(boolean sendHeartbeat) {
		this.sendHeartbeat = sendHeartbeat;
	}

	public void setNesting(boolean nesting) {
		this.nesting = nesting;
	}

	@Override
	final protected void process_open() throws OpenFailedException {
		dataMergeFunction.init();
		internal_process_open();
		if (!singleThreaded && keepOrder) {

			resultProducer = new Thread("ResultProducer") {
				public void run() {
					while (!isInterrupted()) {
						try {
							Future<List<IStreamObject<?>>> future = futureQueue.poll(1, TimeUnit.SECONDS);
							if (future != null) { // could be in case of end
								List<IStreamObject<?>> res = future.get();
								Pair<T, Object> pair = resultMetadata.remove(future);
								if (res != null) {
									handleResults(pair.getE1(), res, pair.getE2());
								}
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
			};
			resultProducer.start();
		}
	}

	@Override
	protected void process_next(T object, int port) {
		Object key = getKey(object);

		if (singleThreaded) {
			handleResults(object, retrieveResult(object, key), key);
		} else {
			if (keepOrder) {
				Future<List<IStreamObject<?>>> future = executor.submit(() -> {
					return retrieveResult(object, key);
				});
				futureQueue.add(future);
				resultMetadata.put(future, new Pair<>(object, key));
			} else {
				executor.submit(() -> {
					handleResults(object, retrieveResult(object, key), key);
				});
			}
		}

	}

	private List<IStreamObject<?>> retrieveResult(T object, Object key) {
		List<IStreamObject<?>> result = null;
		if (key != null) {
			result = cache.get(key);
		}

		// No cache or nothing found in cache
		if (result == null || result.isEmpty()) {
			result = internal_process(object);
		}
		return result;
	}

	private Object getKey(T object) {
		Object key = null;
		if (cache != null) {
			if (uniqueKeys != null) {
				key = object.restrictedHashCode(uniqueKeys);
			} else {
				key = object.hashCode();
			}
		}
		return key;
	}

	private void handleResults(T object, List<IStreamObject<?>> result, Object key) {
		if (result != null && !result.isEmpty()) {
			// Store into cache
			if (cache != null) {
				cache.put(key, result);
			}

			// Enrich and transfer
			if (nesting) {
				enrichAndTransferNested(object, result);
			} else {
				enrichAndTransferMultiple(object, result);
			}
		} else {
			if (dataLeftMergeFunction == null) {
				logger.warn("Empty result for input " + object);
			} else {
				T output = dataLeftMergeFunction.createLeftFilledUp(object);
				transfer(output);
			}
		}
		if (sendHeartbeat) {
			sendPunctuation(Heartbeat.createNewHeartbeat(object.getMetadata().getStart()));
		}
	}

	protected void enrichAndTransferNested(T object, List<IStreamObject<?>> result) {
		throw new RuntimeException("Sorry, this operator does not provide nested output!");
	}

	@SuppressWarnings("unchecked")
	protected void enrichAndTransferMultiple(T object, List<IStreamObject<?>> result) {
		for (int i = 0; i < result.size(); i++) {
			T output = dataMergeFunction.merge(object, (T) result.get(i), metaMergeFunction, Order.LeftRight);
			output.setMetadata((M) object.getMetadata().clone());
			transfer(output);
		}
	}

	@Override
	public void processPunctuation(IPunctuation punctuation, int port) {
		sendPunctuation(punctuation);
	}

	@Override
	final protected void process_close() {
		if (this.cache != null) {
			// Print the Caching Statistics
			if (logger.isDebugEnabled()) {
				logger.debug("Caching Statistics: ");
				logger.debug("Cache hits: " + cache.getCacheHits());
				logger.debug("Cache miss: " + cache.getCacheMiss());
				logger.debug("Cache inserts: " + cache.getCacheInsert());
				logger.debug("Cache removes: " + cache.getCacheRemoves());
			}
			cache.close();
		}
		internal_process_close();
		if (!singleThreaded) {
			// TODO: how to handle values, still in queue
			if (!futureQueue.isEmpty()) {
				logger.warn("Will cancel " + futureQueue.size() + " jobs!");
				futureQueue.forEach(f -> f.cancel(true));
			}
			resultProducer.interrupt();
		}

	}

	@Override
	public boolean isSemanticallyEqual(IPhysicalOperator ipo) {

		if (!(ipo instanceof AbstractEnrichPO)) {
			return false;
		}

		@SuppressWarnings("unchecked")
		AbstractEnrichPO<T, M> other = (AbstractEnrichPO<T, M>) ipo;
		if (!Arrays.equals(uniqueKeys, other.uniqueKeys)) {
			return false;
		}

		return true;
	}

	abstract protected void internal_process_open();

	abstract protected List<IStreamObject<?>> internal_process(T input);

	abstract protected void internal_process_close();

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.uniol.inf.is.odysseus.core.physicaloperator.
	 * IPhysicalOperatorKeyValueProvider#getKeyValues()
	 */
	@Override
	public Map<String, String> getKeyValues() {
		Map<String, String> result = new HashMap<>();
		result.put("Cache", "" + cache);
		result.put("Left Merge Function", "" + dataLeftMergeFunction);
		result.put("Unique Keys", "" + uniqueKeys);
		return result;
	}
}
