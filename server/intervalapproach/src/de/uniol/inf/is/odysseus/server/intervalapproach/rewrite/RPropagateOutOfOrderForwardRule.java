package de.uniol.inf.is.odysseus.server.intervalapproach.rewrite;


import de.uniol.inf.is.odysseus.core.server.logicaloperator.IOutOfOrderHandler;
import de.uniol.inf.is.odysseus.core.server.logicaloperator.IOutOfOrderHandler.OrderType;
import de.uniol.inf.is.odysseus.core.server.planmanagement.optimization.configuration.RewriteConfiguration;
import de.uniol.inf.is.odysseus.rewrite.flow.RewriteRuleFlowGroup;
import de.uniol.inf.is.odysseus.rewrite.rule.AbstractRewriteRule;
import de.uniol.inf.is.odysseus.ruleengine.rule.RuleException;
import de.uniol.inf.is.odysseus.ruleengine.ruleflow.IRuleFlowGroup;

public class RPropagateOutOfOrderForwardRule extends AbstractRewriteRule<IOutOfOrderHandler>{

	@Override
	public void execute(IOutOfOrderHandler operator, RewriteConfiguration config) throws RuleException {
		if(isExecutable(operator, config)) {
			operator.setAssureOrder(false);
		}
	}

	@Override
	public boolean isExecutable(IOutOfOrderHandler operator, RewriteConfiguration config) {
		if(operator.getOrderType() == OrderType.NOT_SET && operator.couldHandleOutOfOrder()) {
			for (int port = 0; port < operator.getNumberOfInputs(); port++) {
				if(!operator.getInputSchema(port).isInOrder()) {
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public IRuleFlowGroup getRuleFlowGroup() {
		return RewriteRuleFlowGroup.PROPAGATE_OOO;
	}

	@Override
	public int getPriority() {
		// TODO Auto-generated method stub
		return 0;
	}

}
