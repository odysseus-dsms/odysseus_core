/*******************************************************************************
 * Copyright (C) 2014  Christian Kuka <christian@kuka.cc>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package de.uniol.inf.is.odysseus.server.intervalapproach;

import java.util.Iterator;

import de.uniol.inf.is.odysseus.core.metadata.IStreamObject;
import de.uniol.inf.is.odysseus.core.metadata.ITimeInterval;
import de.uniol.inf.is.odysseus.core.metadata.PointInTime;
import de.uniol.inf.is.odysseus.core.physicaloperator.IPunctuation;
import de.uniol.inf.is.odysseus.core.server.physicaloperator.AbstractPipe;
import de.uniol.inf.is.odysseus.sweeparea.IOutOfOrderTimeIntervalSweepArea;

/**
 * @author Christian Kuka <christian@kuka.cc>
 *
 */
public class DistinctTIPO<T extends IStreamObject<? extends ITimeInterval>> extends AbstractPipe<T, T> {
    /** The sweep area to hold the data. */
    final private IOutOfOrderTimeIntervalSweepArea<T> area;
    boolean outOfOrder;

    public DistinctTIPO(IOutOfOrderTimeIntervalSweepArea<T> area, boolean outOfOrder) {
        this.area = area;
        this.outOfOrder = outOfOrder;
    }

    public DistinctTIPO(DistinctTIPO<T> po) {
        super(po);
        this.area = (IOutOfOrderTimeIntervalSweepArea<T>) po.area.clone();
        this.outOfOrder = po.outOfOrder;
    }

    @Override
    public OutputMode getOutputMode() {
        return OutputMode.INPUT;
    }
    @Override
	protected void process_next(T object, int port) {
		if(outOfOrder) {
			this.process_next_OutOfOrder(object);
		} else {
			this.process_next_InOrder(object,port);
		}
	}
    
    @SuppressWarnings("unchecked")
	protected synchronized void process_next_OutOfOrder(T object) {
    	Iterator<T> qualifies = this.area.extractOverlappingAndEquals(object);
    	if(!qualifies.hasNext()) {
    		area.insert(object);
    		transfer(object);
    	} else {
    		PointInTime tLast = object.getMetadata().getStart();
    		PointInTime tEarliest;
    		T elem = qualifies.next();
    		if(tLast.before(elem.getMetadata().getStart())) {
    			T out = (T) object.clone();
    			out.getMetadata().setStartAndEnd(tLast, elem.getMetadata().getStart());
    			transfer(out);
    			tEarliest = tLast;
    		}
    		else {
    			tEarliest = elem.getMetadata().getStart();
    		}
    		tLast = elem.getMetadata().getEnd();
    		while(qualifies.hasNext()) {
    			T element = qualifies.next();
    			if(tLast.before(element.getMetadata().getStart())) {
    				T out = (T) object.clone();
        			out.getMetadata().setStartAndEnd(tLast, elem.getMetadata().getStart());
        			transfer(out);
    			}
    			tLast = element.getMetadata().getEnd();
    		}
    		if(tLast.before(object.getMetadata().getEnd())) {
    			T out = (T) object.clone();
    			out.getMetadata().setStartAndEnd(tLast, object.getMetadata().getEnd());
    			transfer(out);
    			tLast = object.getMetadata().getEnd();
    		}
    		object.getMetadata().setStartAndEnd(tEarliest, tLast);
    		area.insert(object);
    	}
    }

    protected void process_next_InOrder(T object, int port) {
        Iterator<T> iter = this.area.extractElementsStartingBeforeOrEquals(object.getMetadata().getStart());
        while (iter.hasNext()) {
            T next = iter.next();
            if (next.equals(object)) {
                if (!next.getMetadata().getStart().equals(object.getMetadata().getStart())) {
                    if (next.getMetadata().getEnd().after(object.getMetadata().getStart())) {
                        next.getMetadata().setEnd(object.getMetadata().getStart());
                    }
                    transfer(next);
                }
            }
            else {
                // Assume equal end timestamp (slide window)
                transfer(next);
            }
        }

        this.area.insert(object);
    }

	@Override
	public void processPunctuation(IPunctuation punctuation, int port) {
		if(this.outOfOrder && punctuation.isHeartbeat()) {
			this.area.purgeElementsBefore(punctuation.getTime());
		}
		sendPunctuation(punctuation);
	}

	
    
}
