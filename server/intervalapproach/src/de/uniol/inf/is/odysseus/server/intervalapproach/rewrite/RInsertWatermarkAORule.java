package de.uniol.inf.is.odysseus.server.intervalapproach.rewrite;

import java.util.ArrayList;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.uniol.inf.is.odysseus.core.logicaloperator.ILogicalOperator;
import de.uniol.inf.is.odysseus.core.logicaloperator.LogicalSubscription;
import de.uniol.inf.is.odysseus.core.planmanagement.query.LogicalPlan;
import de.uniol.inf.is.odysseus.core.server.logicaloperator.IOutOfOrderHandler;
import de.uniol.inf.is.odysseus.core.server.planmanagement.optimization.configuration.RewriteConfiguration;
import de.uniol.inf.is.odysseus.rewrite.flow.RewriteRuleFlowGroup;
import de.uniol.inf.is.odysseus.rewrite.rule.AbstractRewriteRule;
import de.uniol.inf.is.odysseus.ruleengine.rule.RuleException;
import de.uniol.inf.is.odysseus.ruleengine.ruleflow.IRuleFlowGroup;
import de.uniol.inf.is.odysseus.server.intervalapproach.logicaloperator.WatermarkAO;

public class RInsertWatermarkAORule extends AbstractRewriteRule<IOutOfOrderHandler>{

	 @SuppressWarnings("unused")
	private static final Logger LOG = LoggerFactory.getLogger(RInsertWatermarkAORule.class);

	
	@Override
	public void execute(IOutOfOrderHandler operator, RewriteConfiguration config) throws RuleException {
		if(isExecutable(operator, config)) {
			Collection<LogicalSubscription> subscriptions = new ArrayList<>(operator.getSubscribedToSource());
			
			for(LogicalSubscription sub : subscriptions) {
				if (sub.getSchema().isInOrder()) {
					WatermarkAO watermark = new WatermarkAO();
					
					Collection<ILogicalOperator> toUpdate = LogicalPlan.insertOperator(watermark, operator,
							sub.getSinkInPort(), 0, sub.getSourceOutPort());
					
					insert(watermark);

					for (ILogicalOperator o : toUpdate) {
						update(o);
					}
				}
			}
		}
		
	}

	@Override
	public boolean isExecutable(IOutOfOrderHandler operator,RewriteConfiguration config) {
		if (operator.isOutOfOrder()) {
			for (int port = 0; port < operator.getNumberOfInputs(); port++) {
				if (operator.getInputSchema(port) != null && operator.getInputSchema(port).isInOrder()) {
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public IRuleFlowGroup getRuleFlowGroup() {
		return RewriteRuleFlowGroup.INSERT_OOO;
	}

	@Override
	public int getPriority() {
		// TODO Auto-generated method stub
		return 0;
	}
	

}
