/**
 * Copyright 2015 The Odysseus Team
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.uniol.inf.is.odysseus.server.intervalapproach;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.uniol.inf.is.odysseus.core.metadata.IStreamObject;
import de.uniol.inf.is.odysseus.core.metadata.ITimeInterval;
import de.uniol.inf.is.odysseus.core.metadata.PointInTime;
import de.uniol.inf.is.odysseus.core.physicaloperator.Heartbeat;
import de.uniol.inf.is.odysseus.core.physicaloperator.IPhysicalOperator;
import de.uniol.inf.is.odysseus.core.physicaloperator.IPunctuation;
import de.uniol.inf.is.odysseus.core.physicaloperator.OpenFailedException;
import de.uniol.inf.is.odysseus.core.predicate.IPredicate;
import de.uniol.inf.is.odysseus.core.server.logicaloperator.builder.TimeValueItem;
import de.uniol.inf.is.odysseus.core.server.physicaloperator.AbstractPipe;
import de.uniol.inf.is.odysseus.core.server.physicaloperator.IHasPredicate;

/**
 * This operator is useful for out-of-order processing. It sends a watermark (as
 * heartbeats / punctuations) which lags behind the application time. Together
 * with the AssureOrder-operator this can be used to bring the elements back to
 * the right order.
 * 
 * Simple example: timespan = 10 minutes; When an element arrives at 12:00 (and
 * no other later element arrived before), a heartbeat with time 11:50 is send.
 * Now the AssureOrder-operator knows that it can transfer all elements until
 * 11:50 because it is guaranteed that no element will arrive earlier (later
 * incoming out-of-order elements could for example be dropped).
 * 
 * @author Cornelius Ludmann, Tobias Brandt, Marco Grawunder
 *
 */
public class WatermarkPO<R extends IStreamObject<? extends ITimeInterval>> extends AbstractPipe<R, R>
		implements IHasPredicate {

	private static final Logger LOG = LoggerFactory.getLogger(WatermarkPO.class);

	// The time span to lag behind the application time
	private final boolean useTimespan;
	private final long timespan;

	private final boolean useElementCount;
	private long[] elements;
	private int elementIndex = 0;
	private final int elementCount;

	private final boolean useLambda;
	private final boolean usePredicate;
	private final boolean useCounter;
	private int mpkElementsCount;
	private final ArrayList<Long> mpkElements; // doesn't use SweepArea because its operating on longs
	private final int mpkElementMax;
	private final double lambda;
	private long clk = 0;
	private long dmax = 0;
	private IPredicate<? super R> pred;
	private final long shrinkLinear;
	private final double shrinkExp;
	private final boolean useShrinkLinear;
	private final boolean useShrinkExp;

	// The current watermark -> the current time minus the timespan
	private long watermark = 0;

	// If set to true, all elements that are older than any send watermark will be
	// removed
	private final boolean removeOutdated;

	/**
	 * Creates a new watermark operator.
	 * 
	 * @param timespan The time span the watermarkt / heartbeats have to lag behind
	 */
	public WatermarkPO(TimeValueItem dragBegindTime, TimeUnit baseTimeUnit, boolean removeOutdated,
			boolean useElementCount, int elementCount, boolean useLambda, double lambda, boolean usePredicate,
			IPredicate<? super R> predicate, boolean useCounter, int mpkElementsMax, boolean useShrinkLinear,
			TimeValueItem shrinkLinear, boolean useShrinkExp, double shrinkExp) {
		if (dragBegindTime != null) {
			long timeValue = dragBegindTime.getTime();
			TimeUnit unit = dragBegindTime.getUnit();
			this.timespan = baseTimeUnit.convert(timeValue, unit);
			this.useTimespan = true;
		} else {
			this.timespan = 0;
			this.useTimespan = !(useLambda || useElementCount); // defualt to zero timespan if the op wouldn't do
																// anything otherwise
		}
		this.removeOutdated = removeOutdated;
		// Elements
		this.useElementCount = useElementCount;
		this.elementCount = elementCount;
		if (useElementCount) {
			elements = new long[elementCount];
		}
		// MP-k-Slack
		this.useLambda = useLambda;
		this.usePredicate = usePredicate;
		this.useCounter = useCounter;
		this.useShrinkLinear = useShrinkLinear;
		this.useShrinkExp = useShrinkExp;
		this.pred = predicate;
		this.mpkElementMax = mpkElementsMax;
		if (useCounter) {
			this.mpkElements = new ArrayList<Long>(mpkElementMax);
		} else {
			this.mpkElements = new ArrayList<Long>();
		}
		this.lambda = lambda;
		this.shrinkLinear = shrinkLinear == null ? 0
				: baseTimeUnit.convert(shrinkLinear.getTime(), shrinkLinear.getUnit());
		this.shrinkExp = shrinkExp;
	}

	@Override
	protected void process_next(R object, int port) {
		PointInTime ts = object.getMetadata().getStart();
		if (removeOutdated && elementIsOlderThanWatermark(object)) {
			if (object.getMetadata().getEnd().getMainPoint() > watermark) {
				object.getMetadata().setStart(new PointInTime(watermark));
				transfer(object, port);
			}
			if (LOG.isWarnEnabled()) {
				LOG.warn("Removed outdated element " + object);
			}
		} else {
			transfer(object, port);

		}
		// Send a water mark that lags behind the given amount of time
		sendWatermarkHeartbeat(ts, port, object);
	}

	private boolean elementIsOlderThanWatermark(R object) {
		return object.getMetadata().getStart().getMainPoint() < watermark;
	}

	@Override
	public void processPunctuation(IPunctuation punctuation, int port) {
		/*
		 * do not redirect other heartbeats directly that would disturb the subsequent
		 * operators that use the heardbeats produced by this operator
		 */
		if (!punctuation.isHeartbeat()) {
			sendPunctuation(punctuation, port);
		}
		if (punctuation.getTime().getMainPoint() > watermark) {
			watermark = punctuation.getTime().getMainPoint();
			sendPunctuation(Heartbeat.createNewHeartbeat(watermark), port);
		}
	}

	/**
	 * Send a heartbeat that is the globally defined timespan behind
	 * 
	 * @param time current time, e.g. from another heartbeat or a tuple
	 * @param port the port from which the input came is used for the output as well
	 */
	private synchronized void sendWatermarkHeartbeat(PointInTime time, int port, R object) {
		/*
		 * if the new element minus the timespan is older than our last watermark (out
		 * of order) we will use the watermark as we guarantee that the watermark is in
		 * order
		 */
		long t = time.getMainPoint();
		long newMark = watermark;

		if (useTimespan && currentElementExpandsWatermark(time, watermark)) {
			newMark = calculateWatermarkFromCurrentElement(time);
		}

		/*
		 * if the ts of the element that arrived element count elements ago is bigger
		 * than the current watermark, the watermark is this point in time.
		 */
		if (useElementCount) {
			if (elements[elementIndex] > newMark) {
				newMark = elements[elementIndex];
			}
			elements[elementIndex] = t;
			elementIndex = (elementIndex + 1) % elementCount;
		}

		if (useLambda) {
			mpkElements.add(t);
			mpkElementsCount++;
			if (useCounter && mpkElementsCount >= mpkElementMax || usePredicate && clk < t && pred.evaluate(object)) {
				mpkElementsCount = 0;
				if (clk < t) {
					clk = t;
				}
				long sum = 0;
				int border = 0;
				long[] ds = new long[mpkElements.size()];
				mpkElements.sort(Comparator.reverseOrder());
				for (int i = mpkElements.size() - 1; i > 0; i--) {
					long ts = mpkElements.get(i);
					border = i;
					if (ts > clk) {
						break;
					}
					long d = clk - ts;
					sum += d;
					ds[i] = d;
					if (d > dmax) {
						dmax = d;
					}
				}
				long mean = sum / (mpkElements.size() - border);
				sum = 0;
				for (int i = mpkElements.size() - 1; i > border; i--) {
					long d = ds[i];
					sum += (mean - d) * (mean - d);
				}
				double std_deviation = Math.sqrt(sum / mpkElements.size());
				long newWatermark = clk - (dmax + (long) (lambda * std_deviation));
				if (newWatermark > newMark) {
					newMark = newWatermark;
					mpkElements.removeIf(ts -> ts > newWatermark);
				}
				if (useShrinkLinear) {
					dmax -= shrinkLinear;
				}
				if (useShrinkExp) {
					dmax *= shrinkExp;
				}
			}

		}
		if (newMark > watermark) {
			watermark = newMark;
			sendPunctuation(Heartbeat.createNewHeartbeat(watermark), port);
		}
	}

	private boolean currentElementExpandsWatermark(PointInTime currentTime, long watermark) {
		long watermarkFromCurrentElement = calculateWatermarkFromCurrentElement(currentTime);
		return watermarkFromCurrentElement > watermark;
	}

	private long calculateWatermarkFromCurrentElement(PointInTime currentTime) {
		return currentTime.getMainPoint() - timespan;
	}

	@Override
	public OutputMode getOutputMode() {
		return OutputMode.INPUT;
	}

	@Override
	protected void process_open() throws OpenFailedException {
		super.process_open();
		this.watermark = Long.MIN_VALUE;
	}

	@Override
	protected void process_close() {
		sendPunctuation(Heartbeat.createNewHeartbeat(PointInTime.INFINITY));
	}
	
	@Override
	protected void process_done() {
		sendPunctuation(Heartbeat.createNewHeartbeat(PointInTime.INFINITY));
	}

	@Override
	public boolean process_isSemanticallyEqual(IPhysicalOperator ipo) {
		if (!(ipo instanceof WatermarkPO)) {
			return false;
		}
		WatermarkPO<?> other = (WatermarkPO<?>) ipo;
		boolean ret = other.useTimespan == this.useTimespan && other.useElementCount == this.useElementCount
				&& other.useLambda == this.useLambda;
		if (!ret) {
			return false;
		}
		if (this.useTimespan) {
			ret = this.timespan == other.timespan;
		}
		if (this.useElementCount) {
			ret = ret && this.elementCount == other.elementCount;
		}
		if (this.useLambda) {
			ret = ret && this.lambda == other.lambda && this.usePredicate == other.usePredicate
					&& this.useCounter == other.useCounter && this.useShrinkExp == other.useShrinkExp
					&& this.useShrinkLinear == other.useShrinkLinear;
			if (this.usePredicate) {
				ret = ret && this.pred.equals(other.pred);
			}
			if (this.useCounter) {
				ret = ret && this.mpkElementMax == other.mpkElementMax;
			}
			if (this.useShrinkExp) {
				ret = ret && this.shrinkExp == other.shrinkExp;
			}
			if (this.useShrinkLinear) {
				ret = ret && this.shrinkLinear == other.shrinkLinear;
			}
		}
		return ret;
	}

	@Override
	public IPredicate<? super R> getPredicate() {
		// TODO Auto-generated method stub
		return pred;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void setPredicate(IPredicate<?> predicate) {
		pred = (IPredicate<? super R>) predicate;

	}

}
