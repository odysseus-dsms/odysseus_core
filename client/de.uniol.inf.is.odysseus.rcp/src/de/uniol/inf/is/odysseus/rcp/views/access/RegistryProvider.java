package de.uniol.inf.is.odysseus.rcp.views.access;

import de.uniol.inf.is.odysseus.core.physicaloperator.access.protocol.IProtocolHandlerRegistry;
import de.uniol.inf.is.odysseus.core.physicaloperator.access.transport.ITransportHandlerRegistry;

public class RegistryProvider {

	public static ITransportHandlerRegistry transportHandlerRegistry;
	public static IProtocolHandlerRegistry protocolHandlerRegistry;

	public void bindTransportHandlerRegistry(ITransportHandlerRegistry registry) {
		transportHandlerRegistry = registry;
	}
	
	public void unbindTransportHandlerRegistry(ITransportHandlerRegistry registry) {
		transportHandlerRegistry = null;
	}
	
	public void bindProtocolHandlerRegistry(IProtocolHandlerRegistry registry) {
		protocolHandlerRegistry = registry;
	}
	
	public void unbindProtocolHandlerRegistry(IProtocolHandlerRegistry registry) {
		protocolHandlerRegistry = null;
	}
}
