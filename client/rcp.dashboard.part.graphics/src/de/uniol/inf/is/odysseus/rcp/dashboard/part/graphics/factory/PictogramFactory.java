package de.uniol.inf.is.odysseus.rcp.dashboard.part.graphics.factory;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.gef.requests.CreationFactory;

import de.uniol.inf.is.odysseus.rcp.dashboard.part.graphics.model.AbstractPictogram;

public class PictogramFactory implements CreationFactory {
	
	private Class<? extends AbstractPictogram> type;

	public PictogramFactory(Class<? extends AbstractPictogram> type){
		this.type = type;
	}
	
	@Override
	public Object getNewObject() {
		try {
			return type.getDeclaredConstructor().newInstance();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {		
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Object getObjectType() {
		return AbstractPictogram.class;
	}
}
